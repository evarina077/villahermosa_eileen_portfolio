﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // method to exit program
        public void ExitApplication(object sender, EventArgs e)
        {
            //exit the program
            Application.Exit();
        }

        // method to clear all data input back to default values
        private void ClearData()
        {
            //default setting for all input controls when user hit reset
            nameTextBox.Text = " ";
            robotCheckBox.Checked = false;
            weightNumericUpDown.Value = 0;
            dateTimePicker.Value = DateTime.Today;
        }

        // method for the reset button
        private void resetButton_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        // method for save menu item
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //save user file
            SaveFileDialog save = new SaveFileDialog();

            // statement to be sure the files are saved as .txt
            save.Filter = "(*.txt)|";

            //if there is file to save this will run and bring up the Save As
            //window screen
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //this will write all of the input control file
                StreamWriter write = new StreamWriter(File.Create(save.FileName));
                write.WriteLine(nameTextBox.Text);
                write.WriteLine(weightNumericUpDown.Value);
                write.WriteLine(dateTimePicker.Value);
                write.Dispose();
                ClearData();
            }
        }

        // method for load menu item
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //load user file
            OpenFileDialog open = new OpenFileDialog();
                        



                        //if there is file to load this will run and bring up the Open File
                        //window screen
                        if (open.ShowDialog() == DialogResult.OK)
                        {
                            //this will read the choosen file to load
                            StreamReader read = new StreamReader(File.OpenRead(open.FileName));
            
                            //this will load the choosen file
                            nameTextBox.Text = read.ReadLine();
                            weightNumericUpDown.Value = Decimal.Parse(read.ReadLine());
                            dateTimePicker.Text = read.ReadLine();

                            read.Dispose();
                    
                
            }
        }
    }
}
