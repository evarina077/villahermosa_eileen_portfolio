﻿namespace EileenVillahermosa_CE02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weightTracker = new System.Windows.Forms.GroupBox();
            this.dateLabel = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.weightLabel = new System.Windows.Forms.Label();
            this.weightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.resetButton = new System.Windows.Forms.Button();
            this.keepCheck = new System.Windows.Forms.CheckBox();
            this.weightsGroupBox = new System.Windows.Forms.GroupBox();
            this.removeArchiveItemButton = new System.Windows.Forms.Button();
            this.removeKeepItemButton = new System.Windows.Forms.Button();
            this.moveToKeepButton = new System.Windows.Forms.Button();
            this.moveToArchiveButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.archiveListBox = new System.Windows.Forms.ListBox();
            this.keepListBox = new System.Windows.Forms.ListBox();
            this.archiveCheck = new System.Windows.Forms.CheckBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.weightTracker.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).BeginInit();
            this.weightsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.statsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(708, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitApplication);
            // 
            // statsToolStripMenuItem
            // 
            this.statsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem});
            this.statsToolStripMenuItem.Name = "statsToolStripMenuItem";
            this.statsToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.statsToolStripMenuItem.Text = "Stats";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // weightTracker
            // 
            this.weightTracker.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.weightTracker.Controls.Add(this.submitButton);
            this.weightTracker.Controls.Add(this.archiveCheck);
            this.weightTracker.Controls.Add(this.dateLabel);
            this.weightTracker.Controls.Add(this.dateTimePicker);
            this.weightTracker.Controls.Add(this.nameLabel);
            this.weightTracker.Controls.Add(this.nameTextBox);
            this.weightTracker.Controls.Add(this.weightLabel);
            this.weightTracker.Controls.Add(this.weightNumericUpDown);
            this.weightTracker.Controls.Add(this.resetButton);
            this.weightTracker.Controls.Add(this.keepCheck);
            this.weightTracker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weightTracker.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.weightTracker.Location = new System.Drawing.Point(12, 60);
            this.weightTracker.Name = "weightTracker";
            this.weightTracker.Size = new System.Drawing.Size(684, 348);
            this.weightTracker.TabIndex = 1;
            this.weightTracker.TabStop = false;
            this.weightTracker.Text = "Weight Tracker";
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(68, 169);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(72, 31);
            this.dateLabel.TabIndex = 7;
            this.dateLabel.Text = "Date";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(145, 164);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(477, 38);
            this.dateTimePicker.TabIndex = 6;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(57, 74);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(86, 31);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Name";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(145, 71);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(406, 38);
            this.nameTextBox.TabIndex = 4;
            // 
            // weightLabel
            // 
            this.weightLabel.AutoSize = true;
            this.weightLabel.Location = new System.Drawing.Point(46, 118);
            this.weightLabel.Name = "weightLabel";
            this.weightLabel.Size = new System.Drawing.Size(98, 31);
            this.weightLabel.TabIndex = 3;
            this.weightLabel.Text = "Weight";
            // 
            // weightNumericUpDown
            // 
            this.weightNumericUpDown.Location = new System.Drawing.Point(145, 116);
            this.weightNumericUpDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.weightNumericUpDown.Name = "weightNumericUpDown";
            this.weightNumericUpDown.Size = new System.Drawing.Size(120, 38);
            this.weightNumericUpDown.TabIndex = 2;
            // 
            // resetButton
            // 
            this.resetButton.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.resetButton.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.Location = new System.Drawing.Point(52, 268);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(265, 56);
            this.resetButton.TabIndex = 1;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = false;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // keepCheck
            // 
            this.keepCheck.AutoSize = true;
            this.keepCheck.Location = new System.Drawing.Point(145, 214);
            this.keepCheck.Name = "keepCheck";
            this.keepCheck.Size = new System.Drawing.Size(109, 35);
            this.keepCheck.TabIndex = 0;
            this.keepCheck.Text = "Keep";
            this.keepCheck.UseVisualStyleBackColor = true;
            // 
            // weightsGroupBox
            // 
            this.weightsGroupBox.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.weightsGroupBox.Controls.Add(this.removeArchiveItemButton);
            this.weightsGroupBox.Controls.Add(this.removeKeepItemButton);
            this.weightsGroupBox.Controls.Add(this.moveToKeepButton);
            this.weightsGroupBox.Controls.Add(this.moveToArchiveButton);
            this.weightsGroupBox.Controls.Add(this.label2);
            this.weightsGroupBox.Controls.Add(this.label1);
            this.weightsGroupBox.Controls.Add(this.archiveListBox);
            this.weightsGroupBox.Controls.Add(this.keepListBox);
            this.weightsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weightsGroupBox.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.weightsGroupBox.Location = new System.Drawing.Point(12, 424);
            this.weightsGroupBox.Name = "weightsGroupBox";
            this.weightsGroupBox.Size = new System.Drawing.Size(684, 396);
            this.weightsGroupBox.TabIndex = 15;
            this.weightsGroupBox.TabStop = false;
            this.weightsGroupBox.Text = "Weights List";
            // 
            // removeArchiveItemButton
            // 
            this.removeArchiveItemButton.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.removeArchiveItemButton.Font = new System.Drawing.Font("Consolas", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeArchiveItemButton.Location = new System.Drawing.Point(390, 316);
            this.removeArchiveItemButton.Name = "removeArchiveItemButton";
            this.removeArchiveItemButton.Size = new System.Drawing.Size(256, 56);
            this.removeArchiveItemButton.TabIndex = 7;
            this.removeArchiveItemButton.Text = "Remove Item";
            this.removeArchiveItemButton.UseVisualStyleBackColor = false;
            this.removeArchiveItemButton.Click += new System.EventHandler(this.removeArchiveItemButton_Click);
            // 
            // removeKeepItemButton
            // 
            this.removeKeepItemButton.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.removeKeepItemButton.Font = new System.Drawing.Font("Consolas", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeKeepItemButton.Location = new System.Drawing.Point(24, 316);
            this.removeKeepItemButton.Name = "removeKeepItemButton";
            this.removeKeepItemButton.Size = new System.Drawing.Size(256, 56);
            this.removeKeepItemButton.TabIndex = 6;
            this.removeKeepItemButton.Text = "Remove Item";
            this.removeKeepItemButton.UseVisualStyleBackColor = false;
            this.removeKeepItemButton.Click += new System.EventHandler(this.removeKeepItemButton_Click);
            // 
            // moveToKeepButton
            // 
            this.moveToKeepButton.BackColor = System.Drawing.Color.YellowGreen;
            this.moveToKeepButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moveToKeepButton.Location = new System.Drawing.Point(298, 191);
            this.moveToKeepButton.Name = "moveToKeepButton";
            this.moveToKeepButton.Size = new System.Drawing.Size(75, 41);
            this.moveToKeepButton.TabIndex = 5;
            this.moveToKeepButton.Text = "<<";
            this.moveToKeepButton.UseVisualStyleBackColor = false;
            this.moveToKeepButton.Click += new System.EventHandler(this.moveToKeepButton_Click);
            // 
            // moveToArchiveButton
            // 
            this.moveToArchiveButton.BackColor = System.Drawing.Color.YellowGreen;
            this.moveToArchiveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moveToArchiveButton.Location = new System.Drawing.Point(298, 127);
            this.moveToArchiveButton.Name = "moveToArchiveButton";
            this.moveToArchiveButton.Size = new System.Drawing.Size(75, 41);
            this.moveToArchiveButton.TabIndex = 4;
            this.moveToArchiveButton.Text = ">>";
            this.moveToArchiveButton.UseVisualStyleBackColor = false;
            this.moveToArchiveButton.Click += new System.EventHandler(this.moveToArchiveButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(385, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = "Archive";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Keep";
            // 
            // archiveListBox
            // 
            this.archiveListBox.FormattingEnabled = true;
            this.archiveListBox.ItemHeight = 31;
            this.archiveListBox.Location = new System.Drawing.Point(390, 74);
            this.archiveListBox.Name = "archiveListBox";
            this.archiveListBox.Size = new System.Drawing.Size(256, 221);
            this.archiveListBox.TabIndex = 1;
            this.archiveListBox.SelectedIndexChanged += new System.EventHandler(this.archiveListBox_SelectedIndexChanged);
            // 
            // keepListBox
            // 
            this.keepListBox.FormattingEnabled = true;
            this.keepListBox.ItemHeight = 31;
            this.keepListBox.Location = new System.Drawing.Point(24, 74);
            this.keepListBox.Name = "keepListBox";
            this.keepListBox.Size = new System.Drawing.Size(256, 221);
            this.keepListBox.TabIndex = 0;
            this.keepListBox.SelectedIndexChanged += new System.EventHandler(this.keepListBox_SelectedIndexChanged);
            // 
            // archiveCheck
            // 
            this.archiveCheck.AutoSize = true;
            this.archiveCheck.Location = new System.Drawing.Point(276, 214);
            this.archiveCheck.Name = "archiveCheck";
            this.archiveCheck.Size = new System.Drawing.Size(137, 35);
            this.archiveCheck.TabIndex = 11;
            this.archiveCheck.Text = "Archive";
            this.archiveCheck.UseVisualStyleBackColor = true;
            // 
            // submitButton
            // 
            this.submitButton.BackColor = System.Drawing.Color.YellowGreen;
            this.submitButton.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(357, 268);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(265, 56);
            this.submitButton.TabIndex = 12;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = false;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 831);
            this.Controls.Add(this.weightsGroupBox);
            this.Controls.Add(this.weightTracker);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.weightTracker.ResumeLayout(false);
            this.weightTracker.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown)).EndInit();
            this.weightsGroupBox.ResumeLayout(false);
            this.weightsGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox weightTracker;
        private System.Windows.Forms.NumericUpDown weightNumericUpDown;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.CheckBox keepCheck;
        private System.Windows.Forms.Label weightLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.ToolStripMenuItem statsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.GroupBox weightsGroupBox;
        private System.Windows.Forms.ListBox archiveListBox;
        private System.Windows.Forms.ListBox keepListBox;
        private System.Windows.Forms.Button moveToKeepButton;
        private System.Windows.Forms.Button moveToArchiveButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button removeArchiveItemButton;
        private System.Windows.Forms.Button removeKeepItemButton;
        private System.Windows.Forms.CheckBox archiveCheck;
        private System.Windows.Forms.Button submitButton;
    }
}

