﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Weight Data
        {
            get
            {
                Weight w = new Weight();
                w.Name = nameTextBox.Text;
                w.UserWeight = weightNumericUpDown.Value;
                w.Date = dateTimePicker.Text;
                w.KeepCheck = keepCheck.Checked;
                w.ArchiveCheck = archiveCheck.Checked;

                return w;
            }

            set
            {
                nameTextBox.Text = value.Name;
                weightNumericUpDown.Value = value.UserWeight;
                dateTimePicker.Text = value.Date;
                keepCheck.Checked = value.KeepCheck;
                archiveCheck.Checked = value.ArchiveCheck;
            }
        }

        // store entered data
        string weightList;

        public void ExitApplication(object sender, EventArgs e)
        {
            //exit the program
            Application.Exit();
        }

        private void ClearData()
        {
            nameTextBox.Text = "";
            archiveCheck.Checked = false;
            weightNumericUpDown.Value = 0;
            dateTimePicker.Value = DateTime.Today;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            //default setting for all input controls when user hit reset
            ClearData();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if(archiveCheck.Checked != true)
            {
                keepListBox.Items.Add(Data);

                weightList += nameTextBox.Text + " " + weightNumericUpDown.Value + " " + dateTimePicker.Text
                + " " + keepCheck.Checked + "\r\n";

                ClearData();
            }

            else
            {
                archiveListBox.Items.Add(Data);

                weightList += nameTextBox.Text + " " + weightNumericUpDown.Value + " " + dateTimePicker.Text
                + " " + archiveCheck.Checked + "\r\n";

                ClearData();
            }
        }

        private void keepListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (keepListBox.SelectedIndex >= 0 && keepListBox.SelectedIndex < keepListBox.Items.Count)
            {
                Weight w = keepListBox.Items[keepListBox.SelectedIndex] as Weight;

                Data = w;
            }
        }

        private void archiveListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (archiveListBox.SelectedIndex >= 0 && archiveListBox.SelectedIndex < archiveListBox.Items.Count)
            {
                Weight w = archiveListBox.Items[archiveListBox.SelectedIndex] as Weight;

                Data = w;
            }
        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StatForm form = new StatForm();

            form.TextDisplay1 = keepListBox.Items.Count.ToString();
            form.TextDisplay2 = archiveListBox.Items.Count.ToString();

            form.ShowDialog();
        }

        private void moveToArchiveButton_Click(object sender, EventArgs e)
        {
            if(keepListBox.SelectedIndex >= 0)
            {
                archiveListBox.Items.Add(keepListBox.SelectedItem);
                keepListBox.Items.Remove(keepListBox.SelectedItem);
            }
        }

        private void moveToKeepButton_Click(object sender, EventArgs e)
        {
            if (archiveListBox.SelectedIndex >= 0)
            {
                keepListBox.Items.Add(archiveListBox.SelectedItem);
                archiveListBox.Items.Remove(archiveListBox.SelectedItem);
            }
        }

        private void removeKeepItemButton_Click(object sender, EventArgs e)
        {
            if (keepListBox.SelectedIndex >= 0)
            {
                keepListBox.Items.Remove(keepListBox.SelectedItem);
            }
        }

        private void removeArchiveItemButton_Click(object sender, EventArgs e)
        {
            if (archiveListBox.SelectedIndex >= 0)
            {
                archiveListBox.Items.Remove(archiveListBox.SelectedItem);
            }
        }

        // Save data to external file .txt
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog(); //creating my dialog for saving
            saveFile.Filter = "(*.txt)|"; //making sure the files are saved as .txt
            //saveFile.ShowDialog(); //showing and saving for user

            if (saveFile.ShowDialog() == DialogResult.OK) //only if they select ok this will run
            {
                using (StreamWriter List = new StreamWriter(saveFile.FileName))
                {
                    //Tacking all info and saving it into my writer 
                    List.WriteLine(weightList.TrimEnd()); //using TrimEnd to ensure there are no spaces this = no crashes for null
                }
            }

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog(); //variable for grabing info

            // openFile.ShowDialog(); //Show options to user
            if (openFile.ShowDialog() == DialogResult.OK) //stops crash if user selects cancel
            {

                keepListBox.Items.Clear();
                archiveListBox.Items.Clear();

                using (StreamReader info = new StreamReader(openFile.FileName))
                {
                    while (!info.EndOfStream)
                    {
                        string open = info.ReadLine(); //Reading ONE LINE AT A TIME to populate my objects

                        string[] fileOpen = open.Split(' '); //taking that Line making it a string and placing it in a array

                        Weight LoadList = new Weight();

                        LoadList.Name = fileOpen[0];
                        //taking all info and placing it into the right boxes
                        LoadList.UserWeight = Decimal.Parse(fileOpen[1]);

                        LoadList.Date = fileOpen[2];
                        LoadList.KeepCheck = Boolean.Parse(fileOpen[3]); //parsing information

                        if (LoadList.ArchiveCheck != false)
                            {
                                keepListBox.Items.Add(LoadList);
                                ClearData();
                            }

                        else
                          {
                                archiveListBox.Items.Add(LoadList);
                                ClearData();
                          }
                    }
                }
            }

        }


    }
}
