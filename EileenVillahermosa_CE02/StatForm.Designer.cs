﻿namespace EileenVillahermosa_CE02
{
    partial class StatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayTextBox1 = new System.Windows.Forms.TextBox();
            this.counterLabel1 = new System.Windows.Forms.Label();
            this.displayTextBox2 = new System.Windows.Forms.TextBox();
            this.counterLabel2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // displayTextBox1
            // 
            this.displayTextBox1.Location = new System.Drawing.Point(151, 33);
            this.displayTextBox1.Name = "displayTextBox1";
            this.displayTextBox1.ReadOnly = true;
            this.displayTextBox1.Size = new System.Drawing.Size(100, 31);
            this.displayTextBox1.TabIndex = 0;
            this.displayTextBox1.Text = "0";
            // 
            // counterLabel1
            // 
            this.counterLabel1.AutoSize = true;
            this.counterLabel1.Location = new System.Drawing.Point(29, 36);
            this.counterLabel1.Name = "counterLabel1";
            this.counterLabel1.Size = new System.Drawing.Size(106, 25);
            this.counterLabel1.TabIndex = 1;
            this.counterLabel1.Text = "Counter 1";
            // 
            // displayTextBox2
            // 
            this.displayTextBox2.Location = new System.Drawing.Point(151, 76);
            this.displayTextBox2.Name = "displayTextBox2";
            this.displayTextBox2.ReadOnly = true;
            this.displayTextBox2.Size = new System.Drawing.Size(100, 31);
            this.displayTextBox2.TabIndex = 2;
            this.displayTextBox2.Text = "0";
            // 
            // counterLabel2
            // 
            this.counterLabel2.AutoSize = true;
            this.counterLabel2.Location = new System.Drawing.Point(29, 79);
            this.counterLabel2.Name = "counterLabel2";
            this.counterLabel2.Size = new System.Drawing.Size(106, 25);
            this.counterLabel2.TabIndex = 3;
            this.counterLabel2.Text = "Counter 2";
            // 
            // StatForm
            // 
            this.ClientSize = new System.Drawing.Size(324, 163);
            this.Controls.Add(this.counterLabel2);
            this.Controls.Add(this.displayTextBox2);
            this.Controls.Add(this.counterLabel1);
            this.Controls.Add(this.displayTextBox1);
            this.Name = "StatForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox displayTextBox1;
        private System.Windows.Forms.Label counterLabel1;
        private System.Windows.Forms.TextBox displayTextBox2;
        private System.Windows.Forms.Label counterLabel2;
    }
}