﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EileenVillahermosa_CE02
{
    public partial class StatForm : Form
    {
        public String TextDisplay1
        {
            set
            {
                displayTextBox1.Text = value;
            }
        }

        public String TextDisplay2
        {
            set
            {
                displayTextBox2.Text = value;
            }
        }

        public StatForm()
        {
            InitializeComponent();
        }

    }
}
