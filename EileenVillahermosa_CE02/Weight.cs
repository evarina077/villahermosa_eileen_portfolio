﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EileenVillahermosa_CE02
{
    public class Weight
    {
        string name;
        decimal userWeight;
        bool keepCheck;
        bool archiveCheck;
        string date;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public decimal UserWeight
        {
            get
            {
                return userWeight;
            }

            set
            {
                userWeight = value;
            }
        }

        public bool KeepCheck
        {
            get
            {
                return keepCheck;
            }

            set
            {
                keepCheck = value;
            }
        }

        public bool ArchiveCheck
        {
            get
            {
                return archiveCheck;
            }

            set
            {
                archiveCheck = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public override string ToString()
        {
            return UserWeight + "lbs";
        }

    }
}
