﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE03
{
    public partial class Form1 : Form
    {
        List<Weight> weightList = new List<Weight>();

        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exit the program
            Application.Exit();
        }

        // method created to clear all when needed
        public void ResetAll()
        {
            nameTextBox.Clear();
            weightNumericUpDown.Value = 0;
            dateTimePicker.Value = DateTime.Today;
            maleButton.Checked = false;
            femaleButton.Checked = false;
        }

        // this will call to the reset method
        private void resetButton_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Weight data = new Weight();

            data.Name = nameTextBox.Text;
            data.WeightNumber = weightNumericUpDown.Value;
            data.Date = dateTimePicker.Text;
            data.MaleButton = maleButton.Checked;
            data.FemaleButton = femaleButton.Checked;

            weightList.Add(data);

            listView1.Items.Add(weightList.ToString());

            

            ResetAll();
        }


        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // instantiate the listForm

            listForm newWindow = new listForm();

            

            newWindow.Show();
        }
    }
}
