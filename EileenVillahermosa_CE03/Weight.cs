﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EileenVillahermosa_CE03
{
    public class Weight
    {
        string nameTextBox;
        decimal weightNumericUpDown;
        bool maleButton;
        bool femaleButton;
        string dateTimePicker;

        public string Name
        {
            get
            {
                return nameTextBox;
            }

            set
            {
                nameTextBox = value;
            }
        }

        public decimal WeightNumber
        {
            get
            {
                return weightNumericUpDown;
            }

            set
            {
                weightNumericUpDown = value;
            }
        }

        public bool MaleButton
        {
            get
            {
                return maleButton;
            }

            set
            {
                maleButton = value;
            }
        }

        public bool FemaleButton
        {
            get
            {
                return femaleButton;
            }

            set
            {
                femaleButton = value;
            }
        }

        public string Date
        {
            get
            {
                return dateTimePicker;
            }

            set
            {
                dateTimePicker = value;
            }
        }


    }
}
