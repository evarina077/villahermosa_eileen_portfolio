﻿namespace EileenVillahermosa_CE04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtDisplayUserInputWindowsCount = new System.Windows.Forms.TextBox();
            this.userInputWindowsCount = new System.Windows.Forms.Label();
            this.txtDisplayListViewObjectCount = new System.Windows.Forms.TextBox();
            this.listViewObjectCount = new System.Windows.Forms.Label();
            this.btnOpenUserInputForm = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(498, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(151, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.displayToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(62, 36);
            this.listToolStripMenuItem.Text = "List";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.clearToolStripMenuItem.Text = "Clear";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // txtDisplayUserInputWindowsCount
            // 
            this.txtDisplayUserInputWindowsCount.Location = new System.Drawing.Point(349, 146);
            this.txtDisplayUserInputWindowsCount.Name = "txtDisplayUserInputWindowsCount";
            this.txtDisplayUserInputWindowsCount.ReadOnly = true;
            this.txtDisplayUserInputWindowsCount.Size = new System.Drawing.Size(80, 31);
            this.txtDisplayUserInputWindowsCount.TabIndex = 1;
            this.txtDisplayUserInputWindowsCount.Text = "0";
            // 
            // userInputWindowsCount
            // 
            this.userInputWindowsCount.AutoSize = true;
            this.userInputWindowsCount.Location = new System.Drawing.Point(58, 149);
            this.userInputWindowsCount.Name = "userInputWindowsCount";
            this.userInputWindowsCount.Size = new System.Drawing.Size(266, 25);
            this.userInputWindowsCount.TabIndex = 2;
            this.userInputWindowsCount.Text = "User Input Windows Count";
            // 
            // txtDisplayListViewObjectCount
            // 
            this.txtDisplayListViewObjectCount.Location = new System.Drawing.Point(349, 199);
            this.txtDisplayListViewObjectCount.Name = "txtDisplayListViewObjectCount";
            this.txtDisplayListViewObjectCount.ReadOnly = true;
            this.txtDisplayListViewObjectCount.Size = new System.Drawing.Size(80, 31);
            this.txtDisplayListViewObjectCount.TabIndex = 3;
            this.txtDisplayListViewObjectCount.Text = "0";
            // 
            // listViewObjectCount
            // 
            this.listViewObjectCount.AutoSize = true;
            this.listViewObjectCount.Location = new System.Drawing.Point(95, 202);
            this.listViewObjectCount.Name = "listViewObjectCount";
            this.listViewObjectCount.Size = new System.Drawing.Size(229, 25);
            this.listViewObjectCount.TabIndex = 4;
            this.listViewObjectCount.Text = "List View Object Count";
            // 
            // btnOpenUserInputForm
            // 
            this.btnOpenUserInputForm.BackColor = System.Drawing.Color.MistyRose;
            this.btnOpenUserInputForm.Location = new System.Drawing.Point(100, 70);
            this.btnOpenUserInputForm.Name = "btnOpenUserInputForm";
            this.btnOpenUserInputForm.Size = new System.Drawing.Size(297, 48);
            this.btnOpenUserInputForm.TabIndex = 5;
            this.btnOpenUserInputForm.Text = "Open User Input Form";
            this.btnOpenUserInputForm.UseVisualStyleBackColor = false;
            this.btnOpenUserInputForm.Click += new System.EventHandler(this.btnOpenUserInputForm_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(498, 273);
            this.Controls.Add(this.btnOpenUserInputForm);
            this.Controls.Add(this.listViewObjectCount);
            this.Controls.Add(this.txtDisplayListViewObjectCount);
            this.Controls.Add(this.userInputWindowsCount);
            this.Controls.Add(this.txtDisplayUserInputWindowsCount);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.TextBox txtDisplayUserInputWindowsCount;
        private System.Windows.Forms.Label userInputWindowsCount;
        private System.Windows.Forms.TextBox txtDisplayListViewObjectCount;
        private System.Windows.Forms.Label listViewObjectCount;
        private System.Windows.Forms.Button btnOpenUserInputForm;
    }
}

