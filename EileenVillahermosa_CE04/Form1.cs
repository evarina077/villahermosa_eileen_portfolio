﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE04
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18

    public partial class Form1 : Form
    {
        public static int objectCount;
        public static int windowCount;

        public event EventHandler PopList;
        public event EventHandler ClearList;

        // list to hold all values
        public static List<User> userList = new List<User>();

        public Form1()
        {
            InitializeComponent();
        }

        public void btnOpenUserInputForm_Click(object sender, EventArgs e)
        { 
            windowCount++;

            // instantiate's the User Input Window form
            UserInputWindow newWindow = new UserInputWindow();

            newWindow.AddUser += AddList_eh;
            newWindow.CloseWindow += CloseWindow_eh;
            newWindow.UpdateListView += UpdateListView_eh;

            // modeless style of dialog
            newWindow.Show();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            // this will exit the program
            Application.Exit();
        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            windowCount++;

          ListViewForm viewForm = new ListViewForm();

            // subscription
            PopList += viewForm.DataInput_eh;
            viewForm.ClearForm += ClearAll;
            ClearList += viewForm._Clearview;
            viewForm.AddWindow += PopWind_eh;

            if(PopList != null)
            {
                PopList(this, new EventArgs());
            }

            viewForm.Show();



        }

        public void WindowCounter()
        {
            txtDisplayUserInputWindowsCount.Text = windowCount.ToString();
        }

        public void ObjectCounter()
        {
            txtDisplayListViewObjectCount.Text = objectCount.ToString();
        }

        private void AddList_eh(object sender, EventArgs e)
        {
            UserInputWindow newWindow = new UserInputWindow();

            newWindow = (sender as UserInputWindow);

            userList.Add(newWindow.Data);

            objectCount++;

            ObjectCounter();
        }

        public void CloseWindow_eh(object sender, EventArgs e)
        {
            WindowCounter();

        }

        public void UpdateListView_eh(object sender, EventArgs e)
        {

            if(PopList != null)
            {
                PopList(this, new EventArgs());
            }
        }

        private void ClearAll(object sender, EventArgs e)
        {
            userList.Clear();
            objectCount = 0;
            txtDisplayListViewObjectCount.Text = objectCount.ToString();
        }
    
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAll(sender, new EventArgs());
            ClearList(sender, new EventArgs());
        }


        private void PopWind_eh(object sender, EventArgs e)
        {
            ObjectCounter();
        }


        // Method to display the count by calling the CountActiveForms method
        //private void txtDisplayUserInputWindowsCount_TextChanged(object sender, EventArgs e)
        //{
        // TextDisplay = CountActiveForms().ToString();
        //}


    }
}
