﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE04
{
    public partial class ListViewForm : Form
    {
        public event EventHandler ClearForm;
        public event EventHandler DataInput;
        public event EventHandler AddWindow;

        public ListViewForm()
        {
            InitializeComponent();
        }

        public ListViewItem selectedStudent
        {
            get
            {
                if(listViewInput.SelectedItems.Count >0)
                {
                    return listViewInput.SelectedItems[0];
                }
                return null;
            }
        }

        public void DataInput_eh(object sender, EventArgs e)
        {
            listViewInput.Items.Clear();

            foreach (User u in Form1.userList)
            {
                ListViewItem lvi = new ListViewItem();

                lvi.Text = u.FirstName;
                lvi.Text = u.LastName;
                lvi.ImageIndex = u.ImageIndex;
                lvi.Tag = u;

                listViewInput.Items.Add(lvi);
            }
        }

        public void UpdateListView_eh(object sender, EventArgs e)
        {
            foreach (User u in Form1.userList)
            {
                ListViewItem lvi = new ListViewItem();

                lvi.Text = u.FirstName;
                lvi.Text = u.LastName;
                lvi.ImageIndex = u.ImageIndex;
                lvi.Tag = u;

                listViewInput.Items.Add(lvi);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            listViewInput.Items.Clear();
            ClearForm(this, new EventArgs());
        }

        public void _Clearview(object sender, EventArgs e)
        {
            listViewInput.Items.Clear();
        }
        
        private void ListView_listV_DoubleClick(object sender, EventArgs e)
        {
            UserInputWindow NewForm = new UserInputWindow();

            NewForm.Data = (User)selectedStudent.Tag;

            NewForm.Show();

            Form1.windowCount++;

            AddWindow(this, new EventArgs());
        }
}
}
