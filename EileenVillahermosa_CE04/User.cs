﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EileenVillahermosa_CE04
{

    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18

    // class created for user data input
    public class User
    {
        // variables for the user input
        private string firstName;
        private string lastName;
        private decimal age;
        private bool maleButton;
        private bool femaleButton;
        private string gender;
        private int imageIndex;

        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }

        public decimal Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }

        public bool buttonOnlineStudent
        {
            get
            {
                return maleButton;
            }

            set
            {
                maleButton = value;
            }
        }

        public bool buttonCampusStudent
        {
            get
            {
                return femaleButton;
            }

            set
            {
                femaleButton = value;
            }
        }

        public string comboxBox_Gender
        {
            get
            {
                return gender;
            }

            set
            {
                gender = value;
            }
        }

        public int ImageIndex
        {
            get
            {
                return imageIndex;
            }

            set
            {
                imageIndex = value;
            }
        }

        // override method for proper display of data input into list
        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }


}
