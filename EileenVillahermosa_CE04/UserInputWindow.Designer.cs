﻿namespace EileenVillahermosa_CE04
{
    partial class UserInputWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserInputWindow));
            this.groupBoxStudentInput = new System.Windows.Forms.GroupBox();
            this.labelGender = new System.Windows.Forms.Label();
            this.comboBox_Gender = new System.Windows.Forms.ComboBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.lableEnrollmentType = new System.Windows.Forms.Label();
            this.radioButtonCampus = new System.Windows.Forms.RadioButton();
            this.radioButtonOnline = new System.Windows.Forms.RadioButton();
            this.labelAge = new System.Windows.Forms.Label();
            this.numericAge = new System.Windows.Forms.NumericUpDown();
            this.labelLastName = new System.Windows.Forms.Label();
            this.textLastName = new System.Windows.Forms.TextBox();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.textFirstName = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAdd = new System.Windows.Forms.ToolStripButton();
            this.groupBoxStudentInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAge)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxStudentInput
            // 
            this.groupBoxStudentInput.Controls.Add(this.labelGender);
            this.groupBoxStudentInput.Controls.Add(this.comboBox_Gender);
            this.groupBoxStudentInput.Controls.Add(this.buttonReset);
            this.groupBoxStudentInput.Controls.Add(this.lableEnrollmentType);
            this.groupBoxStudentInput.Controls.Add(this.radioButtonCampus);
            this.groupBoxStudentInput.Controls.Add(this.radioButtonOnline);
            this.groupBoxStudentInput.Controls.Add(this.labelAge);
            this.groupBoxStudentInput.Controls.Add(this.numericAge);
            this.groupBoxStudentInput.Controls.Add(this.labelLastName);
            this.groupBoxStudentInput.Controls.Add(this.textLastName);
            this.groupBoxStudentInput.Controls.Add(this.labelFirstName);
            this.groupBoxStudentInput.Controls.Add(this.textFirstName);
            this.groupBoxStudentInput.Location = new System.Drawing.Point(12, 54);
            this.groupBoxStudentInput.Name = "groupBoxStudentInput";
            this.groupBoxStudentInput.Size = new System.Drawing.Size(496, 378);
            this.groupBoxStudentInput.TabIndex = 0;
            this.groupBoxStudentInput.TabStop = false;
            this.groupBoxStudentInput.Text = "Student Input";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.Location = new System.Drawing.Point(113, 241);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(89, 25);
            this.labelGender.TabIndex = 10;
            this.labelGender.Text = "Gender:";
            // 
            // comboBox_Gender
            // 
            this.comboBox_Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Gender.FormattingEnabled = true;
            this.comboBox_Gender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.comboBox_Gender.Location = new System.Drawing.Point(219, 238);
            this.comboBox_Gender.Name = "comboBox_Gender";
            this.comboBox_Gender.Size = new System.Drawing.Size(214, 33);
            this.comboBox_Gender.TabIndex = 2;
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.LightGray;
            this.buttonReset.Location = new System.Drawing.Point(86, 296);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(347, 50);
            this.buttonReset.TabIndex = 9;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // lableEnrollmentType
            // 
            this.lableEnrollmentType.AutoSize = true;
            this.lableEnrollmentType.Location = new System.Drawing.Point(28, 196);
            this.lableEnrollmentType.Name = "lableEnrollmentType";
            this.lableEnrollmentType.Size = new System.Drawing.Size(174, 25);
            this.lableEnrollmentType.TabIndex = 8;
            this.lableEnrollmentType.Text = "Enrollment Type:";
            // 
            // radioButtonCampus
            // 
            this.radioButtonCampus.AutoSize = true;
            this.radioButtonCampus.Location = new System.Drawing.Point(330, 192);
            this.radioButtonCampus.Name = "radioButtonCampus";
            this.radioButtonCampus.Size = new System.Drawing.Size(122, 29);
            this.radioButtonCampus.TabIndex = 7;
            this.radioButtonCampus.TabStop = true;
            this.radioButtonCampus.Text = "Campus";
            this.radioButtonCampus.UseVisualStyleBackColor = true;
            // 
            // radioButtonOnline
            // 
            this.radioButtonOnline.AutoSize = true;
            this.radioButtonOnline.Location = new System.Drawing.Point(219, 194);
            this.radioButtonOnline.Name = "radioButtonOnline";
            this.radioButtonOnline.Size = new System.Drawing.Size(105, 29);
            this.radioButtonOnline.TabIndex = 6;
            this.radioButtonOnline.TabStop = true;
            this.radioButtonOnline.Text = "Online";
            this.radioButtonOnline.UseVisualStyleBackColor = true;
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Location = new System.Drawing.Point(146, 152);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(56, 25);
            this.labelAge.TabIndex = 5;
            this.labelAge.Text = "Age:";
            // 
            // numericAge
            // 
            this.numericAge.Location = new System.Drawing.Point(219, 150);
            this.numericAge.Name = "numericAge";
            this.numericAge.Size = new System.Drawing.Size(120, 31);
            this.numericAge.TabIndex = 4;
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(81, 109);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(121, 25);
            this.labelLastName.TabIndex = 3;
            this.labelLastName.Text = "Last Name:";
            // 
            // textLastName
            // 
            this.textLastName.Location = new System.Drawing.Point(219, 103);
            this.textLastName.Name = "textLastName";
            this.textLastName.Size = new System.Drawing.Size(214, 31);
            this.textLastName.TabIndex = 2;
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(81, 59);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(122, 25);
            this.labelFirstName.TabIndex = 1;
            this.labelFirstName.Text = "First Name:";
            // 
            // textFirstName
            // 
            this.textFirstName.Location = new System.Drawing.Point(219, 56);
            this.textFirstName.Name = "textFirstName";
            this.textFirstName.Size = new System.Drawing.Size(214, 31);
            this.textFirstName.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.LightGray;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAdd});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(542, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAdd
            // 
            this.toolStripButtonAdd.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAdd.Image")));
            this.toolStripButtonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAdd.Name = "toolStripButtonAdd";
            this.toolStripButtonAdd.Size = new System.Drawing.Size(163, 36);
            this.toolStripButtonAdd.Text = "addButton";
            this.toolStripButtonAdd.Click += new System.EventHandler(this.toolStripButtonAdd_Click);
            // 
            // UserInputWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(542, 447);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBoxStudentInput);
            this.Name = "UserInputWindow";
            this.Text = "UserInputForm";
            this.groupBoxStudentInput.ResumeLayout(false);
            this.groupBoxStudentInput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericAge)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxStudentInput;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAdd;
        private System.Windows.Forms.Label lableEnrollmentType;
        private System.Windows.Forms.RadioButton radioButtonCampus;
        private System.Windows.Forms.RadioButton radioButtonOnline;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.NumericUpDown numericAge;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.TextBox textLastName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.TextBox textFirstName;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.ComboBox comboBox_Gender;
        private System.Windows.Forms.Label labelGender;
    }
}