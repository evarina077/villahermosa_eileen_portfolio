﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EileenVillahermosa_CE04
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18

    public partial class UserInputWindow : Form
    {
        // declared the event handler for subscription
        // to the ListViewForm's method
        public event EventHandler AddUser;
        public event EventHandler CloseWindow;
        public event EventHandler UpdateListView;

        Form1 userListForm;

        public UserInputWindow()
        {
            InitializeComponent();
        }

        // data variable created to form all user data input
        // call to User Class
        public User Data
        {
            // accessor return the data input values
            get
            {
                User u = new User();

                u.FirstName = textFirstName.Text;
                u.LastName = textLastName.Text;
                u.Age = numericAge.Value;
                u.buttonOnlineStudent = radioButtonOnline.Checked;
                u.buttonCampusStudent = radioButtonCampus.Checked;
                u.comboxBox_Gender = comboBox_Gender.Text;
                //u.StudentTypeComboBox = comboBoxStudentType.Text;
                //u.ImageIndex = comboBoxStudentType.SelectedIndex;

                if(radioButtonOnline.Checked) 
                {
                    u.ImageIndex = 0;
                }
                else
                {
                    u.ImageIndex = 1;
                }

                return u;
            }

            // accessor to set the data input values
            set
            {
                // if statement to make sure values are not null
                // to prevent exception errors
                if (value != null)
                {
                    textFirstName.Text = value.FirstName;
                    textLastName.Text = value.LastName;
                    numericAge.Value = value.Age;
                    radioButtonOnline.Checked = value.buttonOnlineStudent;
                    radioButtonCampus.Checked = value.buttonCampusStudent;
                    comboBox_Gender.Text = value.comboxBox_Gender;
                }
            }
        }


        // method to clear all data input when needed
        public void ResetAll()
        {
            textFirstName.Clear();
            textLastName.Clear();
            numericAge.Value = 0;
            radioButtonOnline.Checked = false;
            radioButtonCampus.Checked = false;
            comboBox_Gender.Text = null;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            // call to reset method
            // this will clear data input when Reset button is clicked
            ResetAll();
        }

        public void toolStripButtonAdd_Click(object sender, EventArgs e)
        {

            // check the event handler to avoid null error
            if (AddUser != null)
            {
                AddUser(this, new EventArgs());
            }

            if (UpdateListView != null)
            {
                UpdateListView(this, new EventArgs());
            }


            ResetAll();

        }

        private void UserInput_FormClosed(object sender, EventArgs e)
        {
            Form1.windowCount--;

            if (CloseWindow != null)
            {
                CloseWindow(this, new EventArgs());
            }
        }

    }
}
