﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EileenVillahermosa_CE06
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE06: TreeView and TabControl
    // 03/13/2018

    // Class created for user data input
    class Leg
    {
        private string comboBox_Direction;
        private decimal number_Miles;
        private decimal number_Hours;
        private string mode;

        public string ComboBox_Direction
        {
            get
            {
                return comboBox_Direction;
            }

            set
            {
                comboBox_Direction = value;
            }
        }

        public decimal Number_Miles
        {
            get
            {
                return number_Miles;
            }

            set
            {
                number_Miles = value;
            }
        }

        public decimal Number_Hours
        {
            get
            {
                return number_Hours;
            }

            set
            {
                number_Hours = value;
            }
        }

        public string Mode
        {
            get
            {
                return mode;
            }

            set
            {
                mode = value;
            }
        }

    }
}
