﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EileenVillahermosa_CE06
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE06: TreeView and TabControl
    // 03/13/2018


    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TravelPlanner());
        }
    }
}
