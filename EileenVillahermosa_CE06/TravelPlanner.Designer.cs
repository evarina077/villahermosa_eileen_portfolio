﻿namespace EileenVillahermosa_CE06
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE06: TreeView and TabControl
    // 03/13/2018


    partial class TravelPlanner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TravelPlanner));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_Leg = new System.Windows.Forms.TabPage();
            this.button_Add = new System.Windows.Forms.Button();
            this.groupBox_Leg = new System.Windows.Forms.GroupBox();
            this.label_Mode = new System.Windows.Forms.Label();
            this.textBox_Mode = new System.Windows.Forms.TextBox();
            this.numericUpDown_Hours = new System.Windows.Forms.NumericUpDown();
            this.label_Hours = new System.Windows.Forms.Label();
            this.numericUpDown_Miles = new System.Windows.Forms.NumericUpDown();
            this.label_Miles = new System.Windows.Forms.Label();
            this.label_Direction = new System.Windows.Forms.Label();
            this.comboBox_Direction = new System.Windows.Forms.ComboBox();
            this.tabPage_Totals = new System.Windows.Forms.TabPage();
            this.label_LegTotal = new System.Windows.Forms.Label();
            this.textBox_Legs = new System.Windows.Forms.TextBox();
            this.label_HoursTotal = new System.Windows.Forms.Label();
            this.textBox_Hours = new System.Windows.Forms.TextBox();
            this.label_MilesTotal = new System.Windows.Forms.Label();
            this.textBox_Miles = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList_Directions = new System.Windows.Forms.ImageList(this.components);
            this.imageList_Leg = new System.Windows.Forms.ImageList(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage_Leg.SuspendLayout();
            this.groupBox_Leg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Hours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Miles)).BeginInit();
            this.tabPage_Totals.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_Leg);
            this.tabControl1.Controls.Add(this.tabPage_Totals);
            this.tabControl1.Location = new System.Drawing.Point(12, 66);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(374, 468);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage_Leg
            // 
            this.tabPage_Leg.Controls.Add(this.button_Add);
            this.tabPage_Leg.Controls.Add(this.groupBox_Leg);
            this.tabPage_Leg.Location = new System.Drawing.Point(8, 39);
            this.tabPage_Leg.Name = "tabPage_Leg";
            this.tabPage_Leg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Leg.Size = new System.Drawing.Size(358, 421);
            this.tabPage_Leg.TabIndex = 0;
            this.tabPage_Leg.Text = "Leg";
            this.tabPage_Leg.UseVisualStyleBackColor = true;
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(24, 337);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(312, 42);
            this.button_Add.TabIndex = 1;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // groupBox_Leg
            // 
            this.groupBox_Leg.Controls.Add(this.label_Mode);
            this.groupBox_Leg.Controls.Add(this.textBox_Mode);
            this.groupBox_Leg.Controls.Add(this.numericUpDown_Hours);
            this.groupBox_Leg.Controls.Add(this.label_Hours);
            this.groupBox_Leg.Controls.Add(this.numericUpDown_Miles);
            this.groupBox_Leg.Controls.Add(this.label_Miles);
            this.groupBox_Leg.Controls.Add(this.label_Direction);
            this.groupBox_Leg.Controls.Add(this.comboBox_Direction);
            this.groupBox_Leg.Location = new System.Drawing.Point(24, 53);
            this.groupBox_Leg.Name = "groupBox_Leg";
            this.groupBox_Leg.Size = new System.Drawing.Size(312, 256);
            this.groupBox_Leg.TabIndex = 0;
            this.groupBox_Leg.TabStop = false;
            this.groupBox_Leg.Text = "Leg";
            // 
            // label_Mode
            // 
            this.label_Mode.AutoSize = true;
            this.label_Mode.Location = new System.Drawing.Point(16, 188);
            this.label_Mode.Name = "label_Mode";
            this.label_Mode.Size = new System.Drawing.Size(66, 25);
            this.label_Mode.TabIndex = 11;
            this.label_Mode.Text = "Mode";
            // 
            // textBox_Mode
            // 
            this.textBox_Mode.Location = new System.Drawing.Point(136, 188);
            this.textBox_Mode.Name = "textBox_Mode";
            this.textBox_Mode.Size = new System.Drawing.Size(153, 31);
            this.textBox_Mode.TabIndex = 10;
            // 
            // numericUpDown_Hours
            // 
            this.numericUpDown_Hours.DecimalPlaces = 2;
            this.numericUpDown_Hours.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDown_Hours.Location = new System.Drawing.Point(136, 142);
            this.numericUpDown_Hours.Name = "numericUpDown_Hours";
            this.numericUpDown_Hours.Size = new System.Drawing.Size(153, 31);
            this.numericUpDown_Hours.TabIndex = 9;
            // 
            // label_Hours
            // 
            this.label_Hours.AutoSize = true;
            this.label_Hours.Location = new System.Drawing.Point(16, 144);
            this.label_Hours.Name = "label_Hours";
            this.label_Hours.Size = new System.Drawing.Size(69, 25);
            this.label_Hours.TabIndex = 8;
            this.label_Hours.Text = "Hours";
            // 
            // numericUpDown_Miles
            // 
            this.numericUpDown_Miles.DecimalPlaces = 2;
            this.numericUpDown_Miles.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.numericUpDown_Miles.Location = new System.Drawing.Point(136, 95);
            this.numericUpDown_Miles.Name = "numericUpDown_Miles";
            this.numericUpDown_Miles.Size = new System.Drawing.Size(153, 31);
            this.numericUpDown_Miles.TabIndex = 7;
            // 
            // label_Miles
            // 
            this.label_Miles.AutoSize = true;
            this.label_Miles.Location = new System.Drawing.Point(16, 97);
            this.label_Miles.Name = "label_Miles";
            this.label_Miles.Size = new System.Drawing.Size(63, 25);
            this.label_Miles.TabIndex = 6;
            this.label_Miles.Text = "Miles";
            // 
            // label_Direction
            // 
            this.label_Direction.AutoSize = true;
            this.label_Direction.Location = new System.Drawing.Point(16, 51);
            this.label_Direction.Name = "label_Direction";
            this.label_Direction.Size = new System.Drawing.Size(97, 25);
            this.label_Direction.TabIndex = 1;
            this.label_Direction.Text = "Direction";
            // 
            // comboBox_Direction
            // 
            this.comboBox_Direction.AccessibleDescription = "";
            this.comboBox_Direction.AccessibleName = "";
            this.comboBox_Direction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Direction.FormattingEnabled = true;
            this.comboBox_Direction.Items.AddRange(new object[] {
            "Nowhere",
            "East",
            "West",
            "North",
            "South"});
            this.comboBox_Direction.Location = new System.Drawing.Point(136, 48);
            this.comboBox_Direction.Name = "comboBox_Direction";
            this.comboBox_Direction.Size = new System.Drawing.Size(153, 33);
            this.comboBox_Direction.TabIndex = 4;
            this.comboBox_Direction.Tag = "";
            // 
            // tabPage_Totals
            // 
            this.tabPage_Totals.Controls.Add(this.label_LegTotal);
            this.tabPage_Totals.Controls.Add(this.textBox_Legs);
            this.tabPage_Totals.Controls.Add(this.label_HoursTotal);
            this.tabPage_Totals.Controls.Add(this.textBox_Hours);
            this.tabPage_Totals.Controls.Add(this.label_MilesTotal);
            this.tabPage_Totals.Controls.Add(this.textBox_Miles);
            this.tabPage_Totals.Location = new System.Drawing.Point(8, 39);
            this.tabPage_Totals.Name = "tabPage_Totals";
            this.tabPage_Totals.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Totals.Size = new System.Drawing.Size(358, 421);
            this.tabPage_Totals.TabIndex = 1;
            this.tabPage_Totals.Text = "Totals";
            this.tabPage_Totals.UseVisualStyleBackColor = true;
            // 
            // label_LegTotal
            // 
            this.label_LegTotal.AutoSize = true;
            this.label_LegTotal.Location = new System.Drawing.Point(34, 152);
            this.label_LegTotal.Name = "label_LegTotal";
            this.label_LegTotal.Size = new System.Drawing.Size(59, 25);
            this.label_LegTotal.TabIndex = 5;
            this.label_LegTotal.Text = "Legs";
            // 
            // textBox_Legs
            // 
            this.textBox_Legs.Location = new System.Drawing.Point(122, 149);
            this.textBox_Legs.Name = "textBox_Legs";
            this.textBox_Legs.ReadOnly = true;
            this.textBox_Legs.Size = new System.Drawing.Size(152, 31);
            this.textBox_Legs.TabIndex = 4;
            // 
            // label_HoursTotal
            // 
            this.label_HoursTotal.AutoSize = true;
            this.label_HoursTotal.Location = new System.Drawing.Point(34, 102);
            this.label_HoursTotal.Name = "label_HoursTotal";
            this.label_HoursTotal.Size = new System.Drawing.Size(69, 25);
            this.label_HoursTotal.TabIndex = 3;
            this.label_HoursTotal.Text = "Hours";
            // 
            // textBox_Hours
            // 
            this.textBox_Hours.Location = new System.Drawing.Point(122, 99);
            this.textBox_Hours.Name = "textBox_Hours";
            this.textBox_Hours.ReadOnly = true;
            this.textBox_Hours.Size = new System.Drawing.Size(152, 31);
            this.textBox_Hours.TabIndex = 2;
            // 
            // label_MilesTotal
            // 
            this.label_MilesTotal.AutoSize = true;
            this.label_MilesTotal.Location = new System.Drawing.Point(34, 54);
            this.label_MilesTotal.Name = "label_MilesTotal";
            this.label_MilesTotal.Size = new System.Drawing.Size(63, 25);
            this.label_MilesTotal.TabIndex = 1;
            this.label_MilesTotal.Text = "Miles";
            // 
            // textBox_Miles
            // 
            this.textBox_Miles.Location = new System.Drawing.Point(122, 51);
            this.textBox_Miles.Name = "textBox_Miles";
            this.textBox_Miles.ReadOnly = true;
            this.textBox_Miles.Size = new System.Drawing.Size(152, 31);
            this.textBox_Miles.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(864, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem_File
            // 
            this.toolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_New,
            this.toolStripMenuItem_Exit});
            this.toolStripMenuItem_File.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem_File.Name = "toolStripMenuItem_File";
            this.toolStripMenuItem_File.Size = new System.Drawing.Size(64, 36);
            this.toolStripMenuItem_File.Text = "File";
            // 
            // toolStripMenuItem_New
            // 
            this.toolStripMenuItem_New.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem_New.Name = "toolStripMenuItem_New";
            this.toolStripMenuItem_New.ShortcutKeyDisplayString = "Ctrl+N";
            this.toolStripMenuItem_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem_New.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_New.Text = "New";
            this.toolStripMenuItem_New.Click += new System.EventHandler(this.toolStripMenuItem_New_Click);
            // 
            // toolStripMenuItem_Exit
            // 
            this.toolStripMenuItem_Exit.Name = "toolStripMenuItem_Exit";
            this.toolStripMenuItem_Exit.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Exit.Text = "Exit";
            this.toolStripMenuItem_Exit.Click += new System.EventHandler(this.toolStripMenuItem_Exit_Click);
            // 
            // treeView1
            // 
            this.treeView1.ImageIndex = 5;
            this.treeView1.ImageList = this.imageList_Directions;
            this.treeView1.Location = new System.Drawing.Point(392, 74);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(458, 460);
            this.treeView1.StateImageList = this.imageList_Leg;
            this.treeView1.TabIndex = 0;
            // 
            // imageList_Directions
            // 
            this.imageList_Directions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Directions.ImageStream")));
            this.imageList_Directions.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Directions.Images.SetKeyName(0, "state.png");
            this.imageList_Directions.Images.SetKeyName(1, "east.jpg");
            this.imageList_Directions.Images.SetKeyName(2, "west.jpg");
            this.imageList_Directions.Images.SetKeyName(3, "north.jpg");
            this.imageList_Directions.Images.SetKeyName(4, "south.jpg");
            this.imageList_Directions.Images.SetKeyName(5, "miles.png");
            this.imageList_Directions.Images.SetKeyName(6, "hours.png");
            this.imageList_Directions.Images.SetKeyName(7, "mode.png");
            // 
            // imageList_Leg
            // 
            this.imageList_Leg.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Leg.ImageStream")));
            this.imageList_Leg.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Leg.Images.SetKeyName(0, "miles.png");
            this.imageList_Leg.Images.SetKeyName(1, "hours.png");
            this.imageList_Leg.Images.SetKeyName(2, "mode.png");
            // 
            // TravelPlanner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 553);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TravelPlanner";
            this.Text = "Travel Planner";
            this.tabControl1.ResumeLayout(false);
            this.tabPage_Leg.ResumeLayout(false);
            this.groupBox_Leg.ResumeLayout(false);
            this.groupBox_Leg.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Hours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Miles)).EndInit();
            this.tabPage_Totals.ResumeLayout(false);
            this.tabPage_Totals.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_Leg;
        private System.Windows.Forms.TabPage tabPage_Totals;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_New;
        private System.Windows.Forms.GroupBox groupBox_Leg;
        private System.Windows.Forms.Label label_Direction;
        private System.Windows.Forms.ComboBox comboBox_Direction;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Label label_Mode;
        private System.Windows.Forms.TextBox textBox_Mode;
        private System.Windows.Forms.NumericUpDown numericUpDown_Hours;
        private System.Windows.Forms.Label label_Hours;
        private System.Windows.Forms.NumericUpDown numericUpDown_Miles;
        private System.Windows.Forms.Label label_Miles;
        private System.Windows.Forms.Label label_MilesTotal;
        private System.Windows.Forms.TextBox textBox_Miles;
        private System.Windows.Forms.Label label_LegTotal;
        private System.Windows.Forms.TextBox textBox_Legs;
        private System.Windows.Forms.Label label_HoursTotal;
        private System.Windows.Forms.TextBox textBox_Hours;
        private System.Windows.Forms.ImageList imageList_Directions;
        private System.Windows.Forms.ImageList imageList_Leg;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Exit;
    }
}

