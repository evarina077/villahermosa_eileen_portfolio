﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE06
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE06: TreeView and TabControl
    // 03/13/2018


    // The main form
    public partial class TravelPlanner : Form
    {
        public TravelPlanner()
        {
            InitializeComponent();

            // To set default text display for drop down list
            comboBox_Direction.SelectedIndex = 0;
        }

        private void toolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            // To exit the program
            Application.Exit();
        }

        private void toolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            // To clear all data
            ResetAllInput();

            // To clear treeview
            treeView1.Nodes.Clear();

            // To clear totals
            textBox_Miles.Text = "";
            textBox_Hours.Text = "";
            textBox_Legs.Text = "";

            // To go back to default tab, Leg
            tabControl1.SelectTab(0);
        }

        private void ResetAllInput()
        {
            // To clear data input
            comboBox_Direction.SelectedIndex = 0;
            numericUpDown_Miles.Value = 0;
            numericUpDown_Hours.Value = 0;
            textBox_Mode.Text = "";

        }

        decimal numMiles = 0;
        decimal numHours = 0;

        private void button_Add_Click(object sender, EventArgs e)
        {
            // Call to BuildTree method
            BuildTree();
        }

        private void BuildTree()
        {
            // To instantiate the Leg class and add get/set all data input
            Leg legData = new Leg();
    
            legData.ComboBox_Direction = comboBox_Direction.Text;
            legData.Number_Miles = numericUpDown_Miles.Value;
            legData.Number_Hours = numericUpDown_Hours.Value;
            legData.Mode = textBox_Mode.Text;

            if(comboBox_Direction.SelectedIndex != 0)
            {
                // To instantiate the TreeNode class and set the nodes
                // for the direction data
                TreeNode directionNode = new TreeNode();

                // To set the image when a node is selected
                directionNode.SelectedImageIndex = 0;

                // To set the first node with each direction
                directionNode.Text = legData.ComboBox_Direction;

                if(comboBox_Direction.Text == "East")
                {
                    directionNode.ImageIndex = 1;
                }
                else if (comboBox_Direction.Text == "West")
                {
                    directionNode.ImageIndex = 2;
                }
                else if (comboBox_Direction.Text == "North")
                {
                    directionNode.ImageIndex = 3;
                }
                else if (comboBox_Direction.Text == "South")
                {
                    directionNode.ImageIndex = 4;
                }

                // New node for miles data
                TreeNode mileNode = new TreeNode();
                mileNode.ImageIndex = 5;
                mileNode.Text = ($"Miles: {legData.Number_Miles}");
                mileNode.SelectedImageIndex = 0;

                // New node for hours data
                TreeNode hourNode = new TreeNode();
                hourNode.ImageIndex = 6;
                hourNode.Text = ($"Hours: {legData.Number_Hours}");
                hourNode.SelectedImageIndex = 0;

                // New node for mode data
                TreeNode modeNode = new TreeNode();
                modeNode.ImageIndex = 7;
                modeNode.Text = ($"Modes: {legData.Mode}");
                modeNode.SelectedImageIndex = 0;

                // Set nodes to the first node created
                directionNode.Nodes.Add(mileNode);
                directionNode.Nodes.Add(hourNode);
                directionNode.Nodes.Add(modeNode);

                // Adding parent node to the the treeview
                treeView1.Nodes.Add(directionNode);

                // to calculate the total values of miles
                numMiles += numericUpDown_Miles.Value;
                textBox_Miles.Text = numMiles.ToString();

                // to calculate the total values of hours
                numHours += numericUpDown_Hours.Value;
                textBox_Hours.Text = numHours.ToString();

                // to count the amount of leg data set added
                textBox_Legs.Text = treeView1.GetNodeCount(false).ToString();
            }
        }
    }
}
