﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE03
{
    public partial class Form1 : Form
    {


        //list to hold all my values 
        public static List<Weight> weightList = new List<Weight>();

        //this will let me know if display is checked
        private bool Displaychecked = false;


        //EventHandlers
        //this helps me add to list
        public event EventHandler AddToList;
        //this is the clear button for the main form
        public event EventHandler MainClear;
        public event EventHandler ClearAll;
        // public event EventHandler Clearall;


        public Form1()
        {
            InitializeComponent();
        }



        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exit the program
            Application.Exit();
        }

        //forming my data
        Weight data
        {
            //get information
            get
            {
                Weight Data = new Weight();

                Data.Name = nameTextBox.Text;
                Data.userWeight = weightNumericUpDown.Value;
                Data.maleCheck = maleButton.Checked;
                Data.femaleCheck = femaleButton.Checked;
                Data.date = dateTimePicker.Text;

                return Data;
            }

            set {
                //sets all values 
                if (value != null)
                {
                    //makes sure the item is not void to stop exception errors
                    nameTextBox.Text = value.Name;
                    weightNumericUpDown.Value = value.userWeight;
                    maleButton.Checked = value.maleCheck;
                    femaleButton.Checked = value.femaleCheck;
                    dateTimePicker.Text = value.date;


                }
            }

        }



        // method created to clear all when needed
        public void ResetAll()
        {
            nameTextBox.Clear();
            weightNumericUpDown.Value = 0;
            dateTimePicker.Value = DateTime.Today;
            maleButton.Checked = false;
            femaleButton.Checked = false;
        }


        // this will call to the reset method
        private void resetButton_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        public void Uncheck()
        {
            displayToolStripMenuItem.Checked = false;
        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // instantiate the listForm
            listForm newWindow = new listForm(); //opening my other form

            if (displayToolStripMenuItem.Checked == false) //checking to see if my form 2 is checked
            {
                displayToolStripMenuItem.CheckState = CheckState.Checked;
            }
            Displaychecked = true; //changes my bool for the submit button


            //Subscription
            MainClear += newWindow._MainClear; //subcribe
            AddToList += newWindow._AddToList;

            newWindow.CloseApp += _CloseApp;

            newWindow.DisplayValues += _DisplayValues;
            newWindow.ClearAll += _ClearAll;
            newWindow.Show(); //modeless open



        }

        private void addButton_Click(object sender, EventArgs e)
        {

            weightList.Add(data); //adding my values to the list

            if (Displaychecked != false) //checking to see if the display option has been selected
            {
                AddToList(this, new EventArgs()); //uses event handler to add the list
            }


            ResetAll(); //reset user inputs
        }

        private void _DisplayValues(object sender, EventArgs e) //a custom event to populate the values in my main window
        {
            Weight values = (Weight)(((ListBox)sender).SelectedItem); //grabs the values for the selected person


            data = values; //places those values correctly 
        }

        private void _ClearAll(object sender, EventArgs e)
        {
            ResetAll();
            weightList = new List<Weight>();
            // listForm.userDate_listBox.Items.Clear();

        }

        private void _CloseApp(object sender, EventArgs e)
        {
            displayToolStripMenuItem.CheckState = CheckState.Unchecked; //changes my checked state

        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {

            listForm userDate_listBox_Form2 = new listForm();

            MainClear?.Invoke(this, new EventArgs());
            userDate_listBox_Form2.userDate_listBox.Items.Clear();

            // -- Clear User imput
            ResetAll();

        }
    }
}
