﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EileenVillahermosa_CE03
{
    public class Weight
    {
        //public string nameTextBox;
        //public decimal weightNumericUpDown;
        //public bool maleButton;
        //public bool femaleButton;
        //public string dateTimePicker;

        public string Name;
        public decimal userWeight;
        public bool maleCheck;
        public bool femaleCheck;
        public string date;

        public override string ToString()
        {
            return Name + " - " + userWeight + " - " + date;
        }

    }
}
