﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EileenVillahermosa_CE03
{
    public partial class listForm : Form
    {
        //event handlers
        public event EventHandler DisplayValues;

        // public event EventHandler ClearEvent;
        public event EventHandler ClearAll;
        public event EventHandler CloseApp;
        


        public listForm()
        {
            InitializeComponent();
            //this makes sure that the list is populated on startup
            addWeightData();
        }

        public void clearToolStripButton_Click(object sender, EventArgs e)
        {
            //clears everything in the list 
            ClearAll(this, new EventArgs());
            //clears the serDate_listBox
            userDate_listBox.Items.Clear(); 
        }

       
        public void addWeightData()
        {
            userDate_listBox.Items.Clear();
            foreach (object userWeight in Form1.weightList)
            {
                userDate_listBox.Items.Add((Weight)userWeight);
            }
        }

        public void userDate_listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayValues(sender, new EventArgs());
        }

        //adds to my list in real time
        public void _AddToList(object sender, EventArgs e) 
        {
            userDate_listBox.Items.Clear();
            foreach (object userWeight in Form1.weightList)
            {
                userDate_listBox.Items.Add((Weight)userWeight);
            }
        }

        public void DisplayForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 clearCheck = new Form1();

            clearCheck.Uncheck();
            //makes sure the form is closed
            CloseApp(this, new EventArgs());

             
     
        }

       
        public void _MainClear(object sender, EventArgs e)
        {
            // ClearEvent?.Invoke(this, new EventArgs());
            //clears everything
            userDate_listBox.Items.Clear(); 

        }

    }
}
