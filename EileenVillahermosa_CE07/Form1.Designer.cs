﻿namespace EileenVillahermosa_CE07
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE07: JSON and Web Connectivity
    // 03/14/2018


    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Load = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_MDVCourseData = new System.Windows.Forms.GroupBox();
            this.numericUpDown_courseMonth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_creditHours = new System.Windows.Forms.NumericUpDown();
            this.button_Display = new System.Windows.Forms.Button();
            this.label_courseMonth = new System.Windows.Forms.Label();
            this.label_creditHours = new System.Windows.Forms.Label();
            this.label_courseCode = new System.Windows.Forms.Label();
            this.textBox_courseCode = new System.Windows.Forms.TextBox();
            this.label_courseName = new System.Windows.Forms.Label();
            this.textBox_courseName = new System.Windows.Forms.TextBox();
            this.textBox_courseDescription = new System.Windows.Forms.TextBox();
            this.label_courseDescription = new System.Windows.Forms.Label();
            this.label_classWebSource = new System.Windows.Forms.Label();
            this.radioButton_VFW = new System.Windows.Forms.RadioButton();
            this.radioButton_ASD = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox_MDVCourseData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_courseMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_creditHours)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(900, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem_File
            // 
            this.toolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_New,
            this.toolStripMenuItem_Save,
            this.toolStripMenuItem_Load,
            this.toolStripMenuItem_Exit});
            this.toolStripMenuItem_File.Name = "toolStripMenuItem_File";
            this.toolStripMenuItem_File.Size = new System.Drawing.Size(64, 36);
            this.toolStripMenuItem_File.Text = "File";
            // 
            // toolStripMenuItem_New
            // 
            this.toolStripMenuItem_New.Name = "toolStripMenuItem_New";
            this.toolStripMenuItem_New.ShortcutKeyDisplayString = "Ctrl+N";
            this.toolStripMenuItem_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem_New.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_New.Text = "New";
            this.toolStripMenuItem_New.Click += new System.EventHandler(this.toolStripMenuItem_New_Click);
            // 
            // toolStripMenuItem_Save
            // 
            this.toolStripMenuItem_Save.Name = "toolStripMenuItem_Save";
            this.toolStripMenuItem_Save.ShortcutKeyDisplayString = "Ctrl+S";
            this.toolStripMenuItem_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_Save.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Save.Text = "Save";
            this.toolStripMenuItem_Save.Click += new System.EventHandler(this.toolStripMenuItem_Save_Click);
            // 
            // toolStripMenuItem_Load
            // 
            this.toolStripMenuItem_Load.Name = "toolStripMenuItem_Load";
            this.toolStripMenuItem_Load.ShortcutKeyDisplayString = "Ctrl+L";
            this.toolStripMenuItem_Load.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem_Load.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Load.Text = "Load";
            this.toolStripMenuItem_Load.Click += new System.EventHandler(this.toolStripMenuItem_Load_Click);
            // 
            // toolStripMenuItem_Exit
            // 
            this.toolStripMenuItem_Exit.Name = "toolStripMenuItem_Exit";
            this.toolStripMenuItem_Exit.ShortcutKeyDisplayString = "Ctrl+Q";
            this.toolStripMenuItem_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolStripMenuItem_Exit.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Exit.Text = "Exit";
            this.toolStripMenuItem_Exit.Click += new System.EventHandler(this.toolStripMenuItem_Exit_Click);
            // 
            // groupBox_MDVCourseData
            // 
            this.groupBox_MDVCourseData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox_MDVCourseData.Controls.Add(this.numericUpDown_courseMonth);
            this.groupBox_MDVCourseData.Controls.Add(this.numericUpDown_creditHours);
            this.groupBox_MDVCourseData.Controls.Add(this.button_Display);
            this.groupBox_MDVCourseData.Controls.Add(this.label_courseMonth);
            this.groupBox_MDVCourseData.Controls.Add(this.label_creditHours);
            this.groupBox_MDVCourseData.Controls.Add(this.label_courseCode);
            this.groupBox_MDVCourseData.Controls.Add(this.textBox_courseCode);
            this.groupBox_MDVCourseData.Controls.Add(this.label_courseName);
            this.groupBox_MDVCourseData.Controls.Add(this.textBox_courseName);
            this.groupBox_MDVCourseData.Controls.Add(this.textBox_courseDescription);
            this.groupBox_MDVCourseData.Controls.Add(this.label_courseDescription);
            this.groupBox_MDVCourseData.Controls.Add(this.label_classWebSource);
            this.groupBox_MDVCourseData.Controls.Add(this.radioButton_VFW);
            this.groupBox_MDVCourseData.Controls.Add(this.radioButton_ASD);
            this.groupBox_MDVCourseData.Location = new System.Drawing.Point(31, 66);
            this.groupBox_MDVCourseData.Name = "groupBox_MDVCourseData";
            this.groupBox_MDVCourseData.Size = new System.Drawing.Size(842, 589);
            this.groupBox_MDVCourseData.TabIndex = 1;
            this.groupBox_MDVCourseData.TabStop = false;
            this.groupBox_MDVCourseData.Text = "Get MDV Course Data";
            // 
            // numericUpDown_courseMonth
            // 
            this.numericUpDown_courseMonth.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_courseMonth.InterceptArrowKeys = false;
            this.numericUpDown_courseMonth.Location = new System.Drawing.Point(307, 526);
            this.numericUpDown_courseMonth.Name = "numericUpDown_courseMonth";
            this.numericUpDown_courseMonth.ReadOnly = true;
            this.numericUpDown_courseMonth.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_courseMonth.TabIndex = 13;
            // 
            // numericUpDown_creditHours
            // 
            this.numericUpDown_creditHours.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_creditHours.InterceptArrowKeys = false;
            this.numericUpDown_creditHours.Location = new System.Drawing.Point(307, 474);
            this.numericUpDown_creditHours.Name = "numericUpDown_creditHours";
            this.numericUpDown_creditHours.ReadOnly = true;
            this.numericUpDown_creditHours.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_creditHours.TabIndex = 12;
            // 
            // button_Display
            // 
            this.button_Display.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Display.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_Display.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Display.Location = new System.Drawing.Point(307, 111);
            this.button_Display.Name = "button_Display";
            this.button_Display.Size = new System.Drawing.Size(309, 65);
            this.button_Display.TabIndex = 11;
            this.button_Display.Text = "Download and Display Data";
            this.button_Display.UseVisualStyleBackColor = false;
            this.button_Display.Click += new System.EventHandler(this.button_Display_Click);
            // 
            // label_courseMonth
            // 
            this.label_courseMonth.AutoSize = true;
            this.label_courseMonth.Location = new System.Drawing.Point(127, 528);
            this.label_courseMonth.Name = "label_courseMonth";
            this.label_courseMonth.Size = new System.Drawing.Size(153, 25);
            this.label_courseMonth.TabIndex = 10;
            this.label_courseMonth.Text = "Course Month:";
            // 
            // label_creditHours
            // 
            this.label_creditHours.AutoSize = true;
            this.label_creditHours.Location = new System.Drawing.Point(142, 474);
            this.label_creditHours.Name = "label_creditHours";
            this.label_creditHours.Size = new System.Drawing.Size(138, 25);
            this.label_creditHours.TabIndex = 9;
            this.label_creditHours.Text = "Credit Hours:";
            // 
            // label_courseCode
            // 
            this.label_courseCode.AutoSize = true;
            this.label_courseCode.Location = new System.Drawing.Point(142, 290);
            this.label_courseCode.Name = "label_courseCode";
            this.label_courseCode.Size = new System.Drawing.Size(144, 25);
            this.label_courseCode.TabIndex = 8;
            this.label_courseCode.Text = "Course Code:";
            // 
            // textBox_courseCode
            // 
            this.textBox_courseCode.Location = new System.Drawing.Point(307, 287);
            this.textBox_courseCode.Name = "textBox_courseCode";
            this.textBox_courseCode.ReadOnly = true;
            this.textBox_courseCode.Size = new System.Drawing.Size(414, 31);
            this.textBox_courseCode.TabIndex = 7;
            // 
            // label_courseName
            // 
            this.label_courseName.AutoSize = true;
            this.label_courseName.Location = new System.Drawing.Point(142, 231);
            this.label_courseName.Name = "label_courseName";
            this.label_courseName.Size = new System.Drawing.Size(149, 25);
            this.label_courseName.TabIndex = 6;
            this.label_courseName.Text = "Course Name:";
            // 
            // textBox_courseName
            // 
            this.textBox_courseName.Location = new System.Drawing.Point(307, 228);
            this.textBox_courseName.Name = "textBox_courseName";
            this.textBox_courseName.ReadOnly = true;
            this.textBox_courseName.Size = new System.Drawing.Size(414, 31);
            this.textBox_courseName.TabIndex = 5;
            // 
            // textBox_courseDescription
            // 
            this.textBox_courseDescription.AcceptsReturn = true;
            this.textBox_courseDescription.AcceptsTab = true;
            this.textBox_courseDescription.Location = new System.Drawing.Point(307, 346);
            this.textBox_courseDescription.Multiline = true;
            this.textBox_courseDescription.Name = "textBox_courseDescription";
            this.textBox_courseDescription.ReadOnly = true;
            this.textBox_courseDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_courseDescription.Size = new System.Drawing.Size(414, 105);
            this.textBox_courseDescription.TabIndex = 4;
            // 
            // label_courseDescription
            // 
            this.label_courseDescription.AutoSize = true;
            this.label_courseDescription.Location = new System.Drawing.Point(90, 349);
            this.label_courseDescription.Name = "label_courseDescription";
            this.label_courseDescription.Size = new System.Drawing.Size(201, 25);
            this.label_courseDescription.TabIndex = 3;
            this.label_courseDescription.Text = "Course Description:";
            // 
            // label_classWebSource
            // 
            this.label_classWebSource.AutoSize = true;
            this.label_classWebSource.Location = new System.Drawing.Point(103, 64);
            this.label_classWebSource.Name = "label_classWebSource";
            this.label_classWebSource.Size = new System.Drawing.Size(188, 25);
            this.label_classWebSource.TabIndex = 2;
            this.label_classWebSource.Text = "Choose the Class:";
            // 
            // radioButton_VFW
            // 
            this.radioButton_VFW.AutoSize = true;
            this.radioButton_VFW.Location = new System.Drawing.Point(399, 62);
            this.radioButton_VFW.Name = "radioButton_VFW";
            this.radioButton_VFW.Size = new System.Drawing.Size(90, 29);
            this.radioButton_VFW.TabIndex = 1;
            this.radioButton_VFW.TabStop = true;
            this.radioButton_VFW.Text = "VFW";
            this.radioButton_VFW.UseVisualStyleBackColor = true;
            // 
            // radioButton_ASD
            // 
            this.radioButton_ASD.AutoSize = true;
            this.radioButton_ASD.Location = new System.Drawing.Point(307, 62);
            this.radioButton_ASD.Name = "radioButton_ASD";
            this.radioButton_ASD.Size = new System.Drawing.Size(86, 29);
            this.radioButton_ASD.TabIndex = 0;
            this.radioButton_ASD.TabStop = true;
            this.radioButton_ASD.Text = "ASD";
            this.radioButton_ASD.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 717);
            this.Controls.Add(this.groupBox_MDVCourseData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox_MDVCourseData.ResumeLayout(false);
            this.groupBox_MDVCourseData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_courseMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_creditHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Save;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Load;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Exit;
        private System.Windows.Forms.GroupBox groupBox_MDVCourseData;
        private System.Windows.Forms.TextBox textBox_courseDescription;
        private System.Windows.Forms.Label label_courseDescription;
        private System.Windows.Forms.Label label_classWebSource;
        private System.Windows.Forms.RadioButton radioButton_VFW;
        private System.Windows.Forms.RadioButton radioButton_ASD;
        private System.Windows.Forms.TextBox textBox_courseName;
        private System.Windows.Forms.Label label_courseCode;
        private System.Windows.Forms.TextBox textBox_courseCode;
        private System.Windows.Forms.Label label_courseName;
        private System.Windows.Forms.Button button_Display;
        private System.Windows.Forms.Label label_courseMonth;
        private System.Windows.Forms.Label label_creditHours;
        private System.Windows.Forms.NumericUpDown numericUpDown_courseMonth;
        private System.Windows.Forms.NumericUpDown numericUpDown_creditHours;
    }
}

