﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace EileenVillahermosa_CE07
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE07: JSON and Web Connectivity
    // 03/14/2018


    public partial class Form1 : Form
    {
        // add instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();

        // api starting point
        string apiStartPoint = "http://mdv-vfw.com";
        // api ending point
        string apiEndPoint;

        // to check internet connection
        // this will prevent from the program crashing
        public static bool CheckConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                MessageBox.Show("Sorry no internet connection. \r\nPlease check and try again.");
                return false;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ResetAll()
        {
            // to reset all input controls
            textBox_courseName.Clear();
            textBox_courseCode.Clear();
            textBox_courseDescription.Clear();
            numericUpDown_creditHours.Value = 0;
            numericUpDown_courseMonth.Value = 0;
        }

        private void toolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            // to call to reset method
            ResetAll();
        }

        private void BuildAPI()
        {

            if (radioButton_VFW.Checked == false)
            {
                // to complete the api
                apiEndPoint = apiStartPoint + "/asd.json";
            }
            else
            {
                // to complete the api
                apiEndPoint = apiStartPoint + "/vfw.json";
            }
        }

        private void GetJSONData()
        {
            // download the data as a string
            var apiData = apiConnection.DownloadString(apiEndPoint);

            // convert the data string to an object
            JObject apiDataObj = JObject.Parse(apiData);

            // find the specific data in the object and view in the input controls
            string courseName = apiDataObj["class"]["course_name_clean"].ToString();
            string courseCode = apiDataObj["class"]["course_code_long"].ToString();
            string courseDescription = apiDataObj["class"]["course_description"].ToString();
            string creditHours = apiDataObj["class"]["credit"].ToString();
            int creditHoursNum = Convert.ToInt32(creditHours);
            string courseMonth = apiDataObj["class"]["sequence"].ToString();
            int courseMonthNum = Convert.ToInt32(courseMonth);


            // view the objects as strings
            textBox_courseName.Text = courseName;
            textBox_courseCode.Text = courseCode;
            textBox_courseDescription.Text = courseDescription;
            numericUpDown_creditHours.Value = creditHoursNum;
            numericUpDown_courseMonth.Value = courseMonthNum;
        }

        // method to determine which data to pull
        private void InputTheData()
        {
            switch(apiEndPoint)
            {
                case "http://mdv-vfw.com/asd.json":
                    GetJSONData();
                    break;

                case "http://mdv-vfw.com/vfw.json":
                    GetJSONData();
                    break;
            }
        }


        private void button_Display_Click(object sender, EventArgs e)
        {
            // to call to check connection method
            CheckConnection();

            // to call to build the api method
            BuildAPI();

            // to coall to input the data method
            InputTheData();
        }


        private void toolStripMenuItem_Save_Click(object sender, EventArgs e)
        {
            // to use the save file dialog
            SaveFileDialog saveFile = new SaveFileDialog();

            // to check if there is data to save
            // if there is it will start the save process
            if(textBox_courseName.Text == "")
            {
                MessageBox.Show("No data to save");
            }

            else
            {
                // filter created to have file saved as .txt
                saveFile.Filter = "JSON (.txt)|*.txt";

                // string variable creted for indentifier
                string identifier = "SavedJsonFile";

                // to check user clicked OK
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    // to start the writer
                    using (StreamWriter Input = new StreamWriter(saveFile.FileName))
                    {
                        // this is the format to save all data input including it's identifier
                        Input.WriteLine(identifier + "/" + textBox_courseName.Text + "/" + textBox_courseCode.Text + "/" + textBox_courseDescription.Text + "/" + numericUpDown_creditHours.Value.ToString() + "/" + numericUpDown_courseMonth.Value.ToString() + "/" + identifier);

                        // to call to clear method
                        ResetAll();
                    }
                }
            }
        }


        private void toolStripMenuItem_Load_Click(object sender, EventArgs e)
        {
            // to call to clear method
            ResetAll();

            // to use the oepn file dialog
            OpenFileDialog openFile = new OpenFileDialog();

            // to check user clicked OK
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                // to start the reader
                using (StreamReader info = new StreamReader(openFile.FileName))
                {
                    // string created to read each line
                    string open = info.ReadLine();

                    // placed string data into an array
                    string[] fileOpen = open.Split('/');

                    // to check if user opens the correct data file by checking
                    // if file has the identifier, if not it will throw a message
                    if (fileOpen[0] != "SavedJsonFile")
                    {
                        MessageBox.Show("Sorry this is not the correct data file");
                        return;
                    }
                    else
                    {
                        textBox_courseName.Text = fileOpen[1];
                        textBox_courseCode.Text = fileOpen[2];
                        textBox_courseDescription.Text = fileOpen[3];
                        numericUpDown_creditHours.Value = Decimal.Parse(fileOpen[4]);
                        numericUpDown_courseMonth.Value = Decimal.Parse(fileOpen[5]);
                    }

                }

            }
        }
        
    }
}
