﻿namespace EileenVillahermosa_CE08
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE08: XML and File I/O
    // 03/16/2018

    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_New = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Load = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Save = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_DisplayCourseData = new System.Windows.Forms.GroupBox();
            this.numericUpDown_courseMonth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_creditHours = new System.Windows.Forms.NumericUpDown();
            this.label_courseMonth = new System.Windows.Forms.Label();
            this.label_creditHours = new System.Windows.Forms.Label();
            this.label_courseCode = new System.Windows.Forms.Label();
            this.textBox_courseCode = new System.Windows.Forms.TextBox();
            this.label_courseName = new System.Windows.Forms.Label();
            this.textBox_courseName = new System.Windows.Forms.TextBox();
            this.textBox_courseDescription = new System.Windows.Forms.TextBox();
            this.label_courseDescription = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButton_XML = new System.Windows.Forms.RadioButton();
            this.radioButton_JSON = new System.Windows.Forms.RadioButton();
            this.button_Display = new System.Windows.Forms.Button();
            this.label_classWebSource = new System.Windows.Forms.Label();
            this.label_saveFileType = new System.Windows.Forms.Label();
            this.radioButton_saveXML = new System.Windows.Forms.RadioButton();
            this.radioButton_saveJSON = new System.Windows.Forms.RadioButton();
            this.groupBox_saveCourseData = new System.Windows.Forms.GroupBox();
            this.groupBox_getMDVCourseData = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton_VFW = new System.Windows.Forms.RadioButton();
            this.radioButton_ASD = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            this.groupBox_DisplayCourseData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_courseMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_creditHours)).BeginInit();
            this.groupBox_saveCourseData.SuspendLayout();
            this.groupBox_getMDVCourseData.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(906, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem_File
            // 
            this.toolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_New,
            this.toolStripSeparator1,
            this.toolStripMenuItem_Load,
            this.toolStripSeparator2,
            this.toolStripMenuItem_Save,
            this.toolStripSeparator3,
            this.toolStripMenuItem_Exit});
            this.toolStripMenuItem_File.Name = "toolStripMenuItem_File";
            this.toolStripMenuItem_File.Size = new System.Drawing.Size(64, 36);
            this.toolStripMenuItem_File.Text = "File";
            // 
            // toolStripMenuItem_New
            // 
            this.toolStripMenuItem_New.Name = "toolStripMenuItem_New";
            this.toolStripMenuItem_New.ShortcutKeyDisplayString = "Ctrl+N";
            this.toolStripMenuItem_New.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItem_New.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_New.Text = "New";
            this.toolStripMenuItem_New.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem_New.Click += new System.EventHandler(this.toolStripMenuItem_New_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(245, 6);
            // 
            // toolStripMenuItem_Load
            // 
            this.toolStripMenuItem_Load.Name = "toolStripMenuItem_Load";
            this.toolStripMenuItem_Load.ShortcutKeyDisplayString = "Ctrl+L";
            this.toolStripMenuItem_Load.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.toolStripMenuItem_Load.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Load.Text = "Load";
            this.toolStripMenuItem_Load.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem_Load.Click += new System.EventHandler(this.toolStripMenuItem_Load_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(245, 6);
            // 
            // toolStripMenuItem_Save
            // 
            this.toolStripMenuItem_Save.Name = "toolStripMenuItem_Save";
            this.toolStripMenuItem_Save.ShortcutKeyDisplayString = "Ctrl+S";
            this.toolStripMenuItem_Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_Save.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Save.Text = "Save";
            this.toolStripMenuItem_Save.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem_Save.Click += new System.EventHandler(this.toolStripMenuItem_Save_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(245, 6);
            // 
            // toolStripMenuItem_Exit
            // 
            this.toolStripMenuItem_Exit.Name = "toolStripMenuItem_Exit";
            this.toolStripMenuItem_Exit.ShortcutKeyDisplayString = "Ctrl+Q";
            this.toolStripMenuItem_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolStripMenuItem_Exit.Size = new System.Drawing.Size(248, 38);
            this.toolStripMenuItem_Exit.Text = "Exit";
            this.toolStripMenuItem_Exit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem_Exit.Click += new System.EventHandler(this.toolStripMenuItem_Exit_Click);
            // 
            // groupBox_DisplayCourseData
            // 
            this.groupBox_DisplayCourseData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox_DisplayCourseData.Controls.Add(this.numericUpDown_courseMonth);
            this.groupBox_DisplayCourseData.Controls.Add(this.numericUpDown_creditHours);
            this.groupBox_DisplayCourseData.Controls.Add(this.label_courseMonth);
            this.groupBox_DisplayCourseData.Controls.Add(this.label_creditHours);
            this.groupBox_DisplayCourseData.Controls.Add(this.label_courseCode);
            this.groupBox_DisplayCourseData.Controls.Add(this.textBox_courseCode);
            this.groupBox_DisplayCourseData.Controls.Add(this.label_courseName);
            this.groupBox_DisplayCourseData.Controls.Add(this.textBox_courseName);
            this.groupBox_DisplayCourseData.Controls.Add(this.textBox_courseDescription);
            this.groupBox_DisplayCourseData.Controls.Add(this.label_courseDescription);
            this.groupBox_DisplayCourseData.Location = new System.Drawing.Point(31, 346);
            this.groupBox_DisplayCourseData.Name = "groupBox_DisplayCourseData";
            this.groupBox_DisplayCourseData.Size = new System.Drawing.Size(842, 418);
            this.groupBox_DisplayCourseData.TabIndex = 1;
            this.groupBox_DisplayCourseData.TabStop = false;
            this.groupBox_DisplayCourseData.Text = "Display Course Data";
            // 
            // numericUpDown_courseMonth
            // 
            this.numericUpDown_courseMonth.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_courseMonth.InterceptArrowKeys = false;
            this.numericUpDown_courseMonth.Location = new System.Drawing.Point(317, 360);
            this.numericUpDown_courseMonth.Name = "numericUpDown_courseMonth";
            this.numericUpDown_courseMonth.ReadOnly = true;
            this.numericUpDown_courseMonth.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_courseMonth.TabIndex = 13;
            // 
            // numericUpDown_creditHours
            // 
            this.numericUpDown_creditHours.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_creditHours.InterceptArrowKeys = false;
            this.numericUpDown_creditHours.Location = new System.Drawing.Point(317, 308);
            this.numericUpDown_creditHours.Name = "numericUpDown_creditHours";
            this.numericUpDown_creditHours.ReadOnly = true;
            this.numericUpDown_creditHours.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_creditHours.TabIndex = 12;
            // 
            // label_courseMonth
            // 
            this.label_courseMonth.AutoSize = true;
            this.label_courseMonth.Location = new System.Drawing.Point(143, 362);
            this.label_courseMonth.Name = "label_courseMonth";
            this.label_courseMonth.Size = new System.Drawing.Size(153, 25);
            this.label_courseMonth.TabIndex = 10;
            this.label_courseMonth.Text = "Course Month:";
            // 
            // label_creditHours
            // 
            this.label_creditHours.AutoSize = true;
            this.label_creditHours.Location = new System.Drawing.Point(158, 310);
            this.label_creditHours.Name = "label_creditHours";
            this.label_creditHours.Size = new System.Drawing.Size(138, 25);
            this.label_creditHours.TabIndex = 9;
            this.label_creditHours.Text = "Credit Hours:";
            // 
            // label_courseCode
            // 
            this.label_courseCode.AutoSize = true;
            this.label_courseCode.Location = new System.Drawing.Point(152, 124);
            this.label_courseCode.Name = "label_courseCode";
            this.label_courseCode.Size = new System.Drawing.Size(144, 25);
            this.label_courseCode.TabIndex = 8;
            this.label_courseCode.Text = "Course Code:";
            // 
            // textBox_courseCode
            // 
            this.textBox_courseCode.Location = new System.Drawing.Point(317, 121);
            this.textBox_courseCode.Name = "textBox_courseCode";
            this.textBox_courseCode.ReadOnly = true;
            this.textBox_courseCode.Size = new System.Drawing.Size(414, 31);
            this.textBox_courseCode.TabIndex = 7;
            // 
            // label_courseName
            // 
            this.label_courseName.AutoSize = true;
            this.label_courseName.Location = new System.Drawing.Point(152, 65);
            this.label_courseName.Name = "label_courseName";
            this.label_courseName.Size = new System.Drawing.Size(149, 25);
            this.label_courseName.TabIndex = 6;
            this.label_courseName.Text = "Course Name:";
            // 
            // textBox_courseName
            // 
            this.textBox_courseName.Location = new System.Drawing.Point(317, 62);
            this.textBox_courseName.Name = "textBox_courseName";
            this.textBox_courseName.ReadOnly = true;
            this.textBox_courseName.Size = new System.Drawing.Size(414, 31);
            this.textBox_courseName.TabIndex = 5;
            // 
            // textBox_courseDescription
            // 
            this.textBox_courseDescription.AcceptsReturn = true;
            this.textBox_courseDescription.AcceptsTab = true;
            this.textBox_courseDescription.Location = new System.Drawing.Point(317, 180);
            this.textBox_courseDescription.Multiline = true;
            this.textBox_courseDescription.Name = "textBox_courseDescription";
            this.textBox_courseDescription.ReadOnly = true;
            this.textBox_courseDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_courseDescription.Size = new System.Drawing.Size(414, 105);
            this.textBox_courseDescription.TabIndex = 4;
            // 
            // label_courseDescription
            // 
            this.label_courseDescription.AutoSize = true;
            this.label_courseDescription.Location = new System.Drawing.Point(100, 183);
            this.label_courseDescription.Name = "label_courseDescription";
            this.label_courseDescription.Size = new System.Drawing.Size(201, 25);
            this.label_courseDescription.TabIndex = 3;
            this.label_courseDescription.Text = "Course Description:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(307, 25);
            this.label1.TabIndex = 16;
            this.label1.Text = "Choose the file type to access:";
            // 
            // radioButton_XML
            // 
            this.radioButton_XML.AutoSize = true;
            this.radioButton_XML.Location = new System.Drawing.Point(595, 121);
            this.radioButton_XML.Name = "radioButton_XML";
            this.radioButton_XML.Size = new System.Drawing.Size(87, 29);
            this.radioButton_XML.TabIndex = 15;
            this.radioButton_XML.Text = "XML";
            this.radioButton_XML.UseVisualStyleBackColor = true;
            // 
            // radioButton_JSON
            // 
            this.radioButton_JSON.AutoSize = true;
            this.radioButton_JSON.Location = new System.Drawing.Point(459, 121);
            this.radioButton_JSON.Name = "radioButton_JSON";
            this.radioButton_JSON.Size = new System.Drawing.Size(99, 29);
            this.radioButton_JSON.TabIndex = 14;
            this.radioButton_JSON.Text = "JSON";
            this.radioButton_JSON.UseVisualStyleBackColor = true;
            // 
            // button_Display
            // 
            this.button_Display.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Display.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button_Display.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Display.Location = new System.Drawing.Point(105, 179);
            this.button_Display.Name = "button_Display";
            this.button_Display.Size = new System.Drawing.Size(626, 65);
            this.button_Display.TabIndex = 11;
            this.button_Display.Text = "Download and Display Data";
            this.button_Display.UseVisualStyleBackColor = false;
            this.button_Display.Click += new System.EventHandler(this.button_Display_Click);
            // 
            // label_classWebSource
            // 
            this.label_classWebSource.AutoSize = true;
            this.label_classWebSource.Location = new System.Drawing.Point(265, 59);
            this.label_classWebSource.Name = "label_classWebSource";
            this.label_classWebSource.Size = new System.Drawing.Size(188, 25);
            this.label_classWebSource.TabIndex = 2;
            this.label_classWebSource.Text = "Choose the Class:";
            // 
            // label_saveFileType
            // 
            this.label_saveFileType.AutoSize = true;
            this.label_saveFileType.Location = new System.Drawing.Point(153, 48);
            this.label_saveFileType.Name = "label_saveFileType";
            this.label_saveFileType.Size = new System.Drawing.Size(261, 25);
            this.label_saveFileType.TabIndex = 22;
            this.label_saveFileType.Text = "Choose the save file type:";
            // 
            // radioButton_saveXML
            // 
            this.radioButton_saveXML.AutoSize = true;
            this.radioButton_saveXML.Location = new System.Drawing.Point(549, 46);
            this.radioButton_saveXML.Name = "radioButton_saveXML";
            this.radioButton_saveXML.Size = new System.Drawing.Size(87, 29);
            this.radioButton_saveXML.TabIndex = 21;
            this.radioButton_saveXML.Text = "XML";
            this.radioButton_saveXML.UseVisualStyleBackColor = true;
            // 
            // radioButton_saveJSON
            // 
            this.radioButton_saveJSON.AutoSize = true;
            this.radioButton_saveJSON.Location = new System.Drawing.Point(435, 46);
            this.radioButton_saveJSON.Name = "radioButton_saveJSON";
            this.radioButton_saveJSON.Size = new System.Drawing.Size(99, 29);
            this.radioButton_saveJSON.TabIndex = 20;
            this.radioButton_saveJSON.Text = "JSON";
            this.radioButton_saveJSON.UseVisualStyleBackColor = true;
            // 
            // groupBox_saveCourseData
            // 
            this.groupBox_saveCourseData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox_saveCourseData.Controls.Add(this.radioButton_saveXML);
            this.groupBox_saveCourseData.Controls.Add(this.label_saveFileType);
            this.groupBox_saveCourseData.Controls.Add(this.radioButton_saveJSON);
            this.groupBox_saveCourseData.Location = new System.Drawing.Point(31, 770);
            this.groupBox_saveCourseData.Name = "groupBox_saveCourseData";
            this.groupBox_saveCourseData.Size = new System.Drawing.Size(842, 100);
            this.groupBox_saveCourseData.TabIndex = 23;
            this.groupBox_saveCourseData.TabStop = false;
            this.groupBox_saveCourseData.Text = "Select Save Type";
            // 
            // groupBox_getMDVCourseData
            // 
            this.groupBox_getMDVCourseData.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox_getMDVCourseData.Controls.Add(this.panel1);
            this.groupBox_getMDVCourseData.Controls.Add(this.label_classWebSource);
            this.groupBox_getMDVCourseData.Controls.Add(this.button_Display);
            this.groupBox_getMDVCourseData.Controls.Add(this.label1);
            this.groupBox_getMDVCourseData.Controls.Add(this.radioButton_JSON);
            this.groupBox_getMDVCourseData.Controls.Add(this.radioButton_XML);
            this.groupBox_getMDVCourseData.Location = new System.Drawing.Point(31, 70);
            this.groupBox_getMDVCourseData.Name = "groupBox_getMDVCourseData";
            this.groupBox_getMDVCourseData.Size = new System.Drawing.Size(842, 270);
            this.groupBox_getMDVCourseData.TabIndex = 24;
            this.groupBox_getMDVCourseData.TabStop = false;
            this.groupBox_getMDVCourseData.Text = "Get MDV Course Data";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton_VFW);
            this.panel1.Controls.Add(this.radioButton_ASD);
            this.panel1.Location = new System.Drawing.Point(459, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(223, 55);
            this.panel1.TabIndex = 17;
            // 
            // radioButton_VFW
            // 
            this.radioButton_VFW.AutoSize = true;
            this.radioButton_VFW.Location = new System.Drawing.Point(136, 13);
            this.radioButton_VFW.Name = "radioButton_VFW";
            this.radioButton_VFW.Size = new System.Drawing.Size(90, 29);
            this.radioButton_VFW.TabIndex = 1;
            this.radioButton_VFW.TabStop = true;
            this.radioButton_VFW.Text = "VFW";
            this.radioButton_VFW.UseVisualStyleBackColor = true;
            // 
            // radioButton_ASD
            // 
            this.radioButton_ASD.AutoSize = true;
            this.radioButton_ASD.Location = new System.Drawing.Point(3, 13);
            this.radioButton_ASD.Name = "radioButton_ASD";
            this.radioButton_ASD.Size = new System.Drawing.Size(86, 29);
            this.radioButton_ASD.TabIndex = 0;
            this.radioButton_ASD.TabStop = true;
            this.radioButton_ASD.Text = "ASD";
            this.radioButton_ASD.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 944);
            this.Controls.Add(this.groupBox_getMDVCourseData);
            this.Controls.Add(this.groupBox_saveCourseData);
            this.Controls.Add(this.groupBox_DisplayCourseData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox_DisplayCourseData.ResumeLayout(false);
            this.groupBox_DisplayCourseData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_courseMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_creditHours)).EndInit();
            this.groupBox_saveCourseData.ResumeLayout(false);
            this.groupBox_saveCourseData.PerformLayout();
            this.groupBox_getMDVCourseData.ResumeLayout(false);
            this.groupBox_getMDVCourseData.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_New;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Save;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Load;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Exit;
        private System.Windows.Forms.GroupBox groupBox_DisplayCourseData;
        private System.Windows.Forms.TextBox textBox_courseDescription;
        private System.Windows.Forms.Label label_courseDescription;
        private System.Windows.Forms.Label label_classWebSource;
        private System.Windows.Forms.TextBox textBox_courseName;
        private System.Windows.Forms.Label label_courseCode;
        private System.Windows.Forms.TextBox textBox_courseCode;
        private System.Windows.Forms.Label label_courseName;
        private System.Windows.Forms.Button button_Display;
        private System.Windows.Forms.Label label_courseMonth;
        private System.Windows.Forms.Label label_creditHours;
        private System.Windows.Forms.NumericUpDown numericUpDown_courseMonth;
        private System.Windows.Forms.NumericUpDown numericUpDown_creditHours;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton_XML;
        private System.Windows.Forms.RadioButton radioButton_JSON;
        private System.Windows.Forms.Label label_saveFileType;
        private System.Windows.Forms.RadioButton radioButton_saveXML;
        private System.Windows.Forms.RadioButton radioButton_saveJSON;
        private System.Windows.Forms.GroupBox groupBox_saveCourseData;
        private System.Windows.Forms.GroupBox groupBox_getMDVCourseData;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButton_VFW;
        private System.Windows.Forms.RadioButton radioButton_ASD;
    }
}
