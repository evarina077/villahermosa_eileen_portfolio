﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Xml;

namespace EileenVillahermosa_CE08
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE08: XML and File I/O
    // 03/16/2018

    public partial class Form1 : Form
    {
        // add instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();

        // api starting point
        string apiStartPoint = "http://mdv-vfw.com";
        // api ending point
        string apiEndPoint;

        // to check internet connection
        // this will prevent from the program crashing
        public static bool CheckConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                MessageBox.Show("Sorry no internet connection. \r\nPlease check and try again.");
                return false;
            }
        }


        public Form1()
        {
            InitializeComponent();
        }


        private void toolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            // to exit the program
            Application.Exit();
        }


        private void ResetAll()
        {
            // to reset all controls
            textBox_courseName.Clear();
            textBox_courseCode.Clear();
            textBox_courseDescription.Clear();
            numericUpDown_creditHours.Value = 0;
            numericUpDown_courseMonth.Value = 0;
            radioButton_XML.Checked = false;
            radioButton_JSON.Checked = false;
            radioButton_ASD.Checked = false;
            radioButton_VFW.Checked = false;
            radioButton_saveJSON.Checked = false;
            radioButton_saveXML.Checked = false;
        }


        private void toolStripMenuItem_New_Click(object sender, EventArgs e)
        {
            // to call to reset method
            ResetAll();
        }


        private void BuildAPI()
        {
            string asdJsonOrXml = "/asd.xml";
            string vfwJsonOrXml = "/vfw.json";

            // to check first if proper selections have been made
            // message will display to user what has not been selected
            // they need to make selections first
            // once all proper selections has been made the web source 
            // will then be pulled in
            if(radioButton_ASD.Checked == false && radioButton_VFW.Checked == false && radioButton_JSON.Checked == false && radioButton_XML.Checked == false)
            {
                MessageBox.Show("Nothing selected");
                return;
            }

            else if (radioButton_ASD.Checked == true && radioButton_VFW.Checked == false && radioButton_JSON.Checked == false && radioButton_XML.Checked == false || radioButton_ASD.Checked == false && radioButton_VFW.Checked == true && radioButton_JSON.Checked == false && radioButton_XML.Checked == false)
            {
                MessageBox.Show("No file type selected");
                return;
            }

            else if (radioButton_ASD.Checked == false && radioButton_VFW.Checked == false && radioButton_JSON.Checked == true && radioButton_XML.Checked == false || radioButton_ASD.Checked == false && radioButton_VFW.Checked == false && radioButton_JSON.Checked == false && radioButton_XML.Checked == true)
            {
                MessageBox.Show("No class selected");
                return;
            }

            else
            {
                if (radioButton_ASD.Checked)
                {
                    // to complete the api
                    if (radioButton_JSON.Checked)
                    {
                        asdJsonOrXml = "/asd.json";
                    }

                    apiEndPoint = apiStartPoint + asdJsonOrXml;
                }

                else if(radioButton_VFW.Checked)
                {
                    // to complete the api
                    if (radioButton_XML.Checked)
                    {
                        vfwJsonOrXml = "/vfw.xml";
                    }

                    apiEndPoint = apiStartPoint + vfwJsonOrXml;
                }
            }
        }


        private void GetXMLData()
        {
            int i = 0;
            string courseName = "";
            string courseCode = "";
            string courseDescription = "";
            decimal creditHours = 0;
            decimal courseMonth = 0;

            // Created the XMLreader with a create method
            using (XmlReader apiData = XmlReader.Create(apiEndPoint))
            {
                // to loop through the XML data
                while (apiData.Read())
                {
                    if (apiData.Name == "course_name_clean" && i == 0)
                    {
                        courseName = apiData.ReadElementContentAsString();
                        i = 1;
                    }

                    if (apiData.Name == "course_code_long")
                    {
                        courseCode = apiData.ReadElementContentAsString();
                    }

                    if (apiData.Name == "course_description")
                    {
                        courseDescription = apiData.ReadElementContentAsString();
                    }

                    if (apiData.Name == "credit")
                    {
                        creditHours = apiData.ReadElementContentAsDecimal();
                    }

                    if (apiData.Name == "sequence")
                    {
                        courseMonth = apiData.ReadElementContentAsDecimal();
                    }
                }

                // to place all data back into their input controls
                textBox_courseName.Text = courseName;
                textBox_courseCode.Text = courseCode;
                textBox_courseDescription.Text = courseDescription;
                numericUpDown_creditHours.Value = creditHours;
                numericUpDown_courseMonth.Value = courseMonth;
            }
        }


        private void GetJSONData()
        {
            // to download the data as a string
            var apiData = apiConnection.DownloadString(apiEndPoint);

            // to convert the data string to an object
            JObject apiDataObj = JObject.Parse(apiData);

            // find the specific data in the object and view in the input controls
            string courseName = apiDataObj["class"]["course_name_clean"].ToString();
            string courseCode = apiDataObj["class"]["course_code_long"].ToString();
            string courseDescription = apiDataObj["class"]["course_description"].ToString();
            string creditHours = apiDataObj["class"]["credit"].ToString();
            int creditHoursNum = Convert.ToInt32(creditHours);
            string courseMonth = apiDataObj["class"]["sequence"].ToString();
            int courseMonthNum = Convert.ToInt32(courseMonth);


            // to view the objects as strings
            textBox_courseName.Text = courseName;
            textBox_courseCode.Text = courseCode;
            textBox_courseDescription.Text = courseDescription;
            numericUpDown_creditHours.Value = creditHoursNum;
            numericUpDown_courseMonth.Value = courseMonthNum;
        }


        // method to determine XML course file choosen
        private void InputTheXMLData()
        {
            switch (apiEndPoint)
            {
                case "http://mdv-vfw.com/asd.xml":
                    GetXMLData();
                    break;

                case "http://mdv-vfw.com/vfw.xml":
                    GetXMLData();
                    break;
            }
        }


        // method to determine JSON course file choosen
        private void InputTheJSONData()
        {
            switch (apiEndPoint)
            {
                case "http://mdv-vfw.com/asd.json":
                    GetJSONData();
                    break;

                case "http://mdv-vfw.com/vfw.json":
                    GetJSONData();
                    break;
            }
        }


        private void button_Display_Click(object sender, EventArgs e)
        {
            // to call to check connection method
            CheckConnection();

            // to call to build the api method
            BuildAPI();

            // to check which data type to display
            if (apiEndPoint == "http://mdv-vfw.com/asd.json" || apiEndPoint == "http://mdv-vfw.com/vfw.json")
            {
                InputTheJSONData();
            }
            else
            {
                InputTheXMLData();
            }

        }
        

        private void toolStripMenuItem_Save_Click(object sender, EventArgs e)
        {
            // to check if a save type has been selected
            // will display message to user if save type has not been selected
            // will also display message to user if there is no data to save
            // if all is met for save action, it will then proceed to saving
            if(textBox_courseName.Text == "")
            {
                MessageBox.Show("No data to save");
            }

            else if(radioButton_saveJSON.Checked == false && radioButton_saveXML.Checked == false)
            {
                MessageBox.Show("Please select a save type first");
            }

            else
            {
                // JSON data save process
                if (radioButton_saveJSON.Checked)
                {
                    // to use the save file dialog
                    SaveFileDialog saveJsonFile = new SaveFileDialog();

                    // filter created to have file saved as txt
                    saveJsonFile.Filter = "Text Files(*.txt)|*.txt";

                    // string variable creted for indentifier
                    string identifier = "SavedJsonFile";

                    // to check user clicked OK
                    if (saveJsonFile.ShowDialog() == DialogResult.OK)
                    {
                        // to start the writer
                        using (StreamWriter Input = new StreamWriter(saveJsonFile.FileName))
                        {
                            // this is the format to save all data input including it's identifier
                            Input.WriteLine(identifier + "/" + textBox_courseName.Text + "/" + textBox_courseCode.Text + "/" + textBox_courseDescription.Text + "/" + numericUpDown_creditHours.Value.ToString() + "/" + numericUpDown_courseMonth.Value.ToString() + "/" + identifier);

                            // to call to clear method
                            ResetAll();
                        }
                    }
                }

                else
                {
                    // XML data save process
                    // to use the save file dialog
                    SaveFileDialog saveXmlFile = new SaveFileDialog();

                    // filter created to have save as xml
                    saveXmlFile.Filter = "XML Files (*.xml)|*.xml";

                    // to check user cliked ok
                    if (DialogResult.OK == saveXmlFile.ShowDialog())
                    {
                        // to instantiatne XMLWriterSettings object
                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.ConformanceLevel = ConformanceLevel.Document;

                        // to get the proper indentation
                        settings.Indent = true;

                        // to start the writer
                        using (XmlWriter writer = XmlWriter.Create(saveXmlFile.FileName, settings))
                        {
                            // to save each data input
                            writer.WriteStartElement("MDVCourseData");

                            writer.WriteElementString("course_name_clean", textBox_courseName.Text);

                            writer.WriteElementString("course_code_long", textBox_courseCode.Text);

                            writer.WriteElementString("course_description", textBox_courseDescription.Text);

                            writer.WriteElementString("credit", numericUpDown_creditHours.Text);

                            writer.WriteElementString("sequence", numericUpDown_courseMonth.Text);

                            writer.WriteEndElement();

                            // to call to clear method
                            ResetAll();
                        }
                    }
                }
            }
            
        }


        private void toolStripMenuItem_Load_Click(object sender, EventArgs e)
        {
            // to call to clear method
            ResetAll();


            // to use the oepn file dialog
            OpenFileDialog openFile = new OpenFileDialog();

            // to check user clicked OK
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                // to check if user selected .txt file type
                if (openFile.FileName.Contains(".txt"))
                {
                    // to start the reader
                    using (StreamReader info = new StreamReader(openFile.FileName))
                    {
                        // string created to read each line
                        string open = info.ReadLine();

                        // placed string data into an array
                        string[] fileOpen = open.Split('/');

                        // to check if user opens the correct data file by checking
                        // if file has the identifier, if not it will throw a message
                        if (fileOpen[0] != "SavedJsonFile")
                        {
                            MessageBox.Show("Sorry this is not the correct JSON data file");
                            return;
                        }
                        else
                        {
                            // this will load all data back into their controls
                            textBox_courseName.Text = fileOpen[1];
                            textBox_courseCode.Text = fileOpen[2];
                            textBox_courseDescription.Text = fileOpen[3];
                            numericUpDown_creditHours.Value = Decimal.Parse(fileOpen[4]);
                            numericUpDown_courseMonth.Value = Decimal.Parse(fileOpen[5]);
                        }
                    }
                }
                // to check if user selected .xml file type
                else if (openFile.FileName.Contains(".xml"))
                {
                    // to instantiatne XMLWriterSettings object
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ConformanceLevel = ConformanceLevel.Document;

                    // to make sure reader gets only the XML
                    // to ignore comments and whitespace
                    settings.IgnoreComments = true;
                    settings.IgnoreWhitespace = true;

                    // to start the reader
                    using (XmlReader reader = XmlReader.Create(openFile.FileName, settings))
                    {
                        // to skip the metadata
                        reader.MoveToContent();

                        // to verify it is the MDV Course Data
                        if (reader.Name != "MDVCourseData")
                        {
                            // to return that this is not right
                            MessageBox.Show("Sorry this is not the correct XML data file");
                            return;
                        }

                        // to loop through the data
                        while (reader.Read())
                        {
                            // this will check for the right data needed
                            // then load back into its proper input control
                            if (reader.Name == "course_name_clean" && reader.IsStartElement())
                            {
                                textBox_courseName.Text = reader.ReadElementContentAsString();
                            }

                            if (reader.Name == "course_code_long" && reader.IsStartElement())
                            {
                                textBox_courseCode.Text = reader.ReadElementContentAsString();
                            }

                            if (reader.Name == "course_description" && reader.IsStartElement())
                            {
                                textBox_courseDescription.Text = reader.ReadElementContentAsString();
                            }

                            if (reader.Name == "credit" && reader.IsStartElement())
                            {
                                numericUpDown_creditHours.Value = reader.ReadElementContentAsDecimal();
                            }

                            if (reader.Name == "sequence" && reader.IsStartElement())
                            {
                                numericUpDown_courseMonth.Value = reader.ReadElementContentAsDecimal();
                            }
                        }
                    }
                }
                // if user does not choose a .txt or .xml file this message will display
                else
                {
                    MessageBox.Show("Sorry you did not select the correct .txt or .xml data file");
                }

            }
        }
    }
}

