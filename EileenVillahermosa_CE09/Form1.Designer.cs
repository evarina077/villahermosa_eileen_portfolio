﻿namespace EileenVillahermosa_CE09
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE09: Database Connectivity
    // 03/20/18


    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_movieRentalData = new System.Windows.Forms.GroupBox();
            this.label_record = new System.Windows.Forms.Label();
            this.label_currentRecordNumber = new System.Windows.Forms.Label();
            this.textBox_genre = new System.Windows.Forms.TextBox();
            this.numericUpDown_publicRating = new System.Windows.Forms.NumericUpDown();
            this.label_genre = new System.Windows.Forms.Label();
            this.label_price = new System.Windows.Forms.Label();
            this.numericUpDown_price = new System.Windows.Forms.NumericUpDown();
            this.button_last = new System.Windows.Forms.Button();
            this.button_first = new System.Windows.Forms.Button();
            this.button_previous = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.label_totalRecordsNumber = new System.Windows.Forms.Label();
            this.label_totalRecords = new System.Windows.Forms.Label();
            this.label_publicRating = new System.Windows.Forms.Label();
            this.textBox_DVDTitle = new System.Windows.Forms.TextBox();
            this.label_dvdTitle = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_save = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_movieRentalData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_publicRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_price)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_movieRentalData
            // 
            this.groupBox_movieRentalData.Controls.Add(this.label_record);
            this.groupBox_movieRentalData.Controls.Add(this.label_currentRecordNumber);
            this.groupBox_movieRentalData.Controls.Add(this.textBox_genre);
            this.groupBox_movieRentalData.Controls.Add(this.numericUpDown_publicRating);
            this.groupBox_movieRentalData.Controls.Add(this.label_genre);
            this.groupBox_movieRentalData.Controls.Add(this.label_price);
            this.groupBox_movieRentalData.Controls.Add(this.numericUpDown_price);
            this.groupBox_movieRentalData.Controls.Add(this.button_last);
            this.groupBox_movieRentalData.Controls.Add(this.button_first);
            this.groupBox_movieRentalData.Controls.Add(this.button_previous);
            this.groupBox_movieRentalData.Controls.Add(this.button_next);
            this.groupBox_movieRentalData.Controls.Add(this.label_totalRecordsNumber);
            this.groupBox_movieRentalData.Controls.Add(this.label_totalRecords);
            this.groupBox_movieRentalData.Controls.Add(this.label_publicRating);
            this.groupBox_movieRentalData.Controls.Add(this.textBox_DVDTitle);
            this.groupBox_movieRentalData.Controls.Add(this.label_dvdTitle);
            this.groupBox_movieRentalData.Location = new System.Drawing.Point(12, 58);
            this.groupBox_movieRentalData.Name = "groupBox_movieRentalData";
            this.groupBox_movieRentalData.Size = new System.Drawing.Size(618, 420);
            this.groupBox_movieRentalData.TabIndex = 0;
            this.groupBox_movieRentalData.TabStop = false;
            this.groupBox_movieRentalData.Text = "Movie DVD Data";
            // 
            // label_record
            // 
            this.label_record.AutoSize = true;
            this.label_record.Location = new System.Drawing.Point(193, 266);
            this.label_record.Name = "label_record";
            this.label_record.Size = new System.Drawing.Size(87, 25);
            this.label_record.TabIndex = 18;
            this.label_record.Text = "Record:";
            // 
            // label_currentRecordNumber
            // 
            this.label_currentRecordNumber.AutoSize = true;
            this.label_currentRecordNumber.Location = new System.Drawing.Point(275, 266);
            this.label_currentRecordNumber.Name = "label_currentRecordNumber";
            this.label_currentRecordNumber.Size = new System.Drawing.Size(24, 25);
            this.label_currentRecordNumber.TabIndex = 17;
            this.label_currentRecordNumber.Text = "0";
            // 
            // textBox_genre
            // 
            this.textBox_genre.Location = new System.Drawing.Point(198, 98);
            this.textBox_genre.Name = "textBox_genre";
            this.textBox_genre.ReadOnly = true;
            this.textBox_genre.Size = new System.Drawing.Size(294, 31);
            this.textBox_genre.TabIndex = 16;
            // 
            // numericUpDown_publicRating
            // 
            this.numericUpDown_publicRating.DecimalPlaces = 2;
            this.numericUpDown_publicRating.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_publicRating.Location = new System.Drawing.Point(198, 151);
            this.numericUpDown_publicRating.Name = "numericUpDown_publicRating";
            this.numericUpDown_publicRating.ReadOnly = true;
            this.numericUpDown_publicRating.Size = new System.Drawing.Size(92, 31);
            this.numericUpDown_publicRating.TabIndex = 15;
            // 
            // label_genre
            // 
            this.label_genre.AutoSize = true;
            this.label_genre.Location = new System.Drawing.Point(99, 101);
            this.label_genre.Name = "label_genre";
            this.label_genre.Size = new System.Drawing.Size(77, 25);
            this.label_genre.TabIndex = 14;
            this.label_genre.Text = "Genre:";
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.Location = new System.Drawing.Point(109, 210);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(67, 25);
            this.label_price.TabIndex = 12;
            this.label_price.Text = "Price:";
            // 
            // numericUpDown_price
            // 
            this.numericUpDown_price.DecimalPlaces = 2;
            this.numericUpDown_price.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_price.Location = new System.Drawing.Point(198, 208);
            this.numericUpDown_price.Name = "numericUpDown_price";
            this.numericUpDown_price.ReadOnly = true;
            this.numericUpDown_price.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_price.TabIndex = 11;
            // 
            // button_last
            // 
            this.button_last.Location = new System.Drawing.Point(497, 323);
            this.button_last.Name = "button_last";
            this.button_last.Size = new System.Drawing.Size(95, 49);
            this.button_last.TabIndex = 10;
            this.button_last.Text = ">>";
            this.button_last.UseVisualStyleBackColor = true;
            this.button_last.Click += new System.EventHandler(this.button_last_Click);
            // 
            // button_first
            // 
            this.button_first.Location = new System.Drawing.Point(195, 323);
            this.button_first.Name = "button_first";
            this.button_first.Size = new System.Drawing.Size(95, 49);
            this.button_first.TabIndex = 9;
            this.button_first.Text = "<<";
            this.button_first.UseVisualStyleBackColor = true;
            this.button_first.Click += new System.EventHandler(this.button_first_Click);
            // 
            // button_previous
            // 
            this.button_previous.Location = new System.Drawing.Point(312, 323);
            this.button_previous.Name = "button_previous";
            this.button_previous.Size = new System.Drawing.Size(75, 49);
            this.button_previous.TabIndex = 8;
            this.button_previous.Text = "<";
            this.button_previous.UseVisualStyleBackColor = true;
            this.button_previous.Click += new System.EventHandler(this.button_previous_Click);
            // 
            // button_next
            // 
            this.button_next.Location = new System.Drawing.Point(402, 323);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(75, 49);
            this.button_next.TabIndex = 7;
            this.button_next.Text = ">";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // label_totalRecordsNumber
            // 
            this.label_totalRecordsNumber.AutoSize = true;
            this.label_totalRecordsNumber.Location = new System.Drawing.Point(343, 266);
            this.label_totalRecordsNumber.Name = "label_totalRecordsNumber";
            this.label_totalRecordsNumber.Size = new System.Drawing.Size(24, 25);
            this.label_totalRecordsNumber.TabIndex = 6;
            this.label_totalRecordsNumber.Text = "0";
            // 
            // label_totalRecords
            // 
            this.label_totalRecords.AutoSize = true;
            this.label_totalRecords.Location = new System.Drawing.Point(307, 266);
            this.label_totalRecords.Name = "label_totalRecords";
            this.label_totalRecords.Size = new System.Drawing.Size(36, 25);
            this.label_totalRecords.TabIndex = 5;
            this.label_totalRecords.Text = "of:";
            // 
            // label_publicRating
            // 
            this.label_publicRating.AutoSize = true;
            this.label_publicRating.Location = new System.Drawing.Point(31, 153);
            this.label_publicRating.Name = "label_publicRating";
            this.label_publicRating.Size = new System.Drawing.Size(145, 25);
            this.label_publicRating.TabIndex = 4;
            this.label_publicRating.Text = "Public Rating:";
            // 
            // textBox_DVDTitle
            // 
            this.textBox_DVDTitle.Location = new System.Drawing.Point(198, 46);
            this.textBox_DVDTitle.Name = "textBox_DVDTitle";
            this.textBox_DVDTitle.ReadOnly = true;
            this.textBox_DVDTitle.Size = new System.Drawing.Size(394, 31);
            this.textBox_DVDTitle.TabIndex = 3;
            // 
            // label_dvdTitle
            // 
            this.label_dvdTitle.AutoSize = true;
            this.label_dvdTitle.Location = new System.Drawing.Point(67, 49);
            this.label_dvdTitle.Name = "label_dvdTitle";
            this.label_dvdTitle.Size = new System.Drawing.Size(109, 25);
            this.label_dvdTitle.TabIndex = 1;
            this.label_dvdTitle.Text = "DVD Title:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(642, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_exit,
            this.toolStripMenuItem_save});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+S";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripMenuItem_exit
            // 
            this.toolStripMenuItem_exit.Name = "toolStripMenuItem_exit";
            this.toolStripMenuItem_exit.ShortcutKeyDisplayString = "Ctrl+Q";
            this.toolStripMenuItem_exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolStripMenuItem_exit.Size = new System.Drawing.Size(245, 38);
            this.toolStripMenuItem_exit.Text = "Exit";
            this.toolStripMenuItem_exit.Click += new System.EventHandler(this.toolStripMenuItem_exit_Click);
            // 
            // toolStripMenuItem_save
            // 
            this.toolStripMenuItem_save.Name = "toolStripMenuItem_save";
            this.toolStripMenuItem_save.ShortcutKeyDisplayString = "Ctrl+S";
            this.toolStripMenuItem_save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem_save.Size = new System.Drawing.Size(245, 38);
            this.toolStripMenuItem_save.Text = "Save";
            this.toolStripMenuItem_save.Click += new System.EventHandler(this.toolStripMenuItem_save_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 567);
            this.Controls.Add(this.groupBox_movieRentalData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox_movieRentalData.ResumeLayout(false);
            this.groupBox_movieRentalData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_publicRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_price)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_movieRentalData;
        private System.Windows.Forms.Label label_totalRecordsNumber;
        private System.Windows.Forms.Label label_totalRecords;
        private System.Windows.Forms.Label label_publicRating;
        private System.Windows.Forms.TextBox textBox_DVDTitle;
        private System.Windows.Forms.Label label_dvdTitle;
        private System.Windows.Forms.Button button_next;
        private System.Windows.Forms.Button button_previous;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_exit;
        private System.Windows.Forms.Button button_last;
        private System.Windows.Forms.Button button_first;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.NumericUpDown numericUpDown_price;
        private System.Windows.Forms.Label label_genre;
        private System.Windows.Forms.NumericUpDown numericUpDown_publicRating;
        private System.Windows.Forms.TextBox textBox_genre;
        private System.Windows.Forms.Label label_record;
        private System.Windows.Forms.Label label_currentRecordNumber;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_save;
    }
}

