﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace EileenVillahermosa_CE09
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE09: Database Connectivity
    // 03/20/18


    public partial class Form1 : Form
    {
        // connection string variable
        MySqlConnection conn = new MySqlConnection();

        // an instance variable for the DataTable
        DataTable theData = new DataTable();

        // an instance variable for the current row
        int row = 0;

        public Form1()
        {
            InitializeComponent();

            // call the method to build the connection string
            string connectionString = BuildConnectionString();

            // call the method to make the connection
            Connect(connectionString);

            // call the method to retrieve data from database
            RetrieveData();

        }

        // method for popup details when user hovers over input controls
        private void Form1_Load(object sender, EventArgs e)
        {
            ToolTip TP = new ToolTip();
            TP.ShowAlways = true;
            TP.SetToolTip(textBox_DVDTitle, theData.Rows[0]["DVD_Title"].ToString());
            TP.SetToolTip(textBox_genre, theData.Rows[0]["Genre"].ToString());
            TP.SetToolTip(numericUpDown_publicRating, theData.Rows[0]["publicRating"].ToString());
            TP.SetToolTip(numericUpDown_price, theData.Rows[0]["Price"].ToString());
            TP.SetToolTip(button_first, "Go to first record");
            TP.SetToolTip(button_last, "Go to last record");
            TP.SetToolTip(button_next, "Go to next record");
            TP.SetToolTip(button_previous, "Go to previous record");
        }

        private void toolStripMenuItem_exit_Click(object sender, EventArgs e)
        {
            // to exit the program
            Application.Exit();
        }

        // method to retrieve data from database
        private bool RetrieveData()
        {
            // the SQL statement
            string sql = "SELECT DVD_Title, publicRating, Price, Genre FROM dvd LIMIT 10";

            // the DataAdapter instantiated
            MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

            // to set the type for the SELECT command to text
            adr.SelectCommand.CommandType = CommandType.Text;

            // the fill method adds rows to match the data source
            // to fill the DataTable with the recordset returned by the DataAdapter
            adr.Fill(theData);

            // to get a count of the number of rows within the DataTable using a method and sending no argument
            int numberOfRecords = theData.Select().Length;

            // to start current number of record count to 1;
            int currentNumber = 1;

            // to put the first record data into the form
            textBox_DVDTitle.Text = theData.Rows[0]["DVD_Title"].ToString();
            textBox_genre.Text = theData.Rows[0]["Genre"].ToString();
            string publicRating = theData.Rows[0]["publicRating"].ToString();
            decimal publicRatingNum = Decimal.Parse(publicRating);
            numericUpDown_publicRating.Value = publicRatingNum;
            string price = theData.Rows[0]["Price"].ToString();
            decimal priceNum = Decimal.Parse(price);
            numericUpDown_price.Value = priceNum;
            label_currentRecordNumber.Text = currentNumber.ToString();
            label_totalRecordsNumber.Text = numberOfRecords.ToString();

            // to close the connection once needed data has been retrieved
            conn.Close();

            return true;
        }
        
        // to connect to sql and catch any exceptions
        private void Connect(string myConnectionString)
        {
            try
            {
                conn.ConnectionString = myConnectionString;
                conn.Open();

                // --- use for testing purposes
                // MessageBox.Show("Connected!");
            }
            catch (MySqlException e)
            {
                // message string variable
                string msg = "";

                // to check what exception was received
                switch (e.Number) 
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve host address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username/password";
                        break;
                    default:
                        // generic message if the others don't cover it
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }
                MessageBox.Show(msg);
            }
        }

        private string BuildConnectionString()
        {
            string serverIP = "";

            try
            {
                // to open the text file using a stream reader
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    //reader the server IP data
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            // return the entire string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";
        }


        private void GetUpdateData()
        {
            // update the data showing on the form
            textBox_DVDTitle.Text = theData.Rows[row]["DVD_Title"].ToString();
            textBox_genre.Text = theData.Rows[row]["Genre"].ToString();
            string publicRating = theData.Rows[row]["publicRating"].ToString();
            decimal publicRatingNum = Decimal.Parse(publicRating);
            numericUpDown_publicRating.Value = publicRatingNum;
            string price = theData.Rows[row]["Price"].ToString();
            decimal priceNum = Decimal.Parse(price);
            numericUpDown_price.Value = priceNum;
        }


        private void button_next_Click(object sender, EventArgs e)
        {
            // make sure we haven't exceeded the last record
            if (row + 1 < theData.Select().Length)
            {
                // will go through all records and place each record set into 
                // the input controls, it will update to the next record when
                // hitting the next arrow button
                row++;
                GetUpdateData();
                int currentRow = row + 1;
                label_currentRecordNumber.Text = currentRow.ToString();
            }
            // this make sure enable is on for the first and previous button
            // but enable off for the last button after clicking
            else
            {
                button_next.Enabled = false;
                button_last.Enabled = false;
            }

            button_previous.Enabled = true;
            button_first.Enabled = true;

        }

        private void button_previous_Click(object sender, EventArgs e)
        {
            // make sure we haven't exceeded the first record
            if (row + 1 == theData.Select().Length || row != 0)
            {
                // will go through all records and place each record set into 
                // the input controls, it will update to the previous record when
                // hitting the previous arrow button
                row--;
                GetUpdateData();
                int currentRow = row + 1;
                label_currentRecordNumber.Text = currentRow.ToString();
            }

            // this make sure enable is off for the last and next button
            // but enable on for the previous and first button after clicking
            else
            {
                button_previous.Enabled = false;
                button_first.Enabled = false;
            }

            button_next.Enabled = true;
            button_last.Enabled = true;
        }

        private void button_first_Click(object sender, EventArgs e)
        {
            // make sure we haven't exceeded the first record
            if (theData.Select().Length > 0)
            {
                // will go through all records and place each record set into 
                // the input controls, it will update to the next record when
                // hitting the next arrow button
                row = 0;
                GetUpdateData();

                int currentRow = row + 1;
                label_currentRecordNumber.Text = currentRow.ToString();
            }

                // this make sure enable is on for the last and next button
                // but enable off for the first button after clicking
                button_first.Enabled = false;
                button_last.Enabled = true;
                button_next.Enabled = true;
        }

        private void button_last_Click(object sender, EventArgs e)
        {
            // to jump to the last record
            row = theData.Select().Length - 1;
            GetUpdateData();
            int currentRow = row + 1;
            label_currentRecordNumber.Text = currentRow.ToString();

            // this make sure enable is on for the first and previous button
            // but enable off for the last button after clicking
            button_last.Enabled = false;
            button_first.Enabled = true;
            button_previous.Enabled = true;

        }

        private void toolStripMenuItem_save_Click(object sender, EventArgs e)
        {
            // to check first if data is not null
            if(theData != null)
            {
                // to instantiate the save file dialog
                SaveFileDialog saveFile = new SaveFileDialog();

                // filter to set safe file type to .txt
                saveFile.Filter = "(*.txt)|*.txt";

                // to access the save file dailog and save data
                if(saveFile.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter Input = new StreamWriter(saveFile.FileName))
                    {
                        //Input.WriteLine(textBox_DVDTitle.Text + "\r\n" + textBox_genre.Text + "r\n" + numericUpDown_publicRating.Value.ToString() + "r\n" + numericUpDown_price.Value.ToString());
                        Input.WriteLine(textBox_DVDTitle.Text);
                        Input.WriteLine(textBox_genre.Text);
                        Input.WriteLine(numericUpDown_publicRating.Value.ToString());
                        Input.WriteLine(numericUpDown_price.Value.ToString());
                    }
                }
            }
        }
    }
}
