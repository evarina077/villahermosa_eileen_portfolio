﻿namespace EileenVillahermosa_Final
{
    // Eileen Villahermosa
    // VFW Term 01
    // Final: Synthesis
    // 03/21/18


    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox_movieDVDData = new System.Windows.Forms.GroupBox();
            this.button_update = new System.Windows.Forms.Button();
            this.textBox_genre = new System.Windows.Forms.TextBox();
            this.numericUpDown_publicRating = new System.Windows.Forms.NumericUpDown();
            this.label_genre = new System.Windows.Forms.Label();
            this.label_price = new System.Windows.Forms.Label();
            this.numericUpDown_price = new System.Windows.Forms.NumericUpDown();
            this.label_publicRating = new System.Windows.Forms.Label();
            this.textBox_DVDTitle = new System.Windows.Forms.TextBox();
            this.label_dvdTitle = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_DVDList = new System.Windows.Forms.TabPage();
            this.listView_DVDList = new System.Windows.Forms.ListView();
            this.tabPage_DVDDetail = new System.Windows.Forms.TabPage();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_smallIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_largeIcon = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.imageList_small = new System.Windows.Forms.ImageList(this.components);
            this.imageList_large = new System.Windows.Forms.ImageList(this.components);
            this.groupBox_movieDVDData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_publicRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_price)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage_DVDList.SuspendLayout();
            this.tabPage_DVDDetail.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_movieDVDData
            // 
            this.groupBox_movieDVDData.Controls.Add(this.button_update);
            this.groupBox_movieDVDData.Controls.Add(this.textBox_genre);
            this.groupBox_movieDVDData.Controls.Add(this.numericUpDown_publicRating);
            this.groupBox_movieDVDData.Controls.Add(this.label_genre);
            this.groupBox_movieDVDData.Controls.Add(this.label_price);
            this.groupBox_movieDVDData.Controls.Add(this.numericUpDown_price);
            this.groupBox_movieDVDData.Controls.Add(this.label_publicRating);
            this.groupBox_movieDVDData.Controls.Add(this.textBox_DVDTitle);
            this.groupBox_movieDVDData.Controls.Add(this.label_dvdTitle);
            this.groupBox_movieDVDData.Location = new System.Drawing.Point(145, 41);
            this.groupBox_movieDVDData.Name = "groupBox_movieDVDData";
            this.groupBox_movieDVDData.Size = new System.Drawing.Size(596, 340);
            this.groupBox_movieDVDData.TabIndex = 2;
            this.groupBox_movieDVDData.TabStop = false;
            this.groupBox_movieDVDData.Text = "Movie DVD Data";
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(183, 273);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(216, 45);
            this.button_update.TabIndex = 25;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            // 
            // textBox_genre
            // 
            this.textBox_genre.Location = new System.Drawing.Point(183, 113);
            this.textBox_genre.Name = "textBox_genre";
            this.textBox_genre.ReadOnly = true;
            this.textBox_genre.Size = new System.Drawing.Size(294, 31);
            this.textBox_genre.TabIndex = 24;
            // 
            // numericUpDown_publicRating
            // 
            this.numericUpDown_publicRating.DecimalPlaces = 2;
            this.numericUpDown_publicRating.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_publicRating.Location = new System.Drawing.Point(183, 166);
            this.numericUpDown_publicRating.Name = "numericUpDown_publicRating";
            this.numericUpDown_publicRating.ReadOnly = true;
            this.numericUpDown_publicRating.Size = new System.Drawing.Size(92, 31);
            this.numericUpDown_publicRating.TabIndex = 23;
            // 
            // label_genre
            // 
            this.label_genre.AutoSize = true;
            this.label_genre.Location = new System.Drawing.Point(84, 116);
            this.label_genre.Name = "label_genre";
            this.label_genre.Size = new System.Drawing.Size(77, 25);
            this.label_genre.TabIndex = 22;
            this.label_genre.Text = "Genre:";
            // 
            // label_price
            // 
            this.label_price.AutoSize = true;
            this.label_price.Location = new System.Drawing.Point(94, 225);
            this.label_price.Name = "label_price";
            this.label_price.Size = new System.Drawing.Size(67, 25);
            this.label_price.TabIndex = 21;
            this.label_price.Text = "Price:";
            // 
            // numericUpDown_price
            // 
            this.numericUpDown_price.DecimalPlaces = 2;
            this.numericUpDown_price.Increment = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDown_price.Location = new System.Drawing.Point(183, 223);
            this.numericUpDown_price.Name = "numericUpDown_price";
            this.numericUpDown_price.ReadOnly = true;
            this.numericUpDown_price.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_price.TabIndex = 20;
            // 
            // label_publicRating
            // 
            this.label_publicRating.AutoSize = true;
            this.label_publicRating.Location = new System.Drawing.Point(16, 168);
            this.label_publicRating.Name = "label_publicRating";
            this.label_publicRating.Size = new System.Drawing.Size(145, 25);
            this.label_publicRating.TabIndex = 19;
            this.label_publicRating.Text = "Public Rating:";
            // 
            // textBox_DVDTitle
            // 
            this.textBox_DVDTitle.Location = new System.Drawing.Point(183, 61);
            this.textBox_DVDTitle.Name = "textBox_DVDTitle";
            this.textBox_DVDTitle.ReadOnly = true;
            this.textBox_DVDTitle.Size = new System.Drawing.Size(394, 31);
            this.textBox_DVDTitle.TabIndex = 18;
            // 
            // label_dvdTitle
            // 
            this.label_dvdTitle.AutoSize = true;
            this.label_dvdTitle.Location = new System.Drawing.Point(52, 64);
            this.label_dvdTitle.Name = "label_dvdTitle";
            this.label_dvdTitle.Size = new System.Drawing.Size(109, 25);
            this.label_dvdTitle.TabIndex = 17;
            this.label_dvdTitle.Text = "DVD Title:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage_DVDList);
            this.tabControl1.Controls.Add(this.tabPage_DVDDetail);
            this.tabControl1.Location = new System.Drawing.Point(12, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(904, 470);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage_DVDList
            // 
            this.tabPage_DVDList.Controls.Add(this.listView_DVDList);
            this.tabPage_DVDList.Location = new System.Drawing.Point(8, 39);
            this.tabPage_DVDList.Name = "tabPage_DVDList";
            this.tabPage_DVDList.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_DVDList.Size = new System.Drawing.Size(888, 423);
            this.tabPage_DVDList.TabIndex = 0;
            this.tabPage_DVDList.Text = "DVD List";
            this.tabPage_DVDList.UseVisualStyleBackColor = true;
            // 
            // listView_DVDList
            // 
            this.listView_DVDList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_DVDList.Location = new System.Drawing.Point(3, 3);
            this.listView_DVDList.Margin = new System.Windows.Forms.Padding(2);
            this.listView_DVDList.MultiSelect = false;
            this.listView_DVDList.Name = "listView_DVDList";
            this.listView_DVDList.Size = new System.Drawing.Size(882, 417);
            this.listView_DVDList.TabIndex = 0;
            this.listView_DVDList.UseCompatibleStateImageBehavior = false;
            this.listView_DVDList.DoubleClick += new System.EventHandler(this.listView_DVDList_DoubleClick);
            // 
            // tabPage_DVDDetail
            // 
            this.tabPage_DVDDetail.Controls.Add(this.groupBox_movieDVDData);
            this.tabPage_DVDDetail.Location = new System.Drawing.Point(8, 39);
            this.tabPage_DVDDetail.Name = "tabPage_DVDDetail";
            this.tabPage_DVDDetail.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_DVDDetail.Size = new System.Drawing.Size(888, 423);
            this.tabPage_DVDDetail.TabIndex = 1;
            this.tabPage_DVDDetail.Text = "DVD Detail";
            this.tabPage_DVDDetail.UseVisualStyleBackColor = true;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripMenuItem_exit
            // 
            this.toolStripMenuItem_exit.Name = "toolStripMenuItem_exit";
            this.toolStripMenuItem_exit.ShortcutKeyDisplayString = "Ctrl+Q";
            this.toolStripMenuItem_exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.toolStripMenuItem_exit.Size = new System.Drawing.Size(237, 38);
            this.toolStripMenuItem_exit.Text = "Exit";
            this.toolStripMenuItem_exit.Click += new System.EventHandler(this.toolStripMenuItem_exit_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_smallIcon,
            this.toolStripMenuItem_largeIcon});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(78, 36);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // toolStripMenuItem_smallIcon
            // 
            this.toolStripMenuItem_smallIcon.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem_smallIcon.Name = "toolStripMenuItem_smallIcon";
            this.toolStripMenuItem_smallIcon.Size = new System.Drawing.Size(224, 38);
            this.toolStripMenuItem_smallIcon.Text = "Small Icon";
            this.toolStripMenuItem_smallIcon.Click += new System.EventHandler(this.smallIconToolStripMenuItem_Click);
            // 
            // toolStripMenuItem_largeIcon
            // 
            this.toolStripMenuItem_largeIcon.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem_largeIcon.Name = "toolStripMenuItem_largeIcon";
            this.toolStripMenuItem_largeIcon.Size = new System.Drawing.Size(224, 38);
            this.toolStripMenuItem_largeIcon.Text = "Large Icon";
            this.toolStripMenuItem_largeIcon.Click += new System.EventHandler(this.largeIconToolStripMenuItem_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(954, 40);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // imageList_small
            // 
            this.imageList_small.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_small.ImageStream")));
            this.imageList_small.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_small.Images.SetKeyName(0, "small dvd icon.png");
            // 
            // imageList_large
            // 
            this.imageList_large.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_large.ImageStream")));
            this.imageList_large.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_large.Images.SetKeyName(0, "large dvd icon.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 643);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox_movieDVDData.ResumeLayout(false);
            this.groupBox_movieDVDData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_publicRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_price)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage_DVDList.ResumeLayout(false);
            this.tabPage_DVDDetail.ResumeLayout(false);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_movieDVDData;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.TextBox textBox_genre;
        private System.Windows.Forms.NumericUpDown numericUpDown_publicRating;
        private System.Windows.Forms.Label label_genre;
        private System.Windows.Forms.Label label_price;
        private System.Windows.Forms.NumericUpDown numericUpDown_price;
        private System.Windows.Forms.Label label_publicRating;
        private System.Windows.Forms.TextBox textBox_DVDTitle;
        private System.Windows.Forms.Label label_dvdTitle;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_DVDList;
        private System.Windows.Forms.ListView listView_DVDList;
        private System.Windows.Forms.TabPage tabPage_DVDDetail;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_exit;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_smallIcon;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_largeIcon;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ImageList imageList_small;
        private System.Windows.Forms.ImageList imageList_large;
    }
}

