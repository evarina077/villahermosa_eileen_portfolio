﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace EileenVillahermosa_Final
{
    // Eileen Villahermosa
    // VFW Term 01
    // Final: Synthesis
    // 03/21/18


    public partial class Form1 : Form
    {
        // connection string variable
        MySqlConnection conn = new MySqlConnection();

        // an instance variable for the DataTable
        DataTable theData = new DataTable();

        public Form1()
        {
            InitializeComponent();

            // call the method to build the connection string
            string connectionString = BuildConnectionString();

            // call the method to make the connection
            Connect(connectionString);

            RetrieveData();
        }

        private void toolStripMenuItem_exit_Click(object sender, EventArgs e)
        {   
            // to exit the whole program
            Application.Exit();
        }

        // to connect to sql and catch any exceptions
        private void Connect(string myConnectionString)
        {
            try
            {
                conn.ConnectionString = myConnectionString;
                conn.Open();

                // --- use for testing purposes
                MessageBox.Show("Connected!");
            }
            catch (MySqlException e)
            {
                // message string variable
                string msg = "";

                // to check what exception was received
                switch (e.Number)
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve host address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username/password";
                        break;
                    default:
                        // generic message if the others don't cover it
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }
                MessageBox.Show(msg);
            }
        }

        private string BuildConnectionString()
        {
            string serverIP = "";

            try
            {
                // to open the text file using a stream reader
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    //reader the server IP data
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            // return the entire string
            return "server=" + serverIP + ";uid=dbsAdmin;pwd=password;database=exampleDatabase;port=8889";
        }

        private bool RetrieveData()
        {
            // the SQL statement
            string sql = "SELECT DVD_Title, publicRating, Price, Genre FROM dvd LIMIT 10";

            // the DataAdapter instantiated
            MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

            // to set the type for the SELECT command to text
            adr.SelectCommand.CommandType = CommandType.Text;

            // the fill method adds rows to match the data source
            // to fill the DataTable with the recordset returned by the DataAdapter
            adr.Fill(theData);
            // to

            for (int i = 0; i < theData.Rows.Count; i++)
            {
                DataRow dr = theData.Rows[i];
                ListViewItem listItem = new ListViewItem(dr["DVD_Title"].ToString());
                listItem.SubItems.Add(dr["Genre"].ToString());
                listItem.ImageIndex = 0;
                listView_DVDList.Items.Add(listItem);
            }

            listView_DVDList.LargeImageList = imageList_large;
            listView_DVDList.SmallImageList = imageList_small;

            toolStripMenuItem_smallIcon.Checked = false;
            toolStripMenuItem_largeIcon.Checked = true;
            listView_DVDList.View = View.LargeIcon;



            // to close the connection once needed data has been retrieved
            conn.Close();

            return true;
        }

        int row = 0;

        private void listView_DVDList_DoubleClick(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);
            if (tabControl1.SelectedTab.Name == "listView_DVDList")
            {
                // add clear method
                foreach(ListViewItem lvi in listView_DVDList.SelectedItems)
                {
                    

                }

            }
                

        }

        private void GetSelectedData()
        {
            // to put the first record data into the form
            textBox_DVDTitle.Text = theData.Rows[row]["DVD_Title"].ToString();
            textBox_genre.Text = theData.Rows[row]["Genre"].ToString();
            string publicRating = theData.Rows[row]["publicRating"].ToString();
            decimal publicRatingNum = Decimal.Parse(publicRating);
            numericUpDown_publicRating.Value = publicRatingNum;
            string price = theData.Rows[row]["Price"].ToString();
            decimal priceNum = Decimal.Parse(price);
            numericUpDown_price.Value = priceNum;
        }

        private void smallIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripMenuItem_smallIcon.Checked = true;
            toolStripMenuItem_largeIcon.Checked = false;
            listView_DVDList.View = View.SmallIcon;
        }

        private void largeIconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripMenuItem_smallIcon.Checked = false;
            toolStripMenuItem_largeIcon.Checked = true;
            listView_DVDList.View = View.LargeIcon;
        }


    }
}
