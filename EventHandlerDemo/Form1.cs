﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventHandlerDemo
{
    public partial class mainForm : Form
    {
        // public event handler to copy the data forward
        public event EventHandler CopyForward;

        //property  for modifying and extracting the textbox values
        public DataHold TextValues
        {
            get
            {
                DataHold retObj = new DataHold();

                retObj.firstName = fNameBox.Text;
                retObj.lastName = lNameBox.Text;
                retObj.age = ageNumericUpDown.Value;
                retObj.male = maleRadioButton.Checked;
                retObj.female = femaleRadioButton.Checked;

                return retObj;
            }

            set
            {
                fNameBox.Text = value.firstName;
                lNameBox.Text = value.lastName;
                ageNumericUpDown.Value = value.age;
                maleRadioButton.Checked = value.male;
                femaleRadioButton.Checked = value.female;
            }
        }

        public mainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exit the program
            Application.Exit();
        }

        private void ClearData()
        {
            fNameBox.Text = "";
            lNameBox.Text = "";
            ageNumericUpDown.Value = 0;
            maleRadioButton.Checked = false;

        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        // list to store entered data
        string groupList;

        private void addButton_Click(object sender, EventArgs e)
        {
            
            groupList += fNameBox.Text + ;
        }


        /*private void btnOpenInputForm_Click(object sender, EventArgs e)
        {
            // instantiate the input form
            InputForm newWindow = new InputForm();

            // set the new window's event to be handled
            newWindow.CloseAndSave += NewWindow_CloseAndSave;
            newWindow.OnlyClose += NewWindow_OnlyClose;

            // subscribe to a method on the InputForm
            CopyForward += newWindow.CopyForward;

            // invoke the event handler
            CopyForward?.Invoke(this, new EventArgs());

            // show the form as modeless
            newWindow.Show();
        }

        private void NewWindow_OnlyClose(object sender, EventArgs e)
        {
            fNameBox.Clear();
            lNameBox.Clear();
        }

        private void NewWindow_CloseAndSave(object sender, EventArgs e)
        {
            // When we call this method, the InputForm will be closed.
            // But, the sender object to be the the InputForm, which will
            // have all the data from that form.

            // create the instance of the sender as InputForm
            InputForm theInputForm = (InputForm)sender;

            // get the text values from the closed window and set the
            // read-only textboxes using those values
            TextValues = theInputForm.TextValues;
        }*/

    }
}
