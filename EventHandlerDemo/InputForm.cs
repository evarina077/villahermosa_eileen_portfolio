﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventHandlerDemo
{
    public partial class InputForm : Form
    {
        /*// public event handler declarations
        public event EventHandler CloseAndSave;

        public event EventHandler OnlyClose;

        // boolean to determine how the form was closed
        public bool IsClosingAndSaving = false;

        // property  for modifying and extracting the textbox values
        public DataHold TextValues
        {
            get
            {
                DataHold retObj = new DataHold();
                retObj.firstName = fNameBox.Text;
                retObj.lastName = lNameBox.Text;
                retObj.age = ageBox.Text;
                return retObj;
            }

            set
            {
                fNameBox.Text = value.firstName;
                lNameBox.Text = value.lastName;
                ageBox.Text = value.age;
            }
        }

        public InputForm()
        {
            InitializeComponent();
        }

        private void btnCloseAndCopy_Click(object sender, EventArgs e)
        {
            IsClosingAndSaving = true;
            Close();
        }

        private void InputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // This handler gets called whenever the form is closed.
            // We need to check if the button was clicked.

            if (IsClosingAndSaving && CloseAndSave != null)
            {
                CloseAndSave(this, new EventArgs());
            }
            else if (OnlyClose != null)
            {
                OnlyClose(this, new EventArgs());
            }
        }

        public void CopyForward(object sender, EventArgs e)
        {
            // instantiate Form1
            mainForm copyData = (mainForm)sender;

            // use the property to populate the textboxes
            TextValues = copyData.TextValues;
        }
    }
}
