﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE04
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18


    public partial class ListViewForm : Form
    {
        // Declared the event handler for subscription
        public event EventHandler ClearForm;
        public event EventHandler PopulateWindow;

        public ListViewForm()
        {
            InitializeComponent();
        }

        // To set the selection from the List View
        public ListViewItem selectedStudent
        {
            get
            {
                if (listViewInput.SelectedItems.Count > 0)
                {
                    return listViewInput.SelectedItems[0];
                }
                return null;
            }
        }

        // To populate the list view
        public void PopulateList(object sender, EventArgs e)
        {
            listViewInput.Items.Clear();

            // This will capture all data input
            // To add all data into the list view
            foreach (Student student in MainForm.userList)
            {
                ListViewItem lvi = new ListViewItem();

                lvi.Text = student.FirstName;
                lvi.Text = student.LastName;
                lvi.ImageIndex = student.ImageIndex;
                lvi.Tag = student;

                listViewInput.Items.Add(lvi);
            }
        }

        // To update the list view
        public void UpdateListView(object sender, EventArgs e)
        {
            // This will capture all data input
            // To update all data into the list view
            foreach (Student student in MainForm.userList)
            {
                ListViewItem lvi = new ListViewItem();

                lvi.Text = student.FirstName;
                lvi.Text = student.LastName;
                lvi.ImageIndex = student.ImageIndex;
                lvi.Tag = student;

                listViewInput.Items.Add(lvi);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            // To clear the form
            listViewInput.Items.Clear();
            ClearForm(this, new EventArgs());
        }


        public void ClearListView(object sender, EventArgs e)
        {
            // To clear the list view
            listViewInput.Items.Clear();
        }

        private void listViewInput_DoubleClick(object sender, EventArgs e)
        {
            // To show input window when a student is selected
            StudentInputForm student = new StudentInputForm();

            student.StudentInput = (Student)selectedStudent.Tag;

            student.Show();

            // To add to count of open input window
            MainForm.windowCount++;

            // To call to Event Handler
            PopulateWindow(this, new EventArgs());
            
        }

    }
    
 }
