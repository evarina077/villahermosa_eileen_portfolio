﻿namespace EileenVillahermosa_CE04
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exit_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clear_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.display_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtDisplayUserInputWindowsCount = new System.Windows.Forms.TextBox();
            this.userInputWindowsCount = new System.Windows.Forms.Label();
            this.txtDisplayListViewObjectCount = new System.Windows.Forms.TextBox();
            this.listView_StudentCount = new System.Windows.Forms.Label();
            this.btn_OpenUserInputForm = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(498, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exit_ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exit_ToolStripMenuItem
            // 
            this.exit_ToolStripMenuItem.Name = "exit_ToolStripMenuItem";
            this.exit_ToolStripMenuItem.Size = new System.Drawing.Size(151, 38);
            this.exit_ToolStripMenuItem.Text = "Exit";
            this.exit_ToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clear_ToolStripMenuItem,
            this.display_ToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(62, 36);
            this.listToolStripMenuItem.Text = "List";
            // 
            // clear_ToolStripMenuItem
            // 
            this.clear_ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.clear_ToolStripMenuItem.Name = "clear_ToolStripMenuItem";
            this.clear_ToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.clear_ToolStripMenuItem.Text = "Clear";
            this.clear_ToolStripMenuItem.Click += new System.EventHandler(this.clear_ToolStripMenuItem_Click);
            // 
            // display_ToolStripMenuItem
            // 
            this.display_ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.display_ToolStripMenuItem.Name = "display_ToolStripMenuItem";
            this.display_ToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.display_ToolStripMenuItem.Text = "Display";
            this.display_ToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // txtDisplayUserInputWindowsCount
            // 
            this.txtDisplayUserInputWindowsCount.Location = new System.Drawing.Point(348, 146);
            this.txtDisplayUserInputWindowsCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayUserInputWindowsCount.Name = "txtDisplayUserInputWindowsCount";
            this.txtDisplayUserInputWindowsCount.ReadOnly = true;
            this.txtDisplayUserInputWindowsCount.Size = new System.Drawing.Size(80, 31);
            this.txtDisplayUserInputWindowsCount.TabIndex = 1;
            this.txtDisplayUserInputWindowsCount.Text = "0";
            // 
            // userInputWindowsCount
            // 
            this.userInputWindowsCount.AutoSize = true;
            this.userInputWindowsCount.Location = new System.Drawing.Point(58, 148);
            this.userInputWindowsCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.userInputWindowsCount.Name = "userInputWindowsCount";
            this.userInputWindowsCount.Size = new System.Drawing.Size(266, 25);
            this.userInputWindowsCount.TabIndex = 2;
            this.userInputWindowsCount.Text = "User Input Windows Count";
            // 
            // txtDisplayListViewObjectCount
            // 
            this.txtDisplayListViewObjectCount.Location = new System.Drawing.Point(348, 198);
            this.txtDisplayListViewObjectCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtDisplayListViewObjectCount.Name = "txtDisplayListViewObjectCount";
            this.txtDisplayListViewObjectCount.ReadOnly = true;
            this.txtDisplayListViewObjectCount.Size = new System.Drawing.Size(80, 31);
            this.txtDisplayListViewObjectCount.TabIndex = 3;
            this.txtDisplayListViewObjectCount.Text = "0";
            // 
            // listView_StudentCount
            // 
            this.listView_StudentCount.AutoSize = true;
            this.listView_StudentCount.Location = new System.Drawing.Point(96, 202);
            this.listView_StudentCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.listView_StudentCount.Name = "listView_StudentCount";
            this.listView_StudentCount.Size = new System.Drawing.Size(241, 25);
            this.listView_StudentCount.TabIndex = 4;
            this.listView_StudentCount.Text = "List View Student Count";
            // 
            // btn_OpenUserInputForm
            // 
            this.btn_OpenUserInputForm.BackColor = System.Drawing.Color.MistyRose;
            this.btn_OpenUserInputForm.Location = new System.Drawing.Point(100, 69);
            this.btn_OpenUserInputForm.Margin = new System.Windows.Forms.Padding(4);
            this.btn_OpenUserInputForm.Name = "btn_OpenUserInputForm";
            this.btn_OpenUserInputForm.Size = new System.Drawing.Size(296, 48);
            this.btn_OpenUserInputForm.TabIndex = 5;
            this.btn_OpenUserInputForm.Text = "Open User Input Form";
            this.btn_OpenUserInputForm.UseVisualStyleBackColor = false;
            this.btn_OpenUserInputForm.Click += new System.EventHandler(this.btnOpenUserInputForm_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(498, 273);
            this.Controls.Add(this.btn_OpenUserInputForm);
            this.Controls.Add(this.listView_StudentCount);
            this.Controls.Add(this.txtDisplayListViewObjectCount);
            this.Controls.Add(this.userInputWindowsCount);
            this.Controls.Add(this.txtDisplayUserInputWindowsCount);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Main Form";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exit_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clear_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem display_ToolStripMenuItem;
        private System.Windows.Forms.TextBox txtDisplayUserInputWindowsCount;
        private System.Windows.Forms.Label userInputWindowsCount;
        private System.Windows.Forms.TextBox txtDisplayListViewObjectCount;
        private System.Windows.Forms.Label listView_StudentCount;
        private System.Windows.Forms.Button btn_OpenUserInputForm;
    }
}

