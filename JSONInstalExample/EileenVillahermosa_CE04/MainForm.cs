﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EileenVillahermosa_CE04
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18

    public partial class MainForm : Form
    {
        // Declared varaibles for counts        
        public static int studentCount;
        public static int windowCount;

        // Declared Event Handlers for subscriptions
        public event EventHandler PopulateList; // subscribed to the ListViewForm
        public event EventHandler ClearViewList; // subscrbed to the ListVieForm


        // The list to hold all values
        public static List<Student> userList = new List<Student>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // To exit the program
            Application.Exit();
        }

        public void btnOpenUserInputForm_Click(object sender, EventArgs e)
        { 
            // To add open window count
            windowCount++;

            // To instantiate the User Input Window form
            StudentInputForm student = new StudentInputForm();

            // Subscriptions
            student.AddUserToList += AddUserToList;
            student.CloseWindow += CloseWindow;
            student.UpdateListView += UpdateListView;

            // The modeless style of dialog
            student.Show();

            // To start counting windows
            WindowCounter();
        }

        // Method to count user input windows
        public void WindowCounter()
        {
            txtDisplayUserInputWindowsCount.Text = windowCount.ToString();
        }

        // Method to count student input added
        public void StudentCounter()
        {
            txtDisplayListViewObjectCount.Text = studentCount.ToString();
        }

        public void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // To display check when Display is clicked
            // Display can only be clicked once if already opened
            // To display the list view of students
            display_ToolStripMenuItem.Checked = true;
            clear_ToolStripMenuItem.Checked = false;

            if (Application.OpenForms.OfType<ListViewForm>().Count() < 0 || Application.OpenForms.OfType<ListViewForm>().Count() > 0)
            {
                MessageBox.Show("You can only have one open Display Window at a time!");
                return;
            }

            else
            {
                ListViewForm lvf = new ListViewForm();

                PopulateList += lvf.PopulateList;
                lvf.ClearForm += ClearForm;
                ClearViewList += lvf.ClearListView;
                lvf.PopulateWindow += PopulateWindow;


                if (PopulateList != null)
                {
                    PopulateList(this, new EventArgs());
                }

                lvf.Show();
            }
        }


        private void AddUserToList(object sender, EventArgs e)
        {
            StudentInputForm student = new StudentInputForm();

            student = (sender as StudentInputForm);

            // To add to list 
            userList.Add(student.StudentInput); 
            studentCount++;
            StudentCounter();
        }

        public void CloseWindow(object sender, EventArgs e)
        {
            // Call to window count method
            WindowCounter();
        }

        public void UpdateListView(object sender, EventArgs e)
        {
            // To check if list is not null
            // If not not null it will update list
            if(PopulateList != null)
            {
                PopulateList(this, new EventArgs());
            }
        }

        private void ClearForm(object sender, EventArgs e)
        {
            // To clear forms
            userList.Clear();
            studentCount = 0;
            txtDisplayListViewObjectCount.Text = studentCount.ToString();

        }

        private void clear_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // To clear forms and list
            // Toggle the check mark from display to true
            display_ToolStripMenuItem.Checked = false;
            clear_ToolStripMenuItem.Checked = true;

            ClearForm(sender, new EventArgs());
            ClearViewList(this, new EventArgs());

        }
        
        public void PopulateWindow(object sender, EventArgs e)
        {
            WindowCounter();
        }

    }
}
