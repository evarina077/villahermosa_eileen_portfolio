﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EileenVillahermosa_CE04
{

    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18


    // class created for user data input
    public class Student
    {
        // variables for the user input
        public string FirstName;
        public string LastName;
        public decimal Age;
        public bool ButtonOnlineStudent;
        public bool ButtonCampusStudent;
        public string comboxBox_Gender;
        public int ImageIndex;

        // override method for proper display of data input into list
        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }


}
