﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EileenVillahermosa_CE04
{
    // Eileen Villahermosa
    // VFW Term 01
    // CE04: List Views
    // 03/05/18


    public partial class StudentInputForm : Form
    {
        // Declared the event handler for subscription
        public event EventHandler AddUserToList;
        public event EventHandler CloseWindow;
        public event EventHandler UpdateListView;

        public StudentInputForm()
        {
            InitializeComponent();
        }

        // Data variable created to form all user data input
        // To call to User Class
        public Student StudentInput
        {
            // accessor return the data input values
            get
            {
                Student student = new Student();

                student.FirstName = textFirstName.Text;
                student.LastName = textLastName.Text;
                student.Age = numericAge.Value;
                student.ButtonOnlineStudent = radioButtonOnline.Checked;
                student.comboxBox_Gender = comboBox_Gender.Text;

                if (radioButtonOnline.Checked) 
                {
                    student.ImageIndex = 1;
                }
                else
                {
                    student.ImageIndex = 0;
                }

                return student;
            }

            // accessor to set the data input values
            set
            {
                // If statement to make sure values are not null
                // To prevent exception errors
                if (value != null)
                {
                    textFirstName.Text = value.FirstName;
                    textLastName.Text = value.LastName;
                    numericAge.Value = value.Age;
                    radioButtonOnline.Checked = value.ButtonOnlineStudent;
                    comboBox_Gender.Text = value.comboxBox_Gender;
                }
            }
        }

        
        public void buttonReset_Click(object sender, EventArgs e)
        {
            // Methods to reset all data input
            textFirstName.Clear();
            textLastName.Clear();
            numericAge.Value = 0;
            radioButtonOnline.Checked = false;
            comboBox_Gender.Text = null;
        }

        //
        public void toolStripButtonAdd_Click(object sender, EventArgs e)
        {

            // To check the event handler to avoid null error
            if (AddUserToList != null)
            {
                AddUserToList(this, new EventArgs());
            }

            if (UpdateListView != null)
            {
                UpdateListView(this, new EventArgs());
            }

            // Method to clear once data has been added/updated
            textFirstName.Clear();
            textLastName.Clear();
            numericAge.Value = 0;
            radioButtonOnline.Checked = false;
            comboBox_Gender.Text = null;

        }

        private void StudentInput_FormClosed(object sender, FormClosedEventArgs e)
        {
            // To deduct the input window form count when closed
            MainForm.windowCount--;

            if (CloseWindow != null)
            {
                CloseWindow(this, new EventArgs());
            }
        }

    }
}
