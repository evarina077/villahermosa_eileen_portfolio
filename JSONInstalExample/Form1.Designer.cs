﻿namespace JSONInstalExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_WeatherData = new System.Windows.Forms.GroupBox();
            this.radioButton_XML = new System.Windows.Forms.RadioButton();
            this.radioButton_JSON = new System.Windows.Forms.RadioButton();
            this.label_selectState = new System.Windows.Forms.Label();
            this.comboBox_State = new System.Windows.Forms.ComboBox();
            this.textBox_CityTown = new System.Windows.Forms.TextBox();
            this.label_enterCityTown = new System.Windows.Forms.Label();
            this.button_viewWeatherData = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_CityState = new System.Windows.Forms.TextBox();
            this.numericUpDown_CurrentTemp = new System.Windows.Forms.NumericUpDown();
            this.saveAsXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsJSONToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.groupBox_WeatherData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CurrentTemp)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_File});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(680, 42);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem_File
            // 
            this.toolStripMenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.loadXMLToolStripMenuItem,
            this.saveAsXMLToolStripMenuItem,
            this.saveAsJSONToolStripMenuItem,
            this.toolStripMenuItem_Exit});
            this.toolStripMenuItem_File.Name = "toolStripMenuItem_File";
            this.toolStripMenuItem_File.Size = new System.Drawing.Size(64, 38);
            this.toolStripMenuItem_File.Text = "File";
            // 
            // toolStripMenuItem_Exit
            // 
            this.toolStripMenuItem_Exit.Name = "toolStripMenuItem_Exit";
            this.toolStripMenuItem_Exit.Size = new System.Drawing.Size(151, 38);
            this.toolStripMenuItem_Exit.Text = "Exit";
            this.toolStripMenuItem_Exit.Click += new System.EventHandler(this.toolStripMenuItem_Exit_Click);
            // 
            // groupBox_WeatherData
            // 
            this.groupBox_WeatherData.Controls.Add(this.radioButton_XML);
            this.groupBox_WeatherData.Controls.Add(this.radioButton_JSON);
            this.groupBox_WeatherData.Controls.Add(this.label_selectState);
            this.groupBox_WeatherData.Controls.Add(this.comboBox_State);
            this.groupBox_WeatherData.Controls.Add(this.textBox_CityTown);
            this.groupBox_WeatherData.Controls.Add(this.label_enterCityTown);
            this.groupBox_WeatherData.Location = new System.Drawing.Point(13, 44);
            this.groupBox_WeatherData.Name = "groupBox_WeatherData";
            this.groupBox_WeatherData.Size = new System.Drawing.Size(617, 243);
            this.groupBox_WeatherData.TabIndex = 1;
            this.groupBox_WeatherData.TabStop = false;
            this.groupBox_WeatherData.Text = "Get Weather Data";
            // 
            // radioButton_XML
            // 
            this.radioButton_XML.AutoSize = true;
            this.radioButton_XML.Checked = true;
            this.radioButton_XML.Location = new System.Drawing.Point(315, 56);
            this.radioButton_XML.Name = "radioButton_XML";
            this.radioButton_XML.Size = new System.Drawing.Size(87, 29);
            this.radioButton_XML.TabIndex = 5;
            this.radioButton_XML.TabStop = true;
            this.radioButton_XML.Text = "XML";
            this.radioButton_XML.UseVisualStyleBackColor = true;
            // 
            // radioButton_JSON
            // 
            this.radioButton_JSON.AutoSize = true;
            this.radioButton_JSON.Location = new System.Drawing.Point(178, 56);
            this.radioButton_JSON.Name = "radioButton_JSON";
            this.radioButton_JSON.Size = new System.Drawing.Size(99, 29);
            this.radioButton_JSON.TabIndex = 4;
            this.radioButton_JSON.Text = "JSON";
            this.radioButton_JSON.UseVisualStyleBackColor = true;
            // 
            // label_selectState
            // 
            this.label_selectState.AutoSize = true;
            this.label_selectState.Location = new System.Drawing.Point(142, 157);
            this.label_selectState.Name = "label_selectState";
            this.label_selectState.Size = new System.Drawing.Size(149, 25);
            this.label_selectState.TabIndex = 3;
            this.label_selectState.Text = "Select a state:";
            // 
            // comboBox_State
            // 
            this.comboBox_State.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_State.FormattingEnabled = true;
            this.comboBox_State.Items.AddRange(new object[] {
            "Alabama",
            "Alaska",
            "Arizona",
            "Arkansas",
            "California",
            "Colorado",
            "Connecticut",
            "Delaware",
            "Florida",
            "Georgia",
            "Hawaii",
            "Idaho",
            "Illinois",
            "Indiana",
            "Iowa",
            "Kansas",
            "Kentucky",
            "Louisiana",
            "Maine",
            "Maryland",
            "Massachusetts",
            "Michigan",
            "Minnesota",
            "Mississippi",
            "Missouri",
            "Montana",
            "Nebraska",
            "Nevada",
            "New Hampshire",
            "New Jersey",
            "New Mexico",
            "New York",
            "North Carolina",
            "North Dakota",
            "Ohio",
            "Oklahoma",
            "Oregon",
            "Pennsylvania",
            "Rhode Island",
            "South Carolina",
            "South Dakota",
            "Tennessee",
            "Texas",
            "Utah",
            "Vermont",
            "Virginia",
            "Washington",
            "West Virginia",
            "Wisconsin",
            "Wyoming"});
            this.comboBox_State.Location = new System.Drawing.Point(297, 154);
            this.comboBox_State.Name = "comboBox_State";
            this.comboBox_State.Size = new System.Drawing.Size(218, 33);
            this.comboBox_State.TabIndex = 2;
            // 
            // textBox_CityTown
            // 
            this.textBox_CityTown.Location = new System.Drawing.Point(297, 107);
            this.textBox_CityTown.Name = "textBox_CityTown";
            this.textBox_CityTown.Size = new System.Drawing.Size(244, 31);
            this.textBox_CityTown.TabIndex = 1;
            // 
            // label_enterCityTown
            // 
            this.label_enterCityTown.AutoSize = true;
            this.label_enterCityTown.Location = new System.Drawing.Point(44, 110);
            this.label_enterCityTown.Name = "label_enterCityTown";
            this.label_enterCityTown.Size = new System.Drawing.Size(247, 25);
            this.label_enterCityTown.TabIndex = 0;
            this.label_enterCityTown.Text = "Please enter a city/town:";
            // 
            // button_viewWeatherData
            // 
            this.button_viewWeatherData.Location = new System.Drawing.Point(385, 304);
            this.button_viewWeatherData.Name = "button_viewWeatherData";
            this.button_viewWeatherData.Size = new System.Drawing.Size(244, 54);
            this.button_viewWeatherData.TabIndex = 4;
            this.button_viewWeatherData.Text = "View Weather Data";
            this.button_viewWeatherData.UseVisualStyleBackColor = true;
            this.button_viewWeatherData.Click += new System.EventHandler(this.button_viewWeatherData_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(162, 436);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Current Temperature:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(226, 385);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "City and State:";
            // 
            // textBox_CityState
            // 
            this.textBox_CityState.Location = new System.Drawing.Point(385, 382);
            this.textBox_CityState.Name = "textBox_CityState";
            this.textBox_CityState.ReadOnly = true;
            this.textBox_CityState.Size = new System.Drawing.Size(245, 31);
            this.textBox_CityState.TabIndex = 7;
            // 
            // numericUpDown_CurrentTemp
            // 
            this.numericUpDown_CurrentTemp.DecimalPlaces = 1;
            this.numericUpDown_CurrentTemp.Location = new System.Drawing.Point(385, 434);
            this.numericUpDown_CurrentTemp.Name = "numericUpDown_CurrentTemp";
            this.numericUpDown_CurrentTemp.ReadOnly = true;
            this.numericUpDown_CurrentTemp.Size = new System.Drawing.Size(120, 31);
            this.numericUpDown_CurrentTemp.TabIndex = 8;
            // 
            // saveAsXMLToolStripMenuItem
            // 
            this.saveAsXMLToolStripMenuItem.Name = "saveAsXMLToolStripMenuItem";
            this.saveAsXMLToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.saveAsXMLToolStripMenuItem.Text = "Save as XML";
            this.saveAsXMLToolStripMenuItem.Click += new System.EventHandler(this.saveAsXMLToolStripMenuItem_Click);
            // 
            // saveAsJSONToolStripMenuItem
            // 
            this.saveAsJSONToolStripMenuItem.Name = "saveAsJSONToolStripMenuItem";
            this.saveAsJSONToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.saveAsJSONToolStripMenuItem.Text = "Save as JSON";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(265, 6);
            // 
            // loadXMLToolStripMenuItem
            // 
            this.loadXMLToolStripMenuItem.Name = "loadXMLToolStripMenuItem";
            this.loadXMLToolStripMenuItem.Size = new System.Drawing.Size(268, 38);
            this.loadXMLToolStripMenuItem.Text = "Load XML";
            this.loadXMLToolStripMenuItem.Click += new System.EventHandler(this.loadXMLToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 553);
            this.Controls.Add(this.numericUpDown_CurrentTemp);
            this.Controls.Add(this.textBox_CityState);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_viewWeatherData);
            this.Controls.Add(this.groupBox_WeatherData);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox_WeatherData.ResumeLayout(false);
            this.groupBox_WeatherData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CurrentTemp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Exit;
        private System.Windows.Forms.GroupBox groupBox_WeatherData;
        private System.Windows.Forms.Label label_selectState;
        private System.Windows.Forms.ComboBox comboBox_State;
        private System.Windows.Forms.TextBox textBox_CityTown;
        private System.Windows.Forms.Label label_enterCityTown;
        private System.Windows.Forms.Button button_viewWeatherData;
        private System.Windows.Forms.RadioButton radioButton_XML;
        private System.Windows.Forms.RadioButton radioButton_JSON;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_CityState;
        private System.Windows.Forms.NumericUpDown numericUpDown_CurrentTemp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem saveAsXMLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsJSONToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadXMLToolStripMenuItem;
    }
}

