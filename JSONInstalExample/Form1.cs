﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// add the namespaces for the parts we want to use
using Newtonsoft.Json;
using Newtonsoft.Json.Linq; // allow to use databases
using System.Net; // gives access to the internet
using System.IO;
using System.Xml;

namespace JSONInstalExample
{
    public partial class Form1 : Form
    {
        // add instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();
        // API starting point
        string apiStaringPoint = "http://api.wunderground.com/api/b1c71e583be1265e/conditions/q/";
        // API ending point
        string apiEndingPoint;

        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private string ReturnStateAbbrev()
        {
            string abbrev = "";

            string[] stateAbbrev = new string[] {"AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"};

            // Match the state to the abbreviation
            abbrev = stateAbbrev[comboBox_State.SelectedIndex];

            return abbrev;
        }

        private void BuildAPI()
        {
            // get the state abbreviation
            string abbrev = ReturnStateAbbrev();

            // replace any spaces in the city
            string city = textBox_CityTown.Text.Replace(' ', '_');

            // check to see what data the suer wants, JSON or XML
            string jsonOrXml = ".xml";

            if (radioButton_JSON.Checked)
            {
                jsonOrXml = ".json";
            }

            // complete the API
            apiEndingPoint = apiStaringPoint + abbrev + "/" + city + jsonOrXml;

            // output the API string to a textbox
            //textBox_CityState.Text = apiEndingPoint;
        }

        private void button_viewWeatherData_Click(object sender, EventArgs e)
        {
            BuildAPI();
            // call the correct method based on the user's choice

            if (radioButton_JSON.Checked)
                ReadTheJSONData();
            else
                ReadTheXMLData();
        }

        // method reads the JSON data
        private void ReadTheJSONData()
        {
            // download the data as a string
            var apiData = apiConnection.DownloadString(apiEndingPoint);

            // See the string
            // MessageBox.Show("The string: \n" + apiData);

            // convert the string to an object
            JObject o = JObject.Parse(apiData);

            // find specific data in the object and view that in the message box
            string specifics = o["current_observation"]["display_location"]["full"].ToString();

            // see the object as a string
            MessageBox.Show(specifics);
        }

        // method read the XML data
        private void ReadTheXMLData()
        {
            int i = 0;
            string city = "";
            decimal temp = 0;
            // Creete an XMLReader. Because it's an abstract class
            // we use the Create method. We'll also use "using"
            // to ensure it closes and is disposed of
            using (XmlReader apiData = XmlReader.Create(apiEndingPoint))
            {
                // loop through the XML data
                while(apiData.Read())
                {
                    if(apiData.Name == "full" && i == 0)
                    {
                        city = apiData.ReadElementContentAsString();
                        i = 1;
                    }

                    if(apiData.Name == "temp_f")
                    {
                        temp = apiData.ReadElementContentAsDecimal();
                    }
                }
                // put the values from the XML onto the form
                textBox_CityState.Text = city;
                numericUpDown_CurrentTemp.Value = temp;
            }
        }

        private void saveAsXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // use the SaveFileDialog class
            SaveFileDialog dlg = new SaveFileDialog();

            // we'll also add a default extension to be sure the file
            // is saved as XML
            dlg.DefaultExt = "xml";

            // if the user clicks OK
            if(DialogResult.OK == dlg.ShowDialog())
            {
                // the first thin we'll do is instantiate an 
                // XMLWritterSettings object
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                // we'll also set the indent to true to ensure the file
                // gets the proper indentation
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {
                    writer.WriteStartElement("WeatherData");

                    writer.WriteElementString("City", textBox_CityState.Text);

                    writer.WriteElementString("Temp", numericUpDown_CurrentTemp.Text);

                    writer.WriteEndElement();
                 }     
            }
        }

        private void loadXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // create the open file dialog
            OpenFileDialog dlg = new OpenFileDialog();

            // verify the use clicked OK
            if (DialogResult.OK == dlg.ShowDialog())
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                // we want to make sure our reader gets only the XML
                // so we'll set it to ignore comments and whitespace
                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;

                // now we'll start the leader
                using (XmlReader reader = XmlReader.Create(dlg.FileName, settings))
                {
                    // skip the metadata
                    reader.MoveToContent();

                    // verify that this is the weather data
                    if (reader.Name != "WeatherData")
                    {
                        // return that this is not right
                        MessageBox.Show("This is not the correct weather data");
                        return;
                    }

                    while (reader.Read())
                    {
                        // because there might be some data other than 
                        // "City" or "Temp" let's make sure we are
                        // reading the data we can use
                        if (reader.Name == "City" && reader.IsStartElement())
                        {
                            textBox_CityState.Text = reader.ReadElementContentAsString();

                        }

                        if (reader.Name == "Temp" && reader.IsStartElement())
                        {
                            numericUpDown_CurrentTemp.Value = reader.ReadElementContentAsDecimal();
                        }
                    }
                }
            }
        }
    }
}
