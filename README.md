README

README for Project & Portfolio II Final Project

    The purpose of this repository is to demonstrate skills set learned from previous classes,
	and use learned JavaScript skills to create a console game or program that will perform 
	a set of simple tasks.

Project Requirements

    ECMA6 JS (refer to week 2 research assignments!)
    Variables
    Expressions / Use of Math Object (random)
    Conditional Statements
    Ternary Statement (at least one)
    User Input (using prompt)
    Console.log output
    Validation of Prompts
    Arrays / Array Methods
    Loops (For and While Loops)
    Functions (arguments, parameters, return values)
    JSON / Object Literal (Must include nested objects)
    ECMA6 - Class Defnition / OOP Principles (50 POINTS!)
    Adequate Comments / Pseudocode

Contact

    Eileen Villahermosa - evarina077@student.fullsail.edu

