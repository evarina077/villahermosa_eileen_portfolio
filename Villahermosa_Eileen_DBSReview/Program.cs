﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Villahermosa_Eileen_DBSReview
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=192.168.38.1;userid=dbsAdmin;password=password;database=SampleAPIData; port=8889";
            MySqlConnection conn = null;

            while (true)
            {
                Console.WriteLine("\nLet's get the current weather for a city!");
                Console.Write("\nPlease enter a city: ");
                string city = Console.ReadLine();

                while (string.IsNullOrWhiteSpace(city))
                {
                    Console.Write("\r\nSorry please don't leave blank. Please re-type and hit enter: ");
                    city = Console.ReadLine();
                }


                try
                {
                    conn = new MySqlConnection(cs);
                    MySqlDataReader rdr = null;
                    conn.Open();

                    string stm = "SELECT * FROM weather WHERE city LIKE '%" + city + "%' AND createdDate = (SELECT MAX(createdDate) from weather where city LIKE '%" + city + "%')";

                    //string stm = "SELECT * from weather where city like '%" + city + "%' limit 1";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);

                    rdr = cmd.ExecuteReader();

                    if (!rdr.HasRows)
                    {
                        Console.WriteLine("\nNo Data Available for the selected city!");
                        Console.Write("Press any key to try again...");
                        Console.ReadKey();
                        Console.Clear();
                    }
                    else
                    {
                        while (rdr.Read())
                        {
                            Console.WriteLine($"\n\n\tCurrent Weather Stats for {city}");
                            Console.WriteLine("\t=======================================");
                            Console.WriteLine($"\tTemperature: {rdr.GetInt32(4)}");
                            Console.WriteLine($"\tPressure: {rdr.GetString(5)}");
                            Console.WriteLine($"\tHumidity: { rdr.GetString(6)}");
                        }

                        break;

                    }

                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error: {0}", ex.ToString());

                }
                finally
                {

                    if (conn != null)
                    {
                        conn.Close();
                    }

                }
            }

            Console.WriteLine("\n\nThanks for using the program!");
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        //public static string TextValidation(string _text)
        //{
        //    while (string.IsNullOrWhiteSpace(_text))
        //    {
        //        Console.Write("\r\nSorry please don't leave blank. Please re-type and hit enter: ");
        //        _text = Console.ReadLine();
        //    }

        //    return _text;
        }

    }

