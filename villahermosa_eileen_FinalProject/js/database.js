//JSON/Object Literal - nested array of all fortune cookie data
var db = [
"2: Plan for many pleasures ahead!<h4>Lotto Numbers: 04-37-39-43-44-46</h4>",
"3: Your everlasting patience will be rewarded sooner or later!<h4>Lotto Numbers: 09-26-17-56-21-23</h4>",
"4: A pleasant surprise is in store for you!<h4>Lotto Numbers: 18-47-41-25-04-06</h4>",
"5: Your many hidden talents will become obvious to those around you!<h4>Lotto Numbers: 36-03-34-02-37-54</h4>",
"6: A firm friendship will prove the foundation on your success in life!<h4>Lotto Numbers: 05-24-03-30-22-23</h4>",
"7: The time is right to make new friends!<h4>Lotto Numbers: 48-16-23-29-38-54</h4>",
"8: You will inherit some money or a small piece of land!<h4>Lotto Numbers: 12-14-24-25-29-44</h4>",
"9: A member of your family will soon do something that will make you proud!<h4>Lotto Numbers: 36-55-04-26-34-52</h4>",
"10: Happy news is on its way to you!<h4>Lotto Numbers: 37-54-38-24-31-48</h4>",
"11: You will take a chance in something in the near future!<h4>Lotto Numbers: 44-32-53-18-30-22</h4>",
"12: You will witness a special ceremony!<h4>Lotto Numbers: 07-09-17-33-36-44</h4>",
"13: Your ability to juggle many tasks will take you far!<h4>Lotto Numbers: 24-23-02-27-21-18</h4>",
"14: You will be invited to an exciting event!<h4>Lotto Numbers: 07-54-35-01-08-36</h4>",
"15: It's up to you to create the peacefulness you long for!<h4>Lotto Numbers: 50-44-33-12-36-18</h4>",
"16: A chance meeting opens new doors to success and friendship!<h4>Lotto Numbers: 24-17-29-37-14-46</h4>",
"17: A dream you have will come true!<h4>Lotto Numbers: 17:05-12-19-26-33-40</h4>",
"18: What ever you're goal is in life, embrace it visualize it, and for it will be yours!<h4>Lotto Numbers: 56-44-10-42-22-51</h4>",
"19: A thrilling time is in your immediate future!<h4>Lotto Numbers: 21-34-16-25-47-09</h4>"
];
