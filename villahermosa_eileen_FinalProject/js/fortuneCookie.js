/*  Eileen Villahermosa
    WDD229-O 01
    Project and Portfolio II: Web Design and Development
    Assignment: Final Project
    Requirements: Write a JavaScript game or application that will performs a set of simple tasks:
        -ECMA6 JS
        -Variables
        -Expressions/Use of Math Object(random)
        -Conditional Statements
        -Ternary Statements(at least one)
        -Console.log output
        -Validation of Prompts
        -Arrays/Array Methods
        -Loops(For and While Loops)
        -Functions(arguments, parameters, return values)
        -JSON/Object Literal(Must include nested objects)
        -ECMA6 - Class Definition/OOP Principles
        -Adequate Comments/Pseudocode

    Final Project: Fortune Cookie
    Synopsis: Will prompt user for their name, a number between 1-7, and their birth month number, 1-12
    (i.e. for May, they would type in 5). The program will then add the number and birth number together.
    The total of the two numbers will then be compared to the fortune cookie database. Once the match is
    found, it will then output to the user thier fortune.
*/

/*ECMA JS used throughout the program (for example: String interpolation, Class Inheritance by using "extends",
base class access, constant and let, etc.)*/

/*Variables - Main variables declared (nameInput will be used to retrieve the name entered by the user,
numberInput will be used to retrieve the chosen number entered, birthNumberInput will be used to retrieve
the entered birth number by the user, fortuneResult will be used to display the fortune cookie result to user,
resultsName will display the user's name as part of the header when displaying their fortune cookie result.)*/
let nameInput = document.forms[0].name, numberInput = document.forms[0].number, birthMonthInput = document.forms[0].birthMonth,
fortuneResult = document.getElementById("fortuneResult"), resultsName = document.getElementById("header");

/*Class Definition/OOP Principles and Validation of prompts - A validator class to store all functions to make sure user enters
a valid name (no blanks/whitespaces or numbers), a valid number between 1-7 and birth number between 1-12(no
blanks/whitspaces or letters)*/
class Validator
{
  //Functions - to validate name input
  validateName(nameInput)
  {
    //Conditional Statements - If user enters blank or whitespace, it will alert user invalid entry
    if (nameInput === "" || nameInput === " ")
    {
      alert("Invalid Name: Please don't leave blank and re-enter you first name.");
      //false used for return option to allow user to re-enter name
      return false;
    }
    //Conditional Statements - Else return name input for use
    else
    {
      //return name for use
      return nameInput;
    }

  };

  //Functions - to validate number input
  validateNumber(numberInput)
  {
      //Conditional Statements - If user enters blank, whitespace and numbers not between 1-7, it will alert user invalid entry
      if (numberInput == "" || numberInput == " "|| numberInput < 1 || numberInput > 7 || !(parseInt(numberInput)))
      {
        alert("Invalid Number: Please re-enter with a number between 1-7.");
        //false used for return option to allow user to re-enter number
        return false;
      }
      //Conditional Statements - Else move to next function of the program
      else
      {
        //Call method to getTrim function, which will trim number from spaces or zeros entered prior to input
        getTrim(numberInput);
      }

      //Variable declared to parse the string number input into an int
      let fortuneNumber1 = parseInt(numberInput);
      //Console.log output to confirm number input has been converted to type number
      console.log("Parsed to Int " + fortuneNumber1 + ": " + typeof fortuneNumber1);
      //return number for use
      return fortuneNumber1;
    };

  //Functions - to validate birth number input
  validateBirthNumber(birthNumberInput)
  {
    //Conditional Statements - If user enters blank, whitespace and numbers not between 1-12, it will alert user invalid entry
    if(birthNumberInput == "" || birthNumberInput == " "|| birthNumberInput < 1 || birthNumberInput > 12 || !(parseInt(birthNumberInput)))
    {
      alert("Invalid Birth Month Number: Please re-enter a number between 1-12.");
      //false used for return option to allow user to re-enter birth number
      return false;
    }
    //Conditional Statements - Else move to next function of the program
    else
    {
      //Call method to getTrim function, which will trim number from spaces or zeros entered prior to input
      getTrim(birthNumberInput);
    }

    //Variable declared to parse the string number input into an int
    let fortuneNumber2 = parseInt(birthNumberInput);
    //Console.log output to confirm number input has been converted to type number
    console.log("Parsed to Int " + fortuneNumber2 + ": " + typeof fortuneNumber2);
    //return birth month number for use
    return fortuneNumber2;
  };

};

//Functions - to trim input from spaces or zero before the entered number/letter
function getTrim(input)
{
  //Loops - while loops used to trim input from any spaces or zeros
  while(input.charAt(0) === " " || input.charAt(0) === "0")
  {
    input = input.substring(1, input.length);
  };

  while(input.charAt(input.length-1) === " ")
  {
    input = input.substring(0, input.length-1);
  };

};

/*Class Definition/OOP Principles - Input Parent Class for user input,
encapsulation used for private variable for user input, return for user input
usage*/
class Input
{
  //Constructor for user input
  constructor(userInput)
  {
    this._userInput = userInput;
  }
  //Function getInput to return user input
  getInput()
  {
    //use of String interpolation
    return `${this._userInput}`;
  }
};

/*Class Definition/OOP Principles - Abstract child Class derived from the Input
parent class*/
class Name extends Input
{
  //Constructor method for name input
  constructor(userInput)
  {
    //Inheritance - Super call to parent class
    super(userInput);
  }
  //Function getFortune to move on with the next step of the program
  getFortune()
  {
    //Call method to the showName function
    showName(super.getInput());
  }

};

/*Class Definition/OOP Principles - Abstract child Class derived from the Input
parent class*/
class Number extends Input
{
  //Constructor method for number total from both number inputs together
  constructor(userInput)
  {
    //Inheritance - Super call to parent class
    super(userInput);
    //Arrays/Array Methods - private array to hold matching fortune cookie result from database
    this._result = [];
  }
  /*Polymorphism - Function now used differently from name class to get the matching fortune
  cookie from database*/
  getFortune()
  {
    //Console.log output to show the total of number and birth number input
    console.log("User total: " + super.getInput());
    //Declared variable to hold the number total from the Number class
    let number = super.getInput();

    /*Loops - for loop used to loop through the fortune cookie database and separate
    each fortune cookie by their number (eliminate anything after the semicolon)*/
    for(let i=0, j=db.length; i<j; i++)
    {
      //variable to locate the semicolon
      let dbNumberEnd = db[i].indexOf(':');
      //variable to eliminate all string after the seimcolong
      let dbNumber = db[i].substring(0, dbNumberEnd);

      //variable to compare user total number with all the database numbers
      var compare = dbNumber.indexOf(number);

      /*Conditional Statements - If their is a match of number/number then push
      the fortune cookie that matches from the database into the result array*/
      if(compare !== -1)
      {
      this._result.push(db[i]);
      };
    }

   //Console.log output to show the matching results
   console.log(this._result);

   //Conditional Statements - If result has an object length of 1 then...
   if(this._result.length == 1)
   {
     //Convert the array object into a string
     var str1 = this._result.toString();
     //Loops - While loop to capture the semicolon
     while(str1.charAt(2) === ":")
     {
       //Eliminates any string before the semicolon, so the number will be omitted
       str1 = str1.substring(4, str1.length);
     };

     //Console.log output to confirm array object has been converted to a string
     console.log("String result: " + str1);
     //Call method to the showFortune function
     showFortune(str1);
   }
   /*Conditional Statements - If result has two fortune cookie (i.e. number total
   is 2, so it will push all fortune cookie with 2 in it, so 2 and 12 into the
   database) then only pull the first object in the array*/
   else
   {
     //Convert the array object into a string
     var str2 = this._result[0].toString();

     //Loops - While loop to capture the semicolon
     while(str2.charAt(1) === ":")
     {
       //Eliminates any string before the semicolon, so the number will be omitted
       str2 = str2.substring(3, str2.length);
     };

     //Console.log output to confirm array object has been converted to a string
     console.log("String result: " + str2);
     //Call method to the showFortune function
     showFortune(str2);
   }
 };
};

//Function to output the fortune cookie result to the user
var showFortune = function(result)
{
  //This will place the output into the web page at the assigned content box
  fortuneResult.innerHTML = "<h2>" + result + "</h2>";
};

//Function to ouput the header of the fortune cookie result with user name
var showName = function(userName)
{
  //This will place the hearder into the web pages at the assigned content box
  resultsName.innerHTML = "<h1>" + userName.toUpperCase() + " your fortune cookie says:</h1>";
};

//function to retrieve user input from the html form upon user hitting sumbit
document.forms[0].onsubmit = function()
{
  //Variables - to store the name input from the html form
  let enteredName = nameInput.value;
  //Variables - to call into the Validator class
  let validateName = new Validator();
  //Variables - to get the name input into Validator class's validateName function
  let userName = validateName.validateName(enteredName);
  console.log("User Name: " + userName);
  //Variables - to call into the Name class and store the name input
  let validatedName = new Name(userName);
  //Console.log output to make sure name is retrievable from the Name class


  //Variables - to store the number input from the html form
  let number1 = numberInput.value;
  //Variables - to call into the Validator class
  let validateNumber = new Validator();
  //Variables - to get the name input into Validator class's validateNumber function
  let userNumber = validateNumber.validateNumber(number1);
  //Console.log output to make sure number is retrievable after entering the Number class
  console.log("Number: " + userNumber);

  //Variables - to store the birth number input from the html form
  let number2 = birthMonthInput.value;
  //Variables - to call into the Validator class
  let validateBirthNumber = new Validator();
  //Variables - to get the name input into Validator class's validateBirthNumber function
  let userBirthNumber = validateBirthNumber.validateBirthNumber(number2);
  //Console.log output to make sure number is retrievable after entering from the Number class

  console.log("Birth Month Number: " + userBirthNumber);

  //Variables - to hold false for user input
  var userValidation = false;
  //Expression to calculate both user number and birth number input
  const fortuneNumberTotal = userNumber + userBirthNumber;
  //Variables - to use to call into the Number class
  let getFortune;
  /*Ternary Statements - used to determine if user input of name, number, and birth numbers is valid. If not valid it will not move
  forward with the program and console.log output an error message, else it will proceed with the program and cosole.log output that
  all entered input is valid.*/
  userName === false || userNumber === false || userBirthNumber === false ? (console.log("Error: One or more user input was not valid.")):
  (userValidation = true, console.log("All user input are valid!"), fortuneNumberTotal, getFortune = new Number(fortuneNumberTotal), getFortune.getFortune(), validatedName.getFortune());

  //return false is needed for most events - this has not been reviewed but advised it will be in upcming courses
  return false;
};
