﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_CountFish
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Eileen Villahermosa
            SDI Section #01
            August 13, 2017
            Count The Fish
            */

            //Declare and Define the fish color arrays
            string[] colors = new string[] { "red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green" };

            //Greet the user and prompt user for choic of 
            Console.WriteLine("Welcome to the Fish Store!\r\nPlease type [1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow and press enter:");
            string userChoice = Console.ReadLine();
            double numUserChoice;

            while (!double.TryParse(userChoice, out numUserChoice))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type [1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow and press enter (e.g. 1):");

                //Re-capture the response
                userChoice = Console.ReadLine();
            }
            bool option = true;
            double counter = 0;

            while (option)
            {


                if (numUserChoice == 1)
                {
                    option = false;
                    foreach (string fish in colors)
                    {
                        if (fish == "red")
                            counter++;
                    }

                    Console.WriteLine("There are {0} in the tank with the color red.", counter);
                    option = false;
                }

                else if (numUserChoice == 2)
                {
                    option = false;
                    foreach (string fish in colors)
                    {
                        if (fish == "blue")
                            counter++;
                    }

                    Console.WriteLine("There are {0} in the tank with the color blue.", counter);
                    option = false;
                }

                else if (numUserChoice == 3)
                {
                    option = false;
                    foreach (string fish in colors)
                    {
                        if (fish == "green")
                            counter++;
                    }

                    Console.WriteLine("There are {0} in the tank with the color green.", counter);
                    option = false;
                }

                else if (numUserChoice == 4)
                {
                    option = false;
                    foreach (string fish in colors)
                    {
                        if (fish == "yellow")
                            counter++;
                    }

                    Console.WriteLine("There are {0} in the tank with the color yellow.", counter);
                    option = false;
                }

                else
                {
                 
                    Console.WriteLine("Sorry please type in a number between 1-4 ([1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow) and press enter:");
                    userChoice = Console.ReadLine();

                    if (!double.TryParse(userChoice, out numUserChoice))
                    {
                        //This will run if user type something other than a number
                        //Prompt user to enter a number in the correct format
                        Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type [1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow and press enter (e.g. 1):");

                        //Re-capture the response
                        userChoice = Console.ReadLine();
                       
                    }

            
                }


                /*
                Test that each color choice, 1,2,3, and 4, works from your menu and gives you the
                correct amount of fish.
                • Try typing in the number 6. Does your code ask the user to give a valid choice of 1-4?
                • If you add more fish to the tank, does your code still work?
                • User Choice – 1 for Red
                o Results - “In the fish tank there are 2 fish of the color Red.” 
                */



            }


        }
    }
}
