﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_ArrayLists
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Eileen Villahermosa
            SDI Section #01
            August 18, 2017
            String Objects
            */

            Console.WriteLine("Hello! Let's find out who's who in the Phan Family! Press enter!");
            Console.ReadLine();

            arrayFamily();
        }

        public static void arrayFamily()
        {
            //Create the 1st ArrayList
            ArrayList familyList = new ArrayList() {"Steve","Eileen","Amaya","Tyler"};

            //Create the 2nd ArrayList
            ArrayList relationshipList = new ArrayList() {"Father","Mother","Daughter","Son"};

            int i = 0;

            //Create a loop to go through each family name in the familyList
            foreach (string familyName in familyList)
            {
            
            //Tell the user the family members names and their relationships
            Console.WriteLine($"{familyName} is the {relationshipList[i]} of the family.");

                i++;
            }

            //To create space between the two sets of the grouped arrayList sentences.
            Console.WriteLine("\r\nNow let's change the family members a bit! Press enter!");
            Console.ReadLine();

            //Remove 2 family members
            familyList.Remove("Steve");
            familyList.Remove("Eileen");

            //Remove 2 relationships
            relationshipList.Remove("Father");
            relationshipList.Remove("Mother");

            //Add a new family member
            familyList.Insert(0,"Wilma");

            //Add a new relationship
            relationshipList.Insert(0,"Grandmother");

            i = 0;

            //Create a loop to go through each family name in the familyList
            foreach (string familyName in familyList)
            {
                //Tell the user the updated family members names and their relationships
                Console.WriteLine($"{familyName} is the {relationshipList[i]} of the family.");

                i++;
            }
        }
    }
}
