﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_StringObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Eileen Villahermosa
               SDI Section #01
               August 17, 2017
               String Objects
            */


            //Problem #1: Email Address Checker
            //Create a method that will accept a String as an argument
            //See if the String follows the pattern on an email address (aaa@bbb.ccc).

            //Prompt user for an email address
            Console.WriteLine("Hello! Please type in email address and press enter:");

            //Create String variable and capture user response
            string userEmail = Console.ReadLine();

            //Create string variable to hold the validation output
            string validate;

            //Function call and store the returned value
            checkEmail(userEmail, out validate);

            //Tell the user if email address is valid or invalid
            Console.WriteLine($"The email address of {userEmail} is {validate} email address.");

            /* Data Sets Test Results
             * Email -test@fullsail.com
                    * Results - The email address of test@fullsail.com is a valid email 
                      address.
             * Email -test@full@sail.com
                    * Results - The email address of test@full@sail.com is not a valid email 
                      address.
             * Email -test@full sail.com
                    * Results - The email address of test@full sail.com is not a valid email 
                      address.
             * Email -evarina077@full@sail.edu
                    * Results - The email address of evarina077@full@sail.edu is not a valid 
                      email address.
             * Email -evarina077@fullsail.edu
                    * Results - The email address of evarina077@fullsail.edu is a valid email 
                      address.
             * Email -evarina077@full sail.edu
                    * Results - The email address of evarina077@full sail.edu is not a valid 
                      email address.                      
            */


            //Problem #2: Separator Swap Out
            /* Method to accept three strings: 
             * first will be a list of items separated by a given string
             * second will be the separator used
             * third will be a new separator
            */

            //Prompt the user for a list of items
            Console.WriteLine("\r\nHello! Let's do a Separator Swap out!\r\nPlease type in a list of items separated with a symbol (e.g. circle,square,triangle,rectangle,hexagon) and press enter:");

            //Create a string to store the user's reponse
            //Capture the user's response
            string userInput = Console.ReadLine();

            //Prompt the user what symbol they use for storing purposes
            Console.WriteLine("\r\nPlease type in the symbol you just used for your list of items and press enter:");

            //Create a string to store the user's reponse
            //Capture the user's response
            string originalSeparator = Console.ReadLine();

            //Prompt the user what symbol they would like to swap out to
            Console.WriteLine("\r\nNow what symbol do you want to change the separator to? \r\nPlease type in the new symbol (e.g. :, /, -) and press enter:");

            //Create a string to store the user's reponse
            //Capture the user's response
            string newSeparator = Console.ReadLine();

            //Tell the user you will now change the original separator to the new one
            Console.WriteLine("\r\nThank you! We will now swap out the separator!");

            //Create string variable to hold the updated separator output
            string updatedSeparator;

            //Function call and store the returned value
            updateSeparator(userInput, originalSeparator, newSeparator, out updatedSeparator);

            //Tell the user the updated symbol swap out on their list of items
            Console.WriteLine($"\r\nThe original string of {userInput} with the new separator is {updatedSeparator}.");


            /* Data Test Results
             * List - 1,2,3,4,5     originalSeparator - ","     newSeparator - "-"
                    Results - The original string of 1,2,3,4,5 with the new separator is 
                    1-2-3-4-5.
             * List - red: blue: green: pink, originalSeparator - ":", newSeparator - ","
                    Results - The original string of red: blue: green: pink with the 
                    new separator is red, blue, green, pink.
             * List - Soccer; Baseball; Football; Volleyball; Basketball    orginalSeparator - ";"
               newSeparator - "/"   
                    Results - The original string of Soccer; Baseball; Football; Volleyball; 
                    Basketball with the new separator is Soccer/ Baseball/ Football/ Volleyball/ 
                    Basketball.
             */
        }



        //Method for Problem #2
        public static void updateSeparator(string ui, string os, string ns, out string us) 
        {
            //Create an array to hold substrings
            char[] separatorArray = ui.ToCharArray();

            //Create an array to hold substrings
            char[] orgSep = os.ToCharArray();

            //Create an array to hold substrings
            char[] newSep = ns.ToCharArray();

            for (int i=0; i<separatorArray.Length; i++)
            {
                //This will update the array from original to new
                if (separatorArray[i] == orgSep[0])
                {
                    separatorArray[i] = newSep[0];
                }
                
            }

            //Holds the updated new string
            us = new string(separatorArray);
        }


        //Method for Problem #1
        public static void checkEmail(string email, out string validate)
        {
            //Create a variable to store number of @ (at)
            int checkAt = 0;

            //Create a variable to store number of . (periods)
            int checkDot = 0;

            //Create a variable to store the place of @ (at)
            int checkFirstAt = 0;

            //Create a variable to store number of spaces
            int checkSpace = 0;

            //Convert string to a character array
            char[] checkString = email.ToCharArray();

            for(int i = 0; i < checkString.Length; i++)
            {
                //This will check for the character
                if (checkString[i] == '@')
                {
                    //Counts the number of @ and place in the array as an int
                    checkAt++;
                    checkFirstAt = i;
                }

                //This will check for the character
                if (checkString[i] == '.')
                {
                    //Sets the value to its place in the array
                    checkDot = i;
                }

                //This will check for the character
                if (checkString[i] == ' ')
                {
                    //Check for spaces
                    checkSpace++;
                }
            }
            
            //This will check if all conditions are met and return a string
            if(checkAt == 1 && checkFirstAt < checkDot && checkSpace == 0)
            {
                //This will run if email address input is valid
                validate = "a valid";
            }

            else
            {   
                //This will run if email address input is invalid
                validate = "not a valid";
            }

        }


    }
}
