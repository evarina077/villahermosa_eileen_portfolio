﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_RestaurantCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Eileen Villahermosa
            SDI Section #01
            August 04, 2017
            Tip Calculator Assignment
            */


            //Added descriptive text to greet user
            Console.WriteLine("Hello! Welcome to EV's Bistro!");

            //Added descriptive text to prompt user to enter the price of the first check
            Console.WriteLine("\r\nPlease type in the price of the first check (e.g. 7.77) and press enter:");

            //Created the first check variable to catch the returned string
            string firstCheck = Console.ReadLine();

            //Converted the returned string into a decimal, for calculation purposes
            decimal firstCheckNumber = decimal.Parse(firstCheck);

            //Added descriptive text to prompt user to enter the price of the second check
            Console.WriteLine("\r\nPlease type in the price of the second check (e.g. 7.77) and press enter:");

            //Created the second check variable to catch the returned string
            string secondCheck = Console.ReadLine();

            //Converted the returned string into a decimal, for calculation purposes
            decimal secondCheckNumber = decimal.Parse(secondCheck);

            //Added descriptive text to prompt user to enter the price of the second check
            Console.WriteLine("\r\nPlease enter the price of the third check (e.g. 7.77):");

            //Created the third check variable to catch the returned string
            string thirdCheck = Console.ReadLine();

            //Converted the returned string into a decimal, for calculation purposes
            decimal thirdCheckNumber = decimal.Parse(thirdCheck);

            //Added descriptive text to prompt user to enter the tip amount based on their good services
            Console.WriteLine("\r\nPlease provide a tip % (e.g. 15) based on the great services received:");

            //Created the tip variable to catch the returned string
            string tip = Console.ReadLine();

            //Converted the returned string into a decimal, for calculation purposes
            decimal tipNumber = decimal.Parse(tip);

            //Used to get the subtotal of the three checks
            decimal subTotal = firstCheckNumber + secondCheckNumber + thirdCheckNumber;

            //Added descriptive text to provide user the subtotal
            Console.WriteLine("\r\nThe subtotal is: \r\n{0,1:C2}", subTotal);

            //Used to change the tip for proper math operation
            decimal tipValue = tipNumber / 100;

            //Used to to get the total of the tip
            decimal tipTotal = subTotal * tipValue;
            Console.WriteLine("\r\nThe total tip amount is: \r\n{0,1:C2}", tipTotal);

            //Used to get the grand total which indluded subtotal and tip total
            decimal grandTotal = subTotal + tipTotal;

            //Added descriptive text to provide user the grand total
            Console.WriteLine("\r\nThe grand total including tip is: \r\n{0,1:C2}", grandTotal);

            //Used to get the split cost evenly between the 3 guests
            decimal splitCost = grandTotal / 3;

            //Added descriptive text to provide the user the cost per guest
            Console.WriteLine("\r\nThe cost per person is \r\n{0,1:C2}", splitCost);
        }
    }
}
