﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_MadLibs
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Eileen Villahermosa
            SDI Section #01
            August 04, 2017
            MadLibs Style Story Creator Assignment
            */


            //Added descriptive text to start the Mad-Lib exercise with user and to prompt the user for his/her name
            Console.WriteLine("Let's get some Mad-Libs in!\r\nPlease type in your name and press enter:");
            
            //Created the name variable to catch the returned string
            string name = Console.ReadLine();

            //Added descriptive text to prompt the user for the first number >1
            Console.WriteLine("\r\nPlease type in a number >1 (e.g. 7) and press enter:");

            //Created the first number variable to catch the returned string
            string number1 = Console.ReadLine();

            //Converted the returned string into a double
            double number1Conv = double.Parse(number1);

            //Added descriptive text to prompt the user for a color
            Console.WriteLine("\r\nPlease type in a color and press enter:");

            //Created the color variable to catch the returned string
            string color = Console.ReadLine();

            //Added descriptive text to prompt the user for an animal
            Console.WriteLine("\r\nPlease type in an animal:");

            //Created the animal variable to catch the returned string
            string animal = Console.ReadLine();

            //Added descriptive text to prompt the user for a personality adjective (e.g. jolly, scary, etc.)
            Console.WriteLine("\r\nPlease type in a personality adjective (e.g. jolly, scary, etc.) and press enter:");

            //Created the adjective variable to catch the returned string
            string adjective = Console.ReadLine();

            //Added descriptive text to prompt the user for a name of someone they like/love
            Console.WriteLine("\r\nPlease type in a name of someone you like/love:");

            //Created the nameLike variable to catch the returned string
            string nameLike = Console.ReadLine();

            //Added descriptive text to prompt the user for the second number >1 
            Console.WriteLine("\r\nPlease type in another number >1 (e.g. 7) and press enter:");

            //Created the second number variable to catch the returned string
            string number2 = Console.ReadLine();

            //Converted the returned string into a double
            double number2Conv = double.Parse(number2);

            //Added descriptive text to prompt the user for the third number >1 
            Console.WriteLine("\r\nPlease type in another number >1 (e.g. 7) and press enter:");

            //Created the third number variable to catch the returned string
            string number3 = Console.ReadLine();

            //Converted the returned string into a double
            double number3Conv = double.Parse(number3);

            //Added descriptive text to prompt the user for the first noun (thing)
            Console.WriteLine("\r\nPlease type in a noun (thing) and press enter:");

            //Created the first noun variable to catch the returned string
            string noun1 = Console.ReadLine();

            //Added descriptive text to prompt the user for the second noun (thing)
            Console.WriteLine("\r\nPlease type in another noun (thing) and press enter:");

            //Created the second noun variable to catch the returned string
            string noun2 = Console.ReadLine();

            //Added descriptive text to prompt the user for a game
            Console.WriteLine("\r\nPlease type in a game:");

            //Created the game variable to catch the returned string
            string game = Console.ReadLine();

            //Created an array to store the set of nouns
            string[] nounSet = new string[] { noun1, noun2 };

            //Created an array to store the set of numbers
            double[] numberSet = new double[] { number1Conv, number2Conv, number3Conv };

            //Added descriptive text to create the mad-lib story that includes all of the values entered from the user 
            Console.WriteLine("\r\n"+name + " is running " + numberSet[0] + " minutes late for a date and rushes to park his/her " + color + " car. " + name + " runs towards the restaurant and bumps into a/an " + animal + ". " + name+" becomes very " + adjective + " and apologizes. " +name+" continues into the restaurant and finally sits down at a table with his/her date. His/her date's name is "+nameLike+". "+"They order " + numberSet[1] + " meals to share. " + "They finish all their meals within " + numberSet[2] + " minutes. " + nameLike + " gives "+name+" a present. " + name + " opens the present and to his/her surprise it is a/an " + nounSet[0] + ". " + name + " is grateful and hands "+nameLike+" a/an " + nounSet[1] + " to show gratitude. They end their date playing "+game+".");

        }
    }
}
