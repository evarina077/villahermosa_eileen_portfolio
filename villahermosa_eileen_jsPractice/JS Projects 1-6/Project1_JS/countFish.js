//declare and define the fish color arrays
let colors = ["red", "blue", "green", "yellow", "blue", "green", "blue", "blue", "red", "green"];
var userChoice;
var option = true;
var counter = 0;
var fish;

alert("Welcome to the Fish Store!")

userChoice = prompt("Please type [1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow and press OK!");

while (option)
{
  if (userChoice == 1)
  {
    for(fish of colors)
    {
      if(fish == "red")
          counter++;
    }

    alert("There are " + counter + " in the tank with the color red.");
    break;
  }

  else if (userChoice == 2)
  {
    for(fish of colors)
    {
      if(fish == "blue")
          counter++;
    }

    alert("There are " + counter + " in the tank with the color blue.");
    break;
  }

  else if (userChoice == 3)
  {
    for(fish of colors)
    {
      if(fish == "green")
          counter++;
    }

    alert("There are " + counter + " in the tank with the color green.");
    break;
  }

  else if (userChoice == 4)
  {
    for(fish of colors)
    {
      if(fish == "yellow")
          counter++;
    }

    alert("There are " + counter + " in the tank with the color yellow.");
    break;
  }

  else
  {
    userChoice = prompt("Sorry please type in a number between 1-4 ([1] for Red, [2] for Blue, [3] for Green, and [4] for Yellow) and press OK!");
  }
}
