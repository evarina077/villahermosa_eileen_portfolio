﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Villahermosa_Eileen_Conditionals
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Eileen Villahermosa
            SDI Section #01
            August 10, 2017
            Conditionals
            */



            //Problem #1: Temperature Converter
            //Convert from degrees Celsius to Fahrenheit
            //or from degrees Fahrenheit to Celsius

            //Greet user and prompt for name
            Console.WriteLine("Welcome to the Temperature Converter! \r\nPlease type in your name and press enter:");

            //Store results in a string variable
            string userName = Console.ReadLine();

            //Create a conditional to test if the user inputted anything or left null/whitespace
            if (string.IsNullOrWhiteSpace(userName))
            {
                //This will run if user left blank, prompt user to not leave blank and enter name
                Console.WriteLine("Please do not leave this blank. Type in your name and press enter:");

                //Re-capture the response
                userName = Console.ReadLine();
            }

            //Prompt user for a number temperature
            Console.WriteLine($"\r\nHello, {userName}! \r\nPlease type in a number temperature (e.g. 85) and press enter:");

            //Store result in a string variable
            string numTemp = Console.ReadLine();

            //Define and declare value to hold converted value
            int userNumTemp;

            //Create a conditional to test validation
            if (!(int.TryParse(numTemp, out userNumTemp)))
            {
                //This will run if it could not convert the number
                //Prompt the user that they need to enter a number like to example provided
                Console.WriteLine("\r\nSorry you did not type in the number in the correct format.\r\nPlease re-type the number (e.g. 1-100) and press enter:");

                //Re-capture the response
                numTemp = Console.ReadLine();

                //Re-convert numTemp into an integer
                int.TryParse(numTemp, out userNumTemp);
            }

            //Prompt user to enter Celsius (C) or Fahrenheit (F) for the temperature number they entered.
            Console.WriteLine($"\r\nWould {userNumTemp} be in degrees Celsius (C) or Fahrenheit (F)?\r\nPlease type in C or F and press enter:");

            //Created the boolean to test 
            bool degree = true;

            //Created the int for conversion from celsius to fahrenheit
            int celToFah;

            //Creted the int for conversion from fahrenheit to celsius
            int fahToCel;

            //Store result in a string variable
            string userInput = Console.ReadLine();

            //Use to run at least once and THEN the do the check
            do
                //User is allowed to enter either lower case or upper case
                if (userInput == "C" || userInput == "c")
                {
                    //The first check if user enter celsius

                    degree = false;

                    //Formula to convert from celsius to fahrenheit
                    celToFah = ((userNumTemp * 9) / 5) + 32;

                    //Text output to let user know the conversion result from celsius to fahrenheit
                    Console.WriteLine($"{userNumTemp} Degrees Celsius converts to {celToFah} Degrees Fahrenheit! \r\nThanks for using the Temperature Converter!");
                }

                //User is allowed to enter either lower case or upper case
                else if (userInput == "F" || userInput == "f")
                {
                    //The second check if user enter fahrenheit

                    degree = false;

                    //Formula to convert from fahrenheit to celsius
                    fahToCel = ((userNumTemp - 32) / 9 * 5);

                    //Text output to let user know the converson result from fahrenheit to celsius
                    Console.WriteLine($"{userNumTemp} Degrees Fahrenheit converts to {fahToCel} Degrees Celsius! \r\nThanks for using the Temperature Converter!");
                }

                else
                {
                    degree = false;
                    //This will run if user does not enter c/C or f/F
                    //Prompt user that they entered the temperature degree incorrect and to re-enter with C or F
                    Console.WriteLine("Sorry you did not type in the Degrees Fahrenreit or Celsius correctly. \r\nPlease type in C or F and press enter:");

                    //Re-capture the response
                    userInput = Console.ReadLine();

                }

            while (degree == true);

            /*
            Data Sets to Test:
            - 32F is 0C
            - 100C is 212F
            - 50 c, Does a lower c make a difference? It won't matter since I've included both lower case and upper case as possiblities
            - Test One Of Your Own. Tested a few other numbers: 92F is 30C, 20c to 68F, 107f to 40C, and 67C to 152F
            */



            //Problem #2: Last Chance for Gas
            //Determine if driver has enough gas to make acress the desert
            //There is a last gas station they will pass for the next 200 miles
            //Need to determine if they need to stop now for gas or not

            //Greet user, prompt for name and ask how many gallons their car tan holds
            Console.WriteLine("\r\nLast chance for Gas, for the next 200 miles! Let's determine if you should fuel up now or later!\r\nPlease type in how many gallons you car tank holds and press enter:");

            //Store result in a string variable
            string numGallons = Console.ReadLine();

            //Declare a variable to to hold the converted string value
            double numTankGallons;

            //While loop test validation for number variable
            while (!double.TryParse(numGallons, out numTankGallons))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format. \r\nPlease type in how many gallons you car tank holds (e.g. 8) and press enter:");

                //Re-capture the response
                numGallons = Console.ReadLine();
            }

            //Prompt user to enter how full is their gas tank (%)?
            Console.WriteLine("Please type in how full your gas tank (%) is and press enter:");

            //Store result in a string variable
            string gasTank = Console.ReadLine();

            //Declare a variable to hold the converted string value
            double numGasTank;

            //While loop test validation for number
            while (!double.TryParse(gasTank, out numGasTank))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format. \r\nPlease type in how full your gas tank (e.g. 50) is and press enter:");

                //Re-capture the response
                gasTank = Console.ReadLine();
            }

            //Prompt user to enter how many miles per gallon does their car go?
            Console.WriteLine("Please type in how many miles per gallon your car goes and press enter:");

            //Store result in a string variable
            string milesPerGallon = Console.ReadLine();

            //Declare a variable to hold the converted string value
            double numMilesPerGallon;

            //While loop test validation for number
            while (!double.TryParse(milesPerGallon, out numMilesPerGallon))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type in how many miles per gallon your car goes (e.g. 25) and press enter:");

                //Re-capture the response
                milesPerGallon = Console.ReadLine();

            }
            //Show user the information they entered
            Console.WriteLine($"\r\nYour car holds {numTankGallons} gallons of gas. \r\nYour gas tank is {numGasTank}% full. \r\nYour car can go {numMilesPerGallon} mile per gallon.");
            Console.WriteLine("Based on this inoformation:\r\n");

            //Calculations to use
            double gasStation = 200;
            double totalMiles = (numGasTank * numTankGallons / 100)*numMilesPerGallon;

            //Statements to determine whether driver needs to get gas now or later
            if (totalMiles >= gasStation)
            {
                Console.WriteLine("Yes, you can drive {0} more miles and make it without stopping for gas!", totalMiles);
            }

            else
                Console.WriteLine("Sorry, you only have {0} miles you can drive, better get gas now while you can!", totalMiles);

            /*
            Data Sets to Test:
            -Gallons - 20, Gas tank = 50% full, MPG - 25
                -Result - "Yes, you can drive 250 more miles and you can make it without stopping for gas!"
            -Gallons - 12, Gas tank = 60% full, MPG - 20
                -Result - "You only have 144 miles you can drive, better get gas now while you can!"
            -Gallons - 10, Gas tank = 40% full, MPG - 25
                -Result - "You ony have 100 miles you can drive, better get gas now while you can!"
         


            //Problem #3: Grade Letter Calculator
            //Student earns a number grade at the conclusion of a Full Sail course
            //Determine the letter grade
            /*Full Sail's Grade Scale:
                A is 90-100
                B is 80-89
                C is 73-79
                D is 70-72
                F is 0-69
            */

            //Greet user and prompt for name
            Console.WriteLine("\r\nWelcome to the Grade Letter Calculator! \r\nPlease type in your name and press enter.");

            //Store result in a string variable
            string studentName = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(studentName))
            {
                //This will run if user left blank, prompt user to not leave blank and enter name
                Console.WriteLine("Please do not leave this blank. Type in your name and press enter:");

                //Re-capture the response
                studentName = Console.ReadLine();
            }

            //Prompt user for their course grade in %
            Console.WriteLine($"\r\nHello, {studentName}! Please type in your course grade (%) and press enter:");

            //Store result in a string variable
            string courseGrade = Console.ReadLine();

            //Declare a variable to hold the converted string value
            double numCourseGrade;



            //While loop test validation for number
            while (!double.TryParse(courseGrade, out numCourseGrade))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type in your course grade (e.g. 90) and press enter:");

                courseGrade = Console.ReadLine();
            }

            //Define and declare bool true variable
            bool option = true;

            //Define and declare a string variable to hold the grade letter output
            string gradeLetter;

            while (option)
            {
                //This will run if user enters a number other then the grade range of 0-100
                if (numCourseGrade < 0 || numCourseGrade > 100)
                {
                    Console.WriteLine("Sorry you must type in a number between 0-100. Please retype and press enter:");

                    courseGrade = Console.ReadLine();
                }

                    if (!double.TryParse(courseGrade, out numCourseGrade))
                    {
                        //This will run if user type something other than a number
                        //Prompt user to enter a number in the correct format
                        Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type in your course grade (e.g. 90) and press enter:");

                        courseGrade = Console.ReadLine();
                    }
               

                else if (numCourseGrade >= 90)
                {
                    //This will run if user enters grade number  between 90-100
                    gradeLetter = "A";

                    //This will make the loop stop
                    option = false;

                    //This will let user know their grade letter result
                    Console.WriteLine($"You have a {numCourseGrade}%, which means you have earned a(n) {gradeLetter} in the class!");
                }
                else if (numCourseGrade >= 80)
                {
                    //This will run if user enters grade number  between 80-89
                    gradeLetter = "B";

                    //This will make the loop stop
                    option = false;

                    //This will let user know their grade letter result
                    Console.WriteLine($"You have a {numCourseGrade}%, which means you have earned a(n) {gradeLetter} in the class!");
                }
                else if (numCourseGrade >= 73)
                {
                    //This will run if user enters grade number  between 73-79
                    gradeLetter = "C";

                    //This will make the loop stop
                    option = false;

                    //This will let user know their grade letter result
                    Console.WriteLine($"You have a {numCourseGrade}%, which means you have earned a(n) {gradeLetter} in the class!");
                }
                else if (numCourseGrade >= 70)
                {
                    //This will run if user enters grade number  between 70-72
                    gradeLetter = "D";

                    //This will make the loop stop
                    option = false;

                    //This will let user know their grade letter result
                    Console.WriteLine($"You have a {numCourseGrade}%, which means you have earned a(n) {gradeLetter} in the class!");
                }
                else 
                {
                    //This will run if user enters grade number  between 0-69
                    gradeLetter = "F";

                    //This will make the loop stop
                    option = false;

                    //This will let user know their grade letter result
                    Console.WriteLine($"You have a {numCourseGrade}%, which means you have earned a(n) {gradeLetter} in the class!");

                }
            }


            /*
            Data Sets to Test
            -Grade - 92%
                 -Result - “You have a 92%, which means you have earned a(n) A in the class!”
            -Grade - 80%
                 -Result - “You have a 80%, which means you have earned a(n) B in the class!”
            -Grade - 67%
                 -Result - “You have a 80%, which means you have earned a(n) F in the class!”
            -Grade - 72%
                 -Result - “You have a 80%, which means you have earned a(n) D in the class!”
           -Grade - 75%
                 -Result - “You have a 80%, which means you have earned a(n) C in the class!”]
           -Grade - 120%
                 -Result - “Sorry you must type in a number between 0-100. Please retype and press enter:
                            67
                            You have a 67%, which means you have earned a(n) F in the class!”
           */



            //Problem #4: Discount Double Check
            //Spend $100 or more, get a 10% discount on total purchase
            //Spend between $50 and $100, get a 5% discount on total purchase
            //Spent less than $50, get no discount

            //Greet user and prompt for name
            Console.WriteLine("\r\nWelcome to EV's online store!\r\nPlease type in your name and press enter:");

            //Store result in a string variable
            string shopperName = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(shopperName))
            {
                //This will run if user left blank, prompt user to not leave blank and enter name
                Console.WriteLine("Please do not leave this blank. Type in your name and press enter:");

                //Re-capture the respones
                shopperName = Console.ReadLine();
            }

            //Prompt user the amount of their firt item
            Console.WriteLine($"\r\nHello, {shopperName}! Please type in the cost of your first item (19.99) and press enter:");

            //Store result in a string variable
            string firstItem = Console.ReadLine();

            //Declare a variable to hold the converted string value
            decimal firstItemPrice;

            //While loop test validation for number
            while (!decimal.TryParse(firstItem, out firstItemPrice))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type in the cost of your first item (19.99) and press enter:");

                //Re-capture the response
                firstItem = Console.ReadLine();

            }

            //Prompt user the amount of their second item
            Console.WriteLine("\r\nNow, please type in the cost of your second item (19.99) and press enter:");

            //Store result in a string variable
            string secondItem = Console.ReadLine();

            //Declare a variable to hold the converted string value
            decimal secondItemPrice;

            //While loop test validation for number
            while (!decimal.TryParse(secondItem, out secondItemPrice))
            {
                //This will run if user type something other than a number
                //Prompt user to enter a number in the correct format
                Console.WriteLine("Sorry you did not type the number in the correct format.\r\nPlease type in the cost of your second item (19.99) and press enter:");


                //Re-capture the response
                secondItem = Console.ReadLine();
            }

            //Declare and define the discount percentage
            decimal discount10 = 10;
            decimal discount5 = 5;

            //Formula for Discounts
            decimal discountOne = (firstItemPrice + secondItemPrice) * (discount10/100);
            decimal discountTwo = (firstItemPrice + secondItemPrice) * (discount5/100);

            //Formula for the totals
            decimal total = firstItemPrice + secondItemPrice;
            decimal totalDiscountOne = total - discountOne;
            decimal totalDiscountTwo = total - discountTwo;

            //Statements to determine if a discount will apply
            if (total >= 100)
            {
                Console.WriteLine("\r\nYour total purchase is {0,1:C2}, which includes your {1}% discount.",totalDiscountOne, discount10);
            }
            else if (total >= 50)
            {
                Console.WriteLine("\r\nYour total purchase is {0,1:C2}, which inlcudes your {1}% discount.", totalDiscountTwo, discount5);
            }
            else
                Console.WriteLine($"\r\nYour total purchase is {total}.");

            /*
            Data Sets to Test:
            -First Item Cost - $45.00, Second Item Cost - $75.00, Total - $108.45
                -Results - "Your total purchase is $108.45, which includes your 10% discount."
            -First Item Cost - $30.00, Second Item Cost - $25.00, Total - $52.25
                -Results - "Your total purchase is $52.25, which includes your 5% discount."
            -First Item Cost - $5.75, Second Item Cost - $12.50, Total - $18.25
                -Results - "Your total purchase is $18.25."
            -First Item Cost - $170.00, Second Item Cost - $220.00, Total - $351.00
                -Results - "Your total purchase is $351.00, which includes your 10% discount."
             */
        }
    }
}
