let familyList = ["Steve","Eileen","Amaya","Tyler"];
let relationshipList = ["Father","Mother","Daughter","Son"];
var i = 0;
var familyName;

alert("Hello! Let's find out who's who in the Phan Family! Press OK!")

arrayFamily(familyList, relationshipList);

function arrayFamily()
{
  for (familyName of familyList)
  {
    alert(familyName + " is the " + relationshipList[i] + " of the family.")
    i++;
  }
}

alert("Now let's change the family members a bit! Press OK!");

familyList.splice(0, 2, "Wilma");
relationshipList.splice(0, 2, "Grandmother");

i = 0;

newArrayFamily(familyList, relationshipList);

function newArrayFamily()
{
  for (familyName of familyList)
  {
    alert(familyName + " is the " + relationshipList[i] + " of the family.")
    i++;
  }
}
