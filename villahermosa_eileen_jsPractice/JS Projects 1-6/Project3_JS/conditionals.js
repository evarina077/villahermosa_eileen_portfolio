//Program #1: Temperature Converter
//Convert from degrees Celsius to Fahrenheit
//or from degrees Fahrenheit to Celsius

var userName;
var numTemp;
var celToFah;
var fahToCel;
var userInput;

alert("Welcome to the Temperature Converter!");
userName = prompt("Please type in your name.");

while (userName === "" || userName === " ")
{
  alert("Please don't enter blank or white space!");
  userName = prompt("Please type in your name.");
}

numTemp = prompt("Hello, " + userName + "! Please type in a number temperature (e.g. 85).");

while (!(parseInt(numTemp)))
{
  alert("Sorry input not a valid number!");
  numTemp = prompt("Please type in a number temperature (e.g. 85).");
}

userInput = prompt("Would " + numTemp + " be in degrees Celsius (C) or Fahrenheit (F)? Please type in C or F.");

do
{
  if (userInput == "C" || userInput == "c")
  {

     celToFah = ((numTemp * 9) /5) + 32;
     alert(numTemp + " Degrees Celsius converts to " + celToFah + " Degrees Fahrenheit!");
     break;

  }
  else if (userInput == "F" || userInput == "f")
  {

      fahToCel = ((numTemp - 32) / 9 * 5);
      alert(numTemp + " Degrees Fahrenheit converts to " + fahToCel + " Degress Celsius!")
      break;
  }
  else
  {
    alert("Sorry invalid input of Degrees!");
    userInput = prompt("Please type in C (Celsius) or F (Fahrenheit)!")
  }

} while (true);

alert("Thanks for using the Temperature Converter program! Lets move on to the next program!");

//Program #2: Last Chance for Gas
//Determine if driver has enough gas to make acress the desert
//There is a last gas station they will pass for the next 200 miles
//Need to determine if they need to stop now for gas or not

var tankGallons;
var gasTank;
var milesPerGallon;
var gasStation;
var totalMiles;
var gasDetails;
var gasTankNumber;
var tankGallonsNumber;
var milesPerGallonNumber;

alert("Welcome to Last chance for Gas, for the next 200 miles! Let's determine if you should fuel up now or later!");

tankGallons = prompt("Please type in how many gallons you car tank holds.");

tankGallonsNumber = ValidateTankNumber(tankGallons);

gasTank = prompt("Please type in how full your gas tank (%) is.");

gasTankNumber = ValidateTankNumber(gasTank);

milesPerGallon = prompt("Please type in how many miles per gallon your car goes.");

milesPerGallonNumber = ValidateTankNumber(milesPerGallon);

function ValidateTankNumber(input)
{
  while (!(parseInt(input)))
  {
    alert("Sorry input not a valid number!");
    input = prompt("Please type in a number (e.g. 20).");
  }
  return input;
}

alert("You car holds " + tankGallonsNumber + " gallons of gas. You gas tank is " + gasTankNumber + "% full. You car can go " + milesPerGallonNumber + " miles per gallon.");

alert("Based on this information......");

gasStation = 200;
totalMiles = (gasTankNumber * tankGallonsNumber/100) * milesPerGallonNumber;

if (totalMiles >= gasStation)
{
  alert("Yes, you can drive " + totalMiles + " more miles and make it without stopping for gas!");
}
else
{
  alert("Sorry, you only have " + totalMiles + " miles you can drive, better get gas now while you can!");
}

alert("Thanks for using the Last Chance for Gas program! Lets move on to the next program!");

//Program #3: Grade Letter Calculator
//Student earns a number grade at the conclusion of a Full Sail course
//Determine the letter grade
/*Full Sail's Grade Scale:
    A is 90-100
    B is 80-89
    C is 73-79
    D is 70-72
    F is 0-69
*/

var studentName;
var courseGrade;
var option = true;
var gradeLetter;

alert("Welcome to the Grade Letter Calculator!");

studentName = prompt("Please type in your name.");

while (studentName === "" || studentName === " ")
{
  alert("Please don't enter blank or white space!");
  studentName = prompt("Please type in your name.");
}

courseGrade = prompt("Hello, " + studentName + "! Please type in your course grade (%).");

while (!(parseInt(courseGrade)))
{
  alert("Sorry input not a valid number!");
  courseGrade = prompt("Please type in your course grade (e.g. 90).");
}

while (option)
{
  if (courseGrade < 0 || courseGrade > 100)
  {
    alert("Sorry you must type in a number between 0-100.");
    courseGrade = prompt("Please type in your course grade (e.g. 90).");
  }
  else if (courseGrade >= 90)
  {
    gradeLetter = "A";
    alert("You have a " + courseGrade + "%, which means you have earned a(n) " + gradeLetter + " in the class!");
    break;
  }
  else if (courseGrade >= 80)
  {
    gradeLetter = "B";
    alert("You have a " + courseGrade + "%, which means you have earned a(n) " + gradeLetter + " in the class!");
    break;
  }
  else if (courseGrade >= 73)
  {
    gradeLetter = "C";
    alert("You have a " + courseGrade + "%, which means you have earned a(n) " + gradeLetter + " in the class!");
    break;
  }
  else if (courseGrade >= 70)
  {
    gradeLetter = "D";
    alert("You have a " + courseGrade + "%, which means you have earned a(n) " + gradeLetter + " in the class!");
    break;
  }
  else
  {
    gradeLetter = "F";
    alert("You have a " + courseGrade + "%, which means you have earned a(n) " + gradeLetter + " in the class!");
    break;
  }
}

alert("Thanks for using the Grade Letter Calculator! Let's move on to the last program!")

//Program #4: Discount Double Check
//Spend $100 or more, get a 10% discount on total purchase
//Spend between $50 and $100, get a 5% discount on total purchase
//Spent less than $50, get no discount

var shopperName;
var firstItemPrice;
var secondItemPrice;
var discount10 = 10;
var discount5 = 5;
var discountOne;
var discountTwo;
var totalDiscountOne;
var totalDiscountTwo;
var totalNoDisount;
var totalPrice;

alert("Welcome to EV's online store!");

shopperName = prompt("Please type in your name.");

while (shopperName === "" || shopperName === " ")
{
  alert("Please don't enter blank or white space!");
  shopperName = prompt("Please type in your name.");
}

 firstItemPrice = prompt("Hello, " + shopperName +"! Please type in the cost of your first item (19.99).");

 ValidateNumber(firstItemPrice);

 secondItemPrice = prompt("Now, please type in the cost of your second item (19.99).");

 ValidateNumber(secondItemPrice);

 function ValidateNumber(price)
 {
   while (!(parseFloat(price)))
   {
     alert("Sorry input not a valid number!");
     price = prompt("Please type in the cost of your item (e.g. 19.99).");
   }
 }

 Total(firstItemPrice, secondItemPrice);

 function Total (priceOne, priceTwo)
 {
   numberOne = parseFloat(priceOne);
   numberTwo = parseFloat(priceTwo);
   totalPrice = numberOne + numberTwo;
 }

 discountOne = (totalPrice) * (discount10/100);
 discountTwo = (totalPrice) * (discount5/100);

 totalDiscountOne = totalPrice - discountOne;
 totalDiscountTwo = totalPrice - discountTwo;
 totalNoDisount = totalPrice;

 if (totalPrice >= 100)
 {
     alert("Your total purchase is " + totalDiscountOne.toFixed(2) + " which includes your " + discount10 + "% discount.");
 }
 else if (totalPrice >= 50)
 {
     alert("Your total purchase is " + totalDiscountTwo.toFixed(2) + " which includes your " + discount5 + "% discount.");
 }
 else
 {
   alert("Your total purchase is " + totalPrice.toFixed(2));
 }
