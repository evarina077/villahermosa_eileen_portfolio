//Problem #1: Email Address Checker
//Create a method that will accept a String as an argument
//See if the String follows the pattern on an email address (aaa@bbb.ccc).

var checkAt = 0;
var checkDot = 0;
var checkFirstAt = 0;
var checkSpace = 0;
var checkString;
var userEmail;
var validate;

userEmail = prompt("Hello! Please type in an email address");

checkString = userEmail.split("");

ValidateEmail(checkString);

function ValidateEmail ()
{
  for (var i = 0; i < checkString.length; i++)
  {
    if (checkString[i] == '@')
                 {
                     //Counts the number of @ and place in the array as an int
                     checkAt++;
                     checkFirstAt = i;
                 }

                 //This will check for the character
                 if (checkString[i] == '.')
                 {
                     //Sets the value to its place in the array
                     checkDot = i;
                 }

                 //This will check for the character
                 if (checkString[i] == ' ')
                 {
                     //Check for spaces
                     checkSpace++;
                 }
             }

             //This will check if all conditions are met and return a string
             if(checkAt == 1 && checkFirstAt < checkDot && checkSpace == 0)
             {
                 //This will run if email address input is valid
                 validate = "a valid";
             }

             else
             {
                 //This will run if email address input is invalid
                 validate = "not a valid";
             }
}

alert("The email address of " + userEmail + " is " + validate + " email address.");

//Problem #2: Separator Swap Out
/* Method to accept three strings:
 * first will be a list of items separated by a given string
 * second will be the separator used
 * third will be a new separator
*/

var userInput;
var originalSeparator;
var newSeparator;
var updatedSeparator;
var separatorArray;
var orgSep;
var newSep;
var us;

alert("Hello! Let's do a Separator Swap out!");

userInput = prompt("Please type in a list of items separated with a symbol (e.g. circle,square,triangle,rectangle,hexagon).");

originalSeparator = prompt("Please type in the symbol you just used for your list of items.");

newSeparator = prompt("Now what symbol do you want to change the separator to? Please type in the new symbol (e.g. :, /, -).");

UpdateSeparator(userInput, originalSeparator, newSeparator);

function UpdateSeparator (ui, os, ns, us)
{

  if (orgSep = ui.split(os))
  {
    newSep = orgSep.join(ns);
  }

  updatedSeparator = newSep;
}

alert("The original string of " + userInput + " with the new separator is " + updatedSeparator);
