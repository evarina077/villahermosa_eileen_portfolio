var firstCheck;
var secondCheck;
var thirdCheck;
var tip;
var firstInput;
var secondInput;
var thirdInput;
var tipInput;
var subTotal;
var tipValue;
var tipTotal;
var grandTotal;
var splitCost;

alert("Hello! Welcome to EV's Bistro!");

firstInput = prompt ("Please type in the price of the first check (e.g. 7.77).");

firstCheck = ValidateNumber(firstInput);

secondInput = prompt ("Please type in the price of the second check (e.g. 7.77).");

secondCheck = ValidateNumber(secondInput);

thirdInput = prompt ("Please type in the price of the third check (e.g. 7.77).");

thirdCheck = ValidateNumber(thirdInput);

tipInput = prompt ("Please provide a tip % (e.g. 15) based on the great services received.");

tip = ValidateNumber(tipInput);

tipValue = tip/100;

subTotal = SubTotal(firstCheck, secondCheck, thirdCheck);

alert(" The subtotal is: $" + subTotal.toFixed(2));

tipTotal = subTotal * tipValue;

alert("The total tip amount is: $" + tipTotal.toFixed(2));

grandTotal = subTotal + tipTotal;

alert("The grand total including tip is: $" + grandTotal.toFixed(2));

splitCost = grandTotal/3;

alert("The cost per person (3) is: $" + splitCost.toFixed(2));

function ValidateNumber(price)
{
  while (!(parseFloat(price)))
  {
    alert("Sorry input not a valid number!");
    price = prompt("Please type in the price (e.g. 7.77).");
  }

  return price;
}

function SubTotal(first, second, third)
{
  var firstOne = parseFloat(first);
  var secondOne = parseFloat(second);
  var thirdOne = parseFloat(third);

  return firstOne + secondOne + thirdOne;
}
