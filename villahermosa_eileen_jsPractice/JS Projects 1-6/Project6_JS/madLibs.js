var nameInput;
var name;
var number1Input;
var number1;
var colorInput;
var color;
var animalInput;
var animal;
var adjectiveInput;
var adjective;
var nameLikeInput;
var nameLike;
var number2Input;
var number2;
var number3Input;
var number3;
var noun1Input;
var noun1;
var noun2Input;
var noun2;
var gameInput
var game;
var nounSet;
var numberSet;

alert("Let's get some Mad-Libs in!");

nameInput = prompt("Please type in your name.");

name = ValidateText(nameInput);

number1Input = prompt("Please type in a number > 1 (e.g. 7).");

number1 = ValidateNumber(number1Input);

colorInput = prompt("Please type in a color.");

color = ValidateText(colorInput);

animalInput = prompt("Please type in an animal.");

animal = ValidateText(animalInput);

adjectiveInput = prompt("Please type in a personality adjective (e.g. jolly, scary, etc.)");

adjective = ValidateText(adjectiveInput);

nameLikeInput = prompt("Please type in a name of someone you like/love.");

nameLike = ValidateText(nameLikeInput);

number2Input = prompt("Please type in another number > 1 (e.g. 7).");

number2 = ValidateText(number2Input);

number3Input = prompt("Please type in another number > 1 (e.g. 7).");

number3 = ValidateNumber(number3Input);

noun1Input = prompt("Please type in a noun (thing).");

noun1 = ValidateText(noun1Input);

noun2Input = prompt("Please type in another noun (thing).");

noun2 = ValidateText(noun2Input);

nounSet = new Array(noun1, noun2);

numberSet = new Array(number1, number2, number3);

gameInput = prompt("Please type in a game.");

game = ValidateText(gameInput);

alert(name + " is running " + numberSet[0] + " minutes late for a date and rushes to park his/her " + color + " car. " + name + " runs towards the restaurant and bumps into a/an " + animal + ". " + name +" becomes very " + adjective + " and apologizes. " + name + " continues into the restaurant and finally sits down at a table with his/her date. His/her date's name is "+ nameLike + ". They order " + numberSet[1] + " meals to share. They finish all their meals within " + numberSet[2] + " minutes. " + nameLike + " gives " + name + " a present. " + name + " opens the present and to his/her surprise it is a/an " + nounSet[0] + ". " + name + " is grateful and hands " + nameLike + " a/an " + nounSet[1] + " to show gratitude. They end their date playing " + game + ".");

function ValidateText(text)
{
  while (text === "" || text === " ")
  {
    alert("Please don't enter blank or white space!");
    text = prompt("Please type again.");
  }
  return text;
}

function ValidateNumber(number)
{
  while (!(parseInt(number)))
  {
    alert("Sorry input not a valid number!");
    number = prompt("Please type again.");
  }
  return number;
}
