//The parent class
class Athletes {
  constructor(name, position) {
    //Encapsulation was used to hold private variables
    this._name = name;
    this._position = position;
  }
  //function to store the athletes
  getAthlete() {
    return `${this._name}'s position in the volleyball team is the ${this._position}.`;
    //console.log(`${this._name}'s position in the volleyball team is ${this._position}.`);
  }
}

//Abstractions was used by creating two abstract class that was inherited by the parent class
//Inheritance and Polymorphism used by sharing the same behavior of getAthlete() from the parent class into the child class
class volleyballCoaches extends Athletes
{
  constructor(name, position)
  {
    super(name, position);
  }
}

class volleyballPlayers extends Athletes
{
  constructor(name, position)
  {
    super(name, position);
  }
}

//array to store all atheletes
var teamRoster = [];

//instantions from the child class
//push method to array
var coach1 = new volleyballCoaches("Allison", "Head Coach");
teamRoster.push(coach1.getAthlete());

var coach2 = new volleyballCoaches("Alyvia", "Assistant Coach");
teamRoster.push(coach2.getAthlete());

var player1 = new volleyballPlayers("Amaya", "Opposite Hitter player");
teamRoster.push(player1.getAthlete());

var player2 = new volleyballPlayers("Nikole", "Libero player");
teamRoster.push(player2.getAthlete());

var player3 = new volleyballPlayers("Makayla", "Setter player");
teamRoster.push(player3.getAthlete());

var player4 = new volleyballPlayers("Jaden", "Middle Blocker player");
teamRoster.push(player4.getAthlete());

var player5 = new volleyballPlayers("Cecilia", "Passer player");
teamRoster.push(player5.getAthlete());

var player6 = new volleyballPlayers("Ava", "Passer player");
teamRoster.push(player6.getAthlete());

//for loop to console output the array of athletes
console.log("---------------The California Academy Volleyball Roster---------------");
for(let i = 0, j = teamRoster; i < j.length; i++)
{
  console.log(teamRoster[i]);
}
